function ratevariables = trans_Rates(v,Vrest2)
% Calculate transition rates.
persistent zee kw kr v_abs rateNumer rateDenom

zee = 0.5;
% Temperture compensation constants
kw = 3^((37-22)/10);
kr = 3.3^((37-22)/10);
v_abs = v + Vrest2;

ratevariables = zeros(1,12);

% Na rate constants
ratevariables(1) = (1.872*(v - 25.41))/(1 - exp((25.41 - v) / 6.06)); % am
ratevariables(2) = (3.973*(21.001 - v))/(1 - exp((v-21.001)/9.41)); % bm
ratevariables(3) = (-0.549*(27.74 + v))/(1 - exp((v + 27.74)/9.06)); % ah
ratevariables(4) = (22.57)/(1 + exp((56.0 - v)/12.5)); % bh

% K rate constants
ratevariables(5) = (0.129*(v - 35))/(1 - exp((35 - v)/10)); % an
ratevariables(6) = (0.3236*(35 - v))/(1-exp((v - 35)/10)); % bn

% HCN rate constants
rateNumer = (1 + exp(-(v_abs + 48)/6))^(-1/4);
rateDenom = 1.5 + 100*(6*exp((v_abs + 60)/6) + 16*exp(-(v_abs + 60)/45))^-1;
ratevariables(7) = kw*rateNumer/rateDenom; % aw
ratevariables(8) = kw*(1-rateNumer)/rateDenom; % bw

rateNumer = (zee + (1 - zee)*(1 + exp((v_abs + 71)/10))^-1);
rateDenom = (50 + 1000*(exp((v_abs + 60)/20) + exp(-(v_abs + 60)/8))^-1);
ratevariables(9) = kw*rateNumer/rateDenom; % az
ratevariables(10) = kw*(1-rateNumer)/rateDenom; % bz

% KLT rate constants
rateNumer = ((1 + exp((v_abs + 76)/7))^-1);
rateDenom = (25 + (1e5)*(273*exp((v_abs + 60)/12) + 17*exp(-(v_abs + 60)/14))^-1);
ratevariables(11) = kr*rateNumer/rateDenom; % ar
ratevariables(12) = kr*(1-rateNumer)/rateDenom; % br
% IMPORTANT NOTE: originally had 237 in rateDenom instead of 273. Assuming
% this is a typo!!

end
