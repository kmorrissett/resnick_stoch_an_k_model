function [v,N_Na,N_K,N_h,N_lt] = stochAN_single(Istim,fiber,options)

% stochAN_single  is a stochastic model for a patch of membrane at the node
% of Ranvier in an auditory nerve fiber. The function simulates four
% channel types - potassium delayed rectifier (Kv), fast activating sodium
% (Nav), hyperpolarization-activated cation channel (HCN), and low
% threshold potassium (KLT). The first two channel types are based on (MINO
% et al 2002) and the last two are based on (Rothman and Manis 2003b). All
% channel gates opening and closure obey a stochastic Markov process.
%
% For more details, see:
%
%    Negm, M. H., & Bruce, I. C. (2014). "The effects of HCN and KLT ion
%    channels on adaptation and refractoriness in a stochastic auditory
%    nerve model," IEEE Transactions on Biomedical Engineering
%    61(11):2749-2759.  http://dx.doi.org/10.1109/TBME.2014.2327055
%
% USAGE: [v,N_Na,N_K,N_h,N_lt] = stochAN_public1p0(Istim,fiber,options)
%
%   v is the voltage vector of the relative membane potential in millivolt
%       (mV)
%   N_Na, N_K, N_h, and N_lt are the number of opened channels for every
%       channel type
%   Istim is the stimulation current waveform in microampere (uA) fiber
%   contains electrical information about the membrane patch. options
%   carries optional simulation parameters
%
%
% Modified from stochAN_public1p0 by Mohamed H. Negm (negmmh@mcmaster.ca) &
% Ian C. Bruce (ibruce@ieee.org) � 2008-2014 by: 
% Jesse M. Resnick(resnick@uw.edu) � 2018. Modularized code more, 
% changed variable types to improve speed, and added comments/changed 
% variable names to improve readability.

rng(options.seed); % Set the rng seed using clock time.

% Extract fiber properties from passed fiber structure.
Cm = fiber.Cm;  Rm = fiber.Rm;

% Time step
dt = options.dt;

% Reversal potentials
E_Na = options.E_Na;    E_K = options.E_K;  E_h = options.E_h;

% Resting membrane potentials for different channels
Vrest = options.Vrest;  Vrest2 = options.Vrest2;

% Single channel (and leak) conductances in mS
gNa = options.gNa;  gK = options.gK;    glk = 1/Rm;
gh = options.gh;    glt = options.glt;

% Maximum number of channels at a node of Ranvier for each ion-channel type
N_Na_max = options.N_Na_max;    N_K_max = options.N_K_max;
N_lt_max = options.N_lt_max;    N_h_max = options.N_h_max;

k_max = length(Istim); % Number of time steps.

% Variables initialization
v = zeros(k_max,1);
N_Na = zeros(k_max,1);
N_K = zeros(k_max,1);
N_lt = zeros(k_max,1);
N_h = zeros(k_max,1);

% initial gating particles trasition rates
ratevariables = trans_Rates(v(1),Vrest2);

% Equilibrium constants for transitions.
m_inf = ratevariables(1)/(ratevariables(1)+ratevariables(2));
h_inf = ratevariables(3)/(ratevariables(3)+ratevariables(4));
n_inf = ratevariables(5)/(ratevariables(5)+ratevariables(6));
w_inf = ratevariables(7)/(ratevariables(7)+ratevariables(8));
z_inf = ratevariables(9)/(ratevariables(9)+ratevariables(10));
r_inf = ratevariables(11)/(ratevariables(11)+ratevariables(12));

% Resting channel conductances.
GeqNa = gNa * N_Na_max * m_inf^3 * h_inf;
GeqK  = gK * N_K_max * n_inf^4;
Geqlt = glt * N_lt_max * w_inf^4 * z_inf;
Geqh = gh * N_h_max * r_inf;
restingConductance = GeqNa + GeqK + glk + Geqlt + Geqh;

% Resting leak reversal potential.
E_lk = ( Vrest * (restingConductance)...
    - GeqNa * E_Na - GeqK * E_K - Geqh * E_h - Geqlt * E_K ) / glk;

% initial number of opened ion channels
N = initialize_HHCW_channels(ratevariables,dt);

% Pull out number of open channels.
N_Na(1) = N(8);
N_K(1)  = N(13);
N_lt(1) = N(23);
N_h(1)  = N(25);

for k = 1:(k_max-1)
    ratevariables = trans_Rates(v(k),Vrest2);
    
    N = update_HHCW_channels(ratevariables,N,dt);
    N_Na(k) = N(8);
    N_K(k)  = N(13);
    N_lt(k) = N(23);
    N_h(k)  = N(25);
    
    iNa = gNa*N_Na(k)*(v(k)-(E_Na - Vrest));
    iK  = gK*N_K(k)*(v(k)-(E_K - Vrest));
    ilt = glt*N_lt(k)*(v(k)-(E_K - Vrest));
    ih  = gh*N_h(k)*(v(k)-(E_h - Vrest));
    ilk = glk*(v(k)-(E_lk - Vrest));
    
    v(k+1) = v(k)+(Istim(k) -iNa -iK -ilt -ih -ilk)*dt/Cm;
end
