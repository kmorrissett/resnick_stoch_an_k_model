function Cluster_SinglePulse_eCAP_PW_demy(ExptName,dataDir,codeDir)

%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamMean	= 1.477;

% 0.5 mm Imennov & Rubinstein 2009. 0.22 mm matched to Wan & Corfas 2017
diamStdev	= 0.22;

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu      = 0.643;
normdtoDsigma   = 0.0738;

numFibers = 200;
%% Set fiber-independent model properties
model   = model_Config;
model.Terminal.areaCoef = 20;

%% Initialize fiber populations
anamProps(1) = diamMean;
anamProps(2) = diamStdev;
anamProps(3) = normdtoDmu;
anamProps(4) = normdtoDsigma;

demySevParam = [0.75,0.5;normdtoDmu/(1-normdtoDmu)*normdtoDsigma,0];

fibPop = {initialize_Population_Demyelinated(numFibers,demySevParam,anamProps,model)};

%% Set experimental options
options = options_Config;
options.numMonte = 5;
options.maxSpikes = 25;
options.recV = 0;
options.recECAP = 1;

%% Generate stimuli
I0 = 3;
Imin = 3; %
Imax = 15;
numI = 25;
pw = [10,25,50,100,200,400];   % pulse width in us
stimOptions.wait = 2000;                 % simulation time in us
stimOptions.delay = 2000;           % pulse onset time in us
stimOptions.IPG = 8;                  % interphase gap in us
stimOptions.type = 'cf';
allStims = cell(length(pw),numI+1);
I = zeros(length(pw),numI+1);

for pwIDX = 1: length(pw)
    stimOptions.pw = pw(pwIDX);
    I(pwIDX,1) = I0*10/stimOptions.pw;
    I(pwIDX,2:end) = logspace(log10(Imin*10/stimOptions.pw),log10(Imax*10/stimOptions.pw),numI);
    for curIDX = 1: length(I)
        stimOptions.I = I(pwIDX,curIDX);
        stimOptsCell = struct2varargin(stimOptions);
        allStims{pwIDX,curIDX} = makeStim_SinglePulse(stimOptsCell{:});
    end
end

% Save all the stimulus values after creating allStims.
stimOptions.I = I; %#ok<STRNU>
stimOptions.pw = pw;
if ~ispc
    % If on unix system save the git hash from the system
    [~,git_hash] = system('git rev-parse HEAD');  %#ok<ASGLU>
end
%% Save overall experiment info
cd(dataDir);
if ~isfolder(ExptName)
    mkdir(ExptName);
end
cd(ExptName); ExptFolder = pwd;
ExptFile = sprintf('%s%sExpt.mat',ExptFolder,filesep);

if ~ispc
    save(ExptFile,'fibPop','allStims','model','options','stimOptions','git_hash');
else
    save(ExptFile,'fibPop','allStims','model','options','stimOptions');
end

cluster_Expt_Setup(ExptName,codeDir,fibPop,allStims,options,ExptFolder)
