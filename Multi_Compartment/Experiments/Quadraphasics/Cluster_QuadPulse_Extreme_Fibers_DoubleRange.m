% function Cluster_QuadPulse_Extreme_Fibers(ExptName,dataDir,codeDir)
% Cluster Run
ExptName = 'FibersTest';
% dataDir = 'C:\Users\jsere\Documents\NewData';
% codeDir = 'C:\Users\jsere\Documents\StochAN\Multi_Compartment';
% dataDir = 'E:\Jesse\Documents\NewData';
codeDir = '/gscratch/cochlea/stoch_an/Multi_Compartment';
dataDir = '/gscratch/cochlea/NewData';
load([dataDir filesep 'StimMinMax_Quad_Cath_pw42_d3_Refin.mat'],'stimMin','stimMax','stimExt');

%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
anamProps.diameters	= [0.75,1.5,3,6];

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu      = 0.643;
normdtoDsigma   = 0.0738;
anamProps.dtoDs           = [normdtoDmu-2*normdtoDsigma,normdtoDmu,normdtoDmu+2*normdtoDsigma];

anamProps.zs              = 3;

%% Set fiber-independent model properties
model   = model_Config;

%% Initialize fiber populations
fibPop = {initialize_Population_Olivier(anamProps,model)};
numFibers = length(fibPop{1}.diameters);

%% Set experimental options
options = options_Config;
options.numMonte = 50;
options.maxSpikes = 25;
options.recV = 0;

%% Generate stimuli
stimOptions.wait = 3000;                 % simulation time in us
stimOptions.delay = 500;           % pulse onset time in us
stimOptions.pw = 42;                   % pulse width in us
stimOptions.IPG = 8;                  % interphase gap in us
stimOptions.IPI = 8;
stimOptions.type = 'cathodic';
numIsteps1 = 100; % sample the dynamic range
numIsteps2 = 15; % look at suprathreshold stim (sample steps between stimMax and stimExt)
numIsteps = numIsteps1 + numIsteps2;

allStims = cell(numIsteps,numFibers);
tempI = cell(numFibers,1);
for fibIDX = 1:length(fibPop{1}.diameters)
    I1 = logspace(log10(stimMin(fibIDX)),log10(stimMax(fibIDX)),numIsteps1);
    I2 = logspace(log10(stimMax(fibIDX))+1/20,log10(stimExt(fibIDX)),numIsteps2);
    I=[I1 I2];
    for curIDX = 1: length(I)
        stimOptions.I = I(curIDX);
        stimOptsCell = struct2varargin(stimOptions);
        allStims{curIDX,fibIDX} = makeStim_Quadraphasic(stimOptsCell{:});
    end
    % Save all the stimulus values after creating allStims.
    tempI{fibIDX} = I;
end
stimOptions = rmfield(stimOptions,'I'); stimOptions.I = tempI;
%% Save overall experiment info
cd(dataDir);
if ~isfolder(ExptName)
    mkdir(ExptName);
end
cd(ExptName); ExptFolder = pwd;
ExptFile = sprintf('%s%sExpt.mat',ExptFolder,filesep);
save(ExptFile,'fibPop','allStims','model','options','stimOptions');

cluster_FiberExpt_Setup(ExptName,codeDir,fibPop,allStims,options,ExptFolder)
