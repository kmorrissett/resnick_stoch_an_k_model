% Local QuadPulse Run

%% Population set-up
anamprops.diameters=[0.75 1.5 3 6];
anamprops.zs= 1;

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu      = 0.643;
normdtoDsigma   = 0.0738;
anamprops.dtoDs=[normdtoDmu-2*normdtoDsigma  normdtoDmu normdtoDmu+2*normdtoDsigma];


%% Set fiber-independent model properties
model   = model_Config;
numFibers = length(anamprops.diameters) * length(anamprops.zs) * length(anamprops.dtoDs);

Fibers = initialize_Population_Olivier(anamprops,model);

%% Set experimental options
options = options_Config;
options.numMonte = 2;
options.recV = 0;

%% Generate stimuli
%I = 0.5:0.1:7;
I = logspace(log10(0.01),log10(3),100);
stimOptions.wait = 3000;                 % simulation time in us
stimOptions.delay = 500;           % pulse onset time in us
stimOptions.pw = 42;                   % pulse width in us
stimOptions.IPG = 8;                  % interphase gap in us
stimOptions.IPI = 8;
stimOptions.type = 'cathodic';
allStims = cell(length(I),1);

for curIDX = 1: length(I)
    stimOptions.I = I(curIDX);
    stimOptsCell = struct2varargin(stimOptions);
    allStims{curIDX} = makeStim_Quadraphasic(stimOptsCell{:});
end

% Save all the stimulus values after creating allStims.
stimOptions.I = I;
%% Run expt
if options.recV
    [allSpikeOutput,allVoltOut] = local_Expt_Run(Fibers,allStims,model,options);
else
    allSpikeOutput = local_Expt_Run(Fibers,allStims,model,options);
end