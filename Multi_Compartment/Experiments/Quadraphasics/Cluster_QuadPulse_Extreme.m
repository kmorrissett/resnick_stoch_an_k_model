% function Cluster_QuadPulse_Extreme(ExptName,dataDir,codeDir)
% Cluster Run
ExptName = 'QuadCath1mm_42us_refined';
% dataDir = 'C:\Users\jsere\Documents\NewData';
% codeDir = 'C:\Users\jsere\Documents\StochAN\Multi_Compartment';
% dataDir = 'E:\Jesse\Documents\NewData';
codeDir = '/gscratch/cochlea/stoch_an/Multi_Compartment';
dataDir = '/gscratch/cochlea/NewData';

%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
anamProps.diameters	= [0.75,1.5,3,6];

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu      = 0.643;
normdtoDsigma   = 0.0738;
anamProps.dtoDs           = [normdtoDmu-2*normdtoDsigma,normdtoDmu,normdtoDmu+2*normdtoDsigma];

anamProps.zs              = 1;

%% Set fiber-independent model properties
model   = model_Config; 

%% Initialize fiber populations

fibPop = {initialize_Population_Olivier(anamProps,model)};

%% Set experimental options
options = options_Config;
options.numMonte = 50;
options.maxSpikes = 25;
options.recV = 0;

%% Generate stimuli
I = logspace(log10(0.02),log10(0.25),100);
stimOptions.wait = 3000;                 % simulation time in us
stimOptions.delay = 500;           % pulse onset time in us
stimOptions.pw = 42;                   % pulse width in us
stimOptions.IPG = 8;                  % interphase gap in us
stimOptions.IPI = 8; 
stimOptions.type = 'cathodic';
allStims = cell(length(I),1);

for curIDX = 1: length(I)
    stimOptions.I = I(curIDX);
    stimOptsCell = struct2varargin(stimOptions);
    allStims{curIDX} = makeStim_Quadraphasic(stimOptsCell{:});
end

% Save all the stimulus values after creating allStims.
stimOptions.I = I; %#ok<STRNU>
%% Save overall experiment info
cd(dataDir);
if ~isfolder(ExptName)
    mkdir(ExptName);
end
cd(ExptName); ExptFolder = pwd;
ExptFile = sprintf('%s%sExpt.mat',ExptFolder,filesep);
save(ExptFile,'fibPop','allStims','model','options','stimOptions');

cluster_Expt_Setup(ExptName,codeDir,fibPop,allStims,options,ExptFolder)
