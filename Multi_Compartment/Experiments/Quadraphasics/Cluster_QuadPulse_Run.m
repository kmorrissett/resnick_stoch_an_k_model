function Cluster_QuadPulse_Run(ExptName,dataDir,codeDir)
% Cluster Run
% ExptName = 'QuadThreshFind01';
% dataDir = 'C:\Users\jsere\Documents\NewData';
% codeDir = 'C:\Users\jsere\Documents\StochAN\Multi_Compartment';
% dataDir = 'E:\Jesse\Documents\NewData';
% codeDir = '/gscratch/cochlea/stoch_an/Multi_Compartment';
% dataDir = '/gscratch/cochlea/NewData';

%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamMean	= 1.477;

% 0.5 mm Imennov & Rubinstein 2009. 0.22 mm matched to Wan & Corfas 2017
diamStdev	= 0.22;

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu      = 0.643;
normdtoDsigma   = 0.0738;

numFibers = 200;
%% Set fiber-independent model properties
model   = model_Config;

%% Initialize fiber populations
anamProps(1) = diamMean;
anamProps(2) = diamStdev;
anamProps(3) = normdtoDmu;
anamProps(4) = normdtoDsigma;


fibPop = {initialize_Population(numFibers,anamProps,model)};

%% Set experimental options
options = options_Config;
options.numMonte = 50;
options.recV = 0;

%% Generate stimuli
I = logspace(log10(0.01),log10(3),100);
stimOptions.wait = 3000;                 % simulation time in us
stimOptions.delay = 500;           % pulse onset time in us
stimOptions.pw = 25;                   % pulse width in us
stimOptions.IPG = 8;                  % interphase gap in us
stimOptions.IPI = 8; 
stimOptions.type = 'cf';
allStims = cell(length(I),1);

for curIDX = 1: length(I)
    stimOptions.I = I(curIDX);
    stimOptsCell = struct2varargin(stimOptions);
    allStims{curIDX} = makeStim_Quadraphasic(stimOptsCell{:});
end

% Save all the stimulus values after creating allStims.
stimOptions.I = I; %#ok<STRNU>
if ~ispc 
    % If on unix system save the git hash from the system
    [~,git_hash] = system('git rev-parse HEAD');  %#ok<ASGLU>
end
%% Save overall experiment info
cd(dataDir);
if ~isfolder(ExptName)
    mkdir(ExptName);
end
cd(ExptName); ExptFolder = pwd;
ExptFile = sprintf('%s%sExpt.mat',ExptFolder,filesep);

if ~ispc 
    save(ExptFile,'fibPop','allStims','model','options','stimOptions','git_hash');
else
    save(ExptFile,'fibPop','allStims','model','options','stimOptions');
end

cluster_Expt_Setup(ExptName,codeDir,fibPop,allStims,options,ExptFolder)
