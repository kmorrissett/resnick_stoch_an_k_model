% Cluster Run
ExptName = 'scheduleTest2';
dataDir = 'C:\Users\jsere\Documents\NewData';
codeDir = 'C:\Users\jsere\Documents\StochAN\Multi_Compartment';
% dataDir = 'E:\Jesse\Documents\NewData';
% codeDir = '/gscratch/stf/resnick/stoch_an/Multi_Compartment';
% dataDir = '/gscratch/stf/resnick/NewData';

%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamMean	= 1.477;

% 0.5 mm Imennov & Rubinstein 2009. 0.22 mm matched to Wan & Corfas 2017
diamStdev	= 0.22;

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu    = 0.643;
normdtoDsigma    = 0.0738;

anamProps(1) = diamMean;
anamProps(2) = diamStdev;
anamProps(3) = normdtoDmu;
anamProps(4) = normdtoDsigma;

numFibers = 200;
%% Set fiber-independent model properties
model   = model_Config;

%% Initialize fiber populations
fibPop{1} = initialize_Population(numFibers,anamProps,model);
fibPop{2} = initialize_Population(numFibers,anamProps,model);

%% Set experimental options
options = options_Config;
options.numMonte = 25;
options.recV = 0;

%% Generate stimuli
stimOptions.condI = 3;
testI = 0.5:0.1:7;
IPI = [500,1000,1500,2000,4000];
stimOptions.wait = 2000;                 % simulation time in us
stimOptions.delay = 1000;           % pulse onset time in us
stimOptions.pw = 25;                   % pulse width in us
stimOptions.IPG = 8;                  % interphase gap in us
stimOptions.type = 'cf';
allStims = cell(length(testI),length(IPI));

for testIDX = 1: length(testI)
    stimOptions.testI = testI(testIDX);
    for IPIIDX = 1: length(IPI)
        stimOptions.IPI = IPI(IPIIDX);
        stimOptsCell = struct2varargin(stimOptions);
        allStims{testIDX,IPIIDX} = makeStim_PairedPulse(stimOptsCell{:});
    end
end

% Save all the stimulus values after creating allStims.
stimOptions.testI = testI; stimOptions.IPI = IPI;

%% Save overall experiment info
cd(dataDir);
if ~isfolder(ExptName)
    mkdir(ExptName);
end
cd(ExptName); ExptFolder = pwd;
ExptFile = sprintf('%s%sExpt.mat',ExptFolder,filesep);
save(ExptFile,'fibPop','allStims','model','options','stimOptions');

cluster_Expt_Setup(ExptName,codeDir,fibPop,allStims,options,ExptFolder)
