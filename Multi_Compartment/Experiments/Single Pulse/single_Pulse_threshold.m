function [fiberThresh,fiberRS,fiberFE,spikeOutput] = single_Pulse_threshold(Fibers,varargin)
% This function is a rough draft of how to encapsulate an entire threshold
% finding experiment together. NOTE: It's not ready to go yet.

p = inputParser;
addRequired(p,'Fibers',@(x) isstruct(x));
addParameter(p,'stimOptions',[],@(x) isstruct(x));
addParameter(p,'options',[],@(x) isstruct(x));
addParameter(p,'model',[],@(x) isstruct(x));
addParameter(p,'montes',10,@(x) isinteger(x) && isscalar(x));
addParameter(p,'Irange',0.5:0.1:1.5,@(x) isnumeric(x));
addParameter(p,'stimFunction',@makeStim_SinglePulse,@(x) isa(x,'function_handle'));
parse(p,Fibers,varargin{:})

model = p.Results.model; options = p.Results.options;
montes = p.Results.montes; stimOptions = p.Results.stimOptions;
numFibers = length(Fibers.diameters); Irange = p.Results.Irange;
stimFunction = p.Results.stimFunction;

if ~isstruct(model); model = model_Config; end

if ~isstruct(options)
    options = options_Config;
    options.numMonte = montes;
end
options.recV = 0; % Don't want to record this many voltage traces.

% Generate stimulus
if ~isstruct(stimOptions)
    stimOptions.wait = 5000;        % simulation time in us
    stimOptions.delay = 1000;  % pulse onset time in us
    stimOptions.pw = 25;            % pulse width in us
    stimOptions.IPG = 8;            % interphase gap in us
    stimOptions.type = 'af';        % stimulus type
end

numStims = length(Irange);
allStims = cell(numStims,1);
for IDX = 1:numStims
    stimOptions.I = Irange(IDX);
    stimOptsCell = struct2varargin(stimOptions);
    allStims{IDX} = stimFunction(stimOptsCell{:});
end

% Run the simulations
spikeOutput = cell(numStims,1);
for IDX = 1:numStims
    spikeOutput{IDX} = Population_Run(Fibers,allStims{IDX},model,options);
end

% Process the spiking data.
spikeNodeData = cell(numStims,1);
fiberFE = nan(numFibers,numStims);
popFE = nan(numStims,1);
for IDX = 1:length(Irange)
    spikeNodeData{IDX} = recordingNodeSpikes(spikeOutput{IDX},Fibers,model,options);
    [fiberFE(:,IDX),popFE(IDX)] = single_Pulse_FE(spikeNodeData{IDX});    
end
[fiberThresh,fiberRS] = CDF_thresh_fit(fiberFE,Irange);
end