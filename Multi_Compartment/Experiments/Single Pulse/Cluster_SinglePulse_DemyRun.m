% Cluster Run
ExptName = 'DemyThreshFind01';
% dataDir = 'C:\Users\jsere\Documents\NewData';
% codeDir = 'C:\Users\jsere\Documents\StochAN\Multi_Compartment';
% dataDir = 'E:\Jesse\Documents\NewData';
codeDir = '/gscratch/cochlea/stoch_an/Multi_Compartment';
dataDir = '/gscratch/cochlea/NewData';

%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamMean	= 1.477;

% 0.5 mm Imennov & Rubinstein 2009. 0.22 mm matched to Wan & Corfas 2017
diamStdev	= 0.22;

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu      = 0.643;
normdtoDsigma   = 0.0738;

demySevMu       = [0,0.5,0.75,0.75];
demySevSigma    = [0 ones(1,length(demySevMu)-1)*normdtoDmu/(1-normdtoDmu)*normdtoDsigma];

demyExtentMu        = [0,0.5,0.5,0.75];
demyExtentSigma     = 0;

numFibers = 200;
%% Set fiber-independent model properties
model   = model_Config;

%% Initialize fiber populations
anamProps(1) = diamMean;
anamProps(2) = diamStdev;
anamProps(3) = normdtoDmu;
anamProps(4) = normdtoDsigma;

numPops = length(demySevMu);
fibPop = cell(numPops,1);
for popIDX = 1:numPops
    demySevParam = [demySevMu(popIDX), demySevSigma(popIDX);
                        demyExtentMu(popIDX),demyExtentSigma];
    fibPop{popIDX} = initialize_Population_Demyelinated(numFibers,demySevParam,anamProps);
end

%% Set experimental options
options = options_Config;
options.numMonte = 25;
options.recV = 0;

%% Generate stimuli
I = 0.5:0.1:7;
stimOptions.wait = 3000;                 % simulation time in us
stimOptions.delay = 500;           % pulse onset time in us
stimOptions.pw = 25;                   % pulse width in us
stimOptions.IPG = 8;                  % interphase gap in us
stimOptions.type = 'af';
allStims = cell(length(I),1);

for curIDX = 1: length(I)
    stimOptions.I = I(curIDX);
    stimOptsCell = struct2varargin(stimOptions);
    allStims{curIDX} = makeStim_SinglePulse(stimOptsCell{:});
end

% Save all the stimulus values after creating allStims.
stimOptions.I = I;
%% Save overall experiment info
cd(dataDir);
if ~isfolder(ExptName)
    mkdir(ExptName);
end
cd(ExptName); ExptFolder = pwd;
ExptFile = sprintf('%s%sExpt.mat',ExptFolder,filesep);
save(ExptFile,'fibPop','allStims','model','options','stims','stimOptions');

cluster_Expt_Setup(ExptName,codeDir,fibPop,allStims,options,ExptFolder)
