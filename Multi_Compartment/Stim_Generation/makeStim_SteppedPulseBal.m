function [Istim,time] = makeStim_SteppedPulseBal(varargin)
% Creates a stimulus current array for a 'stepped' pulse
%
% USAGE:    [Istim,time] = makeStim_SteppedPulseBal
%           Istim = makeStim_SteppedPulseBal
%
% Optional type can be either 'anodic_inc' or 'cathodic_inc', referring to the
% polarity of the various phases with an increasingly higher amplitude. 
% Or 'anodic_dec' or 'cathodic_dec', referring to the
% polarity of the various phases with an increasingly lower amplitude.
% Pulsewidth, interphase gap,
% pre-pulse delay, and post-pulse wait can be passed as optional
% parameters. See parsing for default values.
% Recovery phase is rectangular
%
%   Author: Olivier Macherey, 2018 ; based on Jesse Resnick's makeStim_Quadraphasic.m function

p = inputParser;
validTypes = {'anodic_inc','anodic_dec','cathodic_inc','cathodic_dec'};
posNonZeroInt = @(x) isempty(x) || (x >= 0 && mod(x,1) == 0);
posInt = @(x) isempty(x) || (x > 0 && mod(x,1) == 0);
addParameter(p,'type','anodic',@(x) isempty(x) || any(strcmpi(x,validTypes)))
addParameter(p,'ISG',0,posNonZeroInt) % interstep gap ISG (us)
addParameter(p,'pw',5,posInt) % step pulsewidth (us)
addParameter(p,'nb_steps',4,posNonZeroInt) % Number of steps
addParameter(p,'IPG',0,posNonZeroInt) % interphase gap IPG (us)
addParameter(p,'pw2',20,posInt) % Duration of charge recovery phase (us)
addParameter(p,'delay',100,posNonZeroInt) % delay before pulse train onset(us)
addParameter(p,'wait',1000,posNonZeroInt) % wait after offset (us)
addParameter(p,'I',1,@(x) isempty(x) || x >= 0) % pulse current of the last step (mA)
parse(p,varargin{:})

type = p.Results.type;
ISG = p.Results.ISG; pw	= p.Results.pw; pw2 = p.Results.pw2;
nb_steps = p.Results.nb_steps; 
IPG = p.Results.IPG;
I = p.Results.I;
delay = p.Results.delay; wait = p.Results.wait;


pulseLength = (pw + ISG) * nb_steps;
totDur = delay + pulseLength + wait; % in us

time = 1:totDur; % us
Istim = zeros(1,totDur);

if strcmpi(type,'anodic_inc') || strcmpi(type,'cathodic_inc'),
    
    % Generate stimulus for anodic increasing case
    onsetIDX = round(delay+1);
    I_mean=0;
    for j=1:nb_steps,
       
        offsetIDX = onsetIDX + round(pw);
        Istim(onsetIDX:offsetIDX) = I*(j/nb_steps);
        I_mean=I_mean+I*(j/nb_steps);
        onsetIDX = offsetIDX + round(ISG);
        
    end
    onsetIDX = offsetIDX + round(IPG);
    offsetIDX = onsetIDX + round(pw2);
    I_mean=I_mean/nb_steps;
    Istim(onsetIDX:offsetIDX) = -I_mean*(nb_steps*pw)/pw2;

    if strcmpi(type,'cathodic_inc')
        Istim = -Istim;
    end

elseif strcmpi(type,'anodic_dec') || strcmpi(type,'cathodic_dec')
    
       % Generate stimulus for anodic increasing case
    onsetIDX = round(delay+1);
    I_mean=0;
    for j=1:nb_steps,
       
        offsetIDX = onsetIDX + round(pw);
        Istim(onsetIDX:offsetIDX) = I*((nb_steps-j+1)/nb_steps);
        I_mean=I_mean+I*(j/nb_steps);
        onsetIDX = offsetIDX + round(ISG);
        
    end
    
    onsetIDX = offsetIDX + round(IPG);
    offsetIDX = onsetIDX + round(pw2);
    I_mean=I_mean/nb_steps;
    Istim(onsetIDX:offsetIDX) = -I_mean*(nb_steps*pw)/pw2;

    if strcmpi(type,'cathodic_dec')
        Istim = -Istim;
    end
end

end % function [Istim, time] = makeStim_SteppedPulseBal
