function [Istim,time] = makeStim_PrePulseBal(varargin)
% Creates a stimulus current array for a pseudomonophasic pulse preceded by a
% monophasic pre-pulse of one polarity and followed by a monophasic
% post-pulse of opposite polarity (making the total charge balanced)
%
% USAGE:    [Istim,time] = makeStim_makeStim_PrePulseBal
%           Istim = makeStim_makeStim_PrePulseBal
%
% Optional type can be either 'AnodAnod', 'AnodCathod', 'CathodAnod', or 'CathodCathod'
% The two terms refer to the polarities of the prepulse and testpulse' first phases, respectively. 
% Pulsewidth, interpulse interval,
% pre-pulse delay, and post-pulse wait can be passed as optional
% parameters. See parsing for default values.
%
%   Author: Olivier Macherey, 2018 ; based on Jesse Resnick's makeStim_Quadraphasic.m function

p = inputParser;
validTypes = {'AnodAnod','AnodCathod','CathodAnod','CathodCathod'};
posNonZeroInt = @(x) isempty(x) || (x >= 0 && mod(x,1) == 0);
posInt = @(x) isempty(x) || (x > 0 && mod(x,1) == 0);
addParameter(p,'type','AnodAnod',@(x) isempty(x) || any(strcmpi(x,validTypes)))
addParameter(p,'IPI',0,posNonZeroInt) % interpulse interval between 1st phase of pre and 1st phase of test (us)
addParameter(p,'IPG',0,posNonZeroInt) % interphase gap between the two phases of the asymmetric test pulse(us)
addParameter(p,'IPI2',0,posNonZeroInt) % interpulse interval between 2nd phase of test and 2nd phase of pre (us)
addParameter(p,'pw_pre',400,posInt) % prepulse pulsewidth (us)
addParameter(p,'pw_test',100,posInt) % testpulse pulsewidth (us)
addParameter(p,'asym_ratio',4,posNonZeroInt) % ratio of amplitude between short high and long low phase
addParameter(p,'delay',100,posNonZeroInt) % delay before pulse train onset(us)
addParameter(p,'wait',1000,posNonZeroInt) % wait after offset (us)
addParameter(p,'I_pre',0.1,@(x) isempty(x) || x >= 0) % prepulse current (mA)
addParameter(p,'I_test',0.95,@(x) isempty(x) || x >= 0) % testpulse current (mA)
parse(p,varargin{:})

type = p.Results.type;
IPI = p.Results.IPI; IPG = p.Results.IPG; IPI2 = p.Results.IPI2; asym_ratio = p.Results.asym_ratio; 
pw_pre	= p.Results.pw_pre; pw_test = p.Results.pw_test; 
I_pre = p.Results.I_pre;
I_test = p.Results.I_test;
delay = p.Results.delay; wait = p.Results.wait;

pulseLength = pw_pre + IPI + pw_test + IPG + pw_test*asym_ratio + IPI2 + pw_pre;
totDur = delay + pulseLength + wait; % in us

time = 1:totDur; % us
Istim = zeros(1,totDur);

if strcmpi(type,'AnodAnod') || strcmpi(type,'CathodCathod')
    
    % Generate stimulus for anodic-anodic case
    onsetIDX = round(delay+1);
    offsetIDX = onsetIDX + round(pw_pre);
    Istim(onsetIDX:offsetIDX) = I_pre;
    onsetIDX = offsetIDX + round(IPI);
    offsetIDX = onsetIDX + round(pw_test);
    Istim(onsetIDX:offsetIDX) = I_test;
    onsetIDX = offsetIDX + round(IPG);
    offsetIDX = onsetIDX + round(pw_test*asym_ratio);
    Istim(onsetIDX:offsetIDX) = -I_test/asym_ratio;
    onsetIDX = offsetIDX + round(IPI2);
    offsetIDX = onsetIDX + round(pw_pre);
    Istim(onsetIDX:offsetIDX) = -I_pre;
    
    if strcmpi(type,'CathodCathod')
        Istim = -Istim;
    end

elseif strcmpi(type,'AnodCathod') || strcmpi(type,'CathodAnod')
    
    % Generate stimulus for anodic-cathodic case
    onsetIDX = round(delay+1);
    offsetIDX = onsetIDX + round(pw_pre);
    Istim(onsetIDX:offsetIDX) = I_pre;
    onsetIDX = offsetIDX + round(IPI);
    offsetIDX = onsetIDX + round(pw_test);
    Istim(onsetIDX:offsetIDX) = -I_test;
    onsetIDX = offsetIDX + round(IPG);
    offsetIDX = onsetIDX + round(pw_test*asym_ratio);
    Istim(onsetIDX:offsetIDX) = I_test/asym_ratio;
    onsetIDX = offsetIDX + round(IPI2);
    offsetIDX = onsetIDX + round(pw_pre);
    Istim(onsetIDX:offsetIDX) = -I_pre;

    if strcmpi(type,'CathodAnod')
        Istim = -Istim;
    end
end

end % function [Istim, time] = makeStim_PrePulseBal
