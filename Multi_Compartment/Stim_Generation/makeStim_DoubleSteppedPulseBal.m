function [Istim,time] = makeStim_DoubleSteppedPulseBal(varargin)
% Creates a stimulus current array for a 'stepped' pulse
%
% USAGE:    [Istim,time] = makeStim_DoubleSteppedPulseBal
%           Istim = makeStim_DoubleSteppedPulseBal
%
% Optional type can be either 'anodic_inc' or 'cathodic_inc', referring to the
% polarity of the various phases with an increasingly higher amplitude. 
% Or 'anodic_dec' or 'cathodic_dec', referring to the
% polarity of the various phases with an increasingly lower amplitude.
% Pulsewidth, interphase gap,
% pre-pulse delay, and post-pulse wait can be passed as optional
% parameters. See parsing for default values.
%
%   Author: Olivier Macherey, 2018 ; based on Jesse Resnick's makeStim_Quadraphasic.m function

p = inputParser;
validTypes = {'anodic_inc','anodic_dec','cathodic_inc','cathodic_dec'};
posNonZeroInt = @(x) isempty(x) || (x >= 0 && mod(x,1) == 0);
posInt = @(x) isempty(x) || (x > 0 && mod(x,1) == 0);
addParameter(p,'type','anodic_inc',@(x) isempty(x) || any(strcmpi(x,validTypes)))
addParameter(p,'type2','cathodic_inc',@(x) isempty(x) || any(strcmpi(x,validTypes)))
addParameter(p,'ISG',0,posNonZeroInt) % interstep gap ISG (us)
addParameter(p,'IPG',0,posNonZeroInt) % interphase gap IPG (us)
addParameter(p,'pw',5,posInt) % step pulsewidth (us)
addParameter(p,'nb_steps',4,posNonZeroInt) % Number of steps
addParameter(p,'delay',100,posNonZeroInt) % delay before pulse train onset(us)
addParameter(p,'wait',1000,posNonZeroInt) % wait after offset (us)
addParameter(p,'I',0.95,@(x) isempty(x) || x >= 0) % pulse current of the last step (mA)
parse(p,varargin{:})

type = p.Results.type;
type2 = p.Results.type2;
ISG = p.Results.ISG; pw	= p.Results.pw; nb_steps = p.Results.nb_steps; IPG = p.Results.IPG;
I = p.Results.I;
delay = p.Results.delay; wait = p.Results.wait;


pulseLength = (pw + ISG) * nb_steps;
totDur = delay + 2*pulseLength + IPG + wait; % in us

time = 1:totDur; % us
Istim = zeros(1,totDur);

if strcmpi(type,'anodic_inc') || strcmpi(type,'cathodic_inc'),
    
    % Generate stimulus for anodic increasing case
    onsetIDX = round(delay+1);
    for j=1:nb_steps,
       
        offsetIDX = onsetIDX + round(pw);
        Istim(onsetIDX:offsetIDX) = I*(j/nb_steps);
        onsetIDX = offsetIDX + round(ISG);
        
    end
        

    if strcmpi(type,'cathodic_inc')
        Istim = -Istim;
    end

elseif strcmpi(type,'anodic_dec') || strcmpi(type,'cathodic_dec')
    
       % Generate stimulus for anodic increasing case
    onsetIDX = round(delay+1);
    for j=1:nb_steps,
       
        offsetIDX = onsetIDX + round(pw);
        Istim(onsetIDX:offsetIDX) = I*((nb_steps-j+1)/nb_steps);
        onsetIDX = offsetIDX + round(ISG);
        
    end

    if strcmpi(type,'cathodic_dec')
        Istim = -Istim;
    end
end

onsetIDX = offsetIDX + round(IPG);
onsetPhase2 = onsetIDX;

%%%%%%%%%%%%%% Second phase %%%%%%%%%%%

if strcmpi(type2,'anodic_inc') || strcmpi(type2,'cathodic_inc'),
    
    % Generate stimulus for anodic increasing case
    for j=1:nb_steps,
       
        offsetIDX = onsetIDX + round(pw);
        Istim(onsetIDX:offsetIDX) = I*(j/nb_steps);
        onsetIDX = offsetIDX + round(ISG);
        
    end
        

    if strcmpi(type2,'cathodic_inc')
        Istim(onsetPhase2:offsetIDX) = -Istim(onsetPhase2:offsetIDX);
    end

elseif strcmpi(type2,'anodic_dec') || strcmpi(type2,'cathodic_dec')
    
       % Generate stimulus for anodic increasing case   
    for j=1:nb_steps,
       
        offsetIDX = onsetIDX + round(pw);
        Istim(onsetIDX:offsetIDX) = I*((nb_steps-j+1)/nb_steps);
        onsetIDX = offsetIDX + round(ISG);
        
    end

    if strcmpi(type2,'cathodic_dec')
        Istim(onsetPhase2:offsetIDX) = -Istim(onsetPhase2:offsetIDX);
    end
end

end % function [Istim, time] = makeStim_DoubleSteppedPulseBal
