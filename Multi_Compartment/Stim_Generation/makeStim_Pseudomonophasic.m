function [Istim,time] = makeStim_Pseudomonophasic(varargin)
% Creates a stimulus current array for a 'pseudomonophasic' pulse as described
%
% USAGE:    [Istim,time] = makeStim_Pseudomonophasic
%           Istim = makeStim_Pseudomonophasic
%
% Optional type can be either 'anodic' or 'cathodic', referring to the
% polarity of the first short-duration, high-amplitude phase. 
% Or 'rev_anodic' or 'rev_cathodic' still referring to the polarity of the
% short-duration, high-amplitude phase but in this case, this short phase
% is presented second
% Pulsewidth, interphase gap, interpulse interval,
% pre-pulse delay, and post-pulse wait can be passed as optional
% parameters. See parsing for default values.
%
%   Author: Olivier Macherey, 2018 ; based on Jesse Resnick's makeStim_Quadraphasic.m function

p = inputParser;
validTypes = {'anodic','cathodic','rev_anodic','rev_cathodic'};
posNonZeroInt = @(x) isempty(x) || (x >= 0 && mod(x,1) == 0);
posInt = @(x) isempty(x) || (x > 0 && mod(x,1) == 0);
addParameter(p,'type','anodic',@(x) isempty(x) || any(strcmpi(x,validTypes)))
addParameter(p,'IPG',8,posNonZeroInt) % interphase IPG (us)
addParameter(p,'pw',25,posInt) % pulsewidth (us)
addParameter(p,'asym_ratio',4,posNonZeroInt) % ratio of amplitude between short high and long low phase
addParameter(p,'delay',100,posNonZeroInt) % delay before pulse train onset(us)
addParameter(p,'wait',1000,posNonZeroInt) % wait after offset (us)
addParameter(p,'I',0.95,@(x) isempty(x) || x >= 0) % pulse current (mA)
parse(p,varargin{:})

type = p.Results.type;
IPG = p.Results.IPG; pw	= p.Results.pw; asym_ratio = p.Results.asym_ratio; 
I = p.Results.I;
delay = p.Results.delay; wait = p.Results.wait;


pulseLength = pw + asym_ratio*pw + IPG;
totDur = delay + pulseLength + wait; % in us

time = 1:totDur; % us
Istim = zeros(1,totDur);

if strcmpi(type,'anodic') || strcmpi(type,'cathodic')
    
    % Generate stimulus for anodic case
    onsetIDX = round(delay+1);
    offsetIDX = onsetIDX + round(pw);
    Istim(onsetIDX:offsetIDX) = I;
    onsetIDX = offsetIDX + round(IPG);
    offsetIDX = onsetIDX + round(asym_ratio * pw);
    Istim(onsetIDX:offsetIDX) = -I/asym_ratio;

    if strcmpi(type,'cathodic')
        Istim = -Istim;
    end

elseif strcmpi(type,'rev_anodic') || strcmpi(type,'rev_cathodic')
    
    % Generate stimulus for reversed anodic case
    onsetIDX = round(delay+1);
    offsetIDX = onsetIDX + round(asym_ratio * pw);
    Istim(onsetIDX:offsetIDX) = -I/asym_ratio;
    onsetIDX = offsetIDX + round(IPG);
    offsetIDX = onsetIDX + round(pw);
    Istim(onsetIDX:offsetIDX) = I;

    if strcmpi(type,'rev_cathodic')
        Istim = -Istim;
    end
end

end % function [Istim, time] = makeStim_Pseudomonophasic
