function [Istim,time] = makeStim_PrePulseUnbal(varargin)
% Creates a stimulus current array for a monophasic pulse preceded by a
% pre-pulse
%
% USAGE:    [Istim,time] = makeStim_makeStim_PrePulseUnbal
%           Istim = makeStim_makeStim_PrePulseUnbal
%
% Optional type can be either 'anodic_inc' or 'cathodic_inc', referring to the
% polarity of the various phases with an increasingly higher amplitude. 
% Or 'anodic_dec' or 'cathodic_dec', referring to the
% polarity of the various phases with an increasingly lower amplitude.
% Pulsewidth, interpulse interval,
% pre-pulse delay, and post-pulse wait can be passed as optional
% parameters. See parsing for default values.
%
%   Author: Olivier Macherey, 2018 ; based on Jesse Resnick's makeStim_Quadraphasic.m function

p = inputParser;
validTypes = {'AnodAnod','AnodCathod','CathodAnod','CathodCathod'};
posNonZeroInt = @(x) isempty(x) || (x >= 0 && mod(x,1) == 0);
posInt = @(x) isempty(x) || (x > 0 && mod(x,1) == 0);
addParameter(p,'type','AnodAnod',@(x) isempty(x) || any(strcmpi(x,validTypes)))
addParameter(p,'IPI',0,posNonZeroInt) % interpulse interval between pre and test (us)
addParameter(p,'pw_pre',400,posInt) % prepulse pulsewidth (us)
addParameter(p,'pw_test',100,posInt) % testpulse pulsewidth (us)
addParameter(p,'delay',100,posNonZeroInt) % delay before pulse train onset(us)
addParameter(p,'wait',1000,posNonZeroInt) % wait after offset (us)
addParameter(p,'I_pre',0.1,@(x) isempty(x) || x >= 0) % prepulse current (mA)
addParameter(p,'I',0.95,@(x) isempty(x) || x >= 0) % testpulse current (mA)
parse(p,varargin{:})

type = p.Results.type;
IPI = p.Results.IPI;
pw_pre	= p.Results.pw_pre; pw_test = p.Results.pw_test; 
I_pre = p.Results.I_pre;
I = p.Results.I;
delay = p.Results.delay; wait = p.Results.wait;

pulseLength = pw_pre + IPI + pw_test;
totDur = delay + pulseLength + wait; % in us

time = 1:totDur; % us
Istim = zeros(1,totDur);

if strcmpi(type,'AnodAnod') || strcmpi(type,'CathodCathod')
    
    % Generate stimulus for anodic-anodic case
    onsetIDX = round(delay+1);
    offsetIDX = onsetIDX + round(pw_pre);
    Istim(onsetIDX:offsetIDX) = I_pre;
    onsetIDX = offsetIDX + round(IPI);
    offsetIDX = onsetIDX + round(pw_test);
    Istim(onsetIDX:offsetIDX) = I;

    if strcmpi(type,'CathodCathod')
        Istim = -Istim;
    end

elseif strcmpi(type,'AnodCathod') || strcmpi(type,'CathodAnod')
    
    % Generate stimulus for anodic-cathodic case
    onsetIDX = round(delay+1);
    offsetIDX = onsetIDX + round(pw_pre);
    Istim(onsetIDX:offsetIDX) = I_pre;
    onsetIDX = offsetIDX + round(IPI);
    offsetIDX = onsetIDX + round(pw_test);
    Istim(onsetIDX:offsetIDX) = -I;

    if strcmpi(type,'CathodAnod')
        Istim = -Istim;
    end
end

end % function [Istim, time] = makeStim_PrePulseUnbal
