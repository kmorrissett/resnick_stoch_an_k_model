This Multi_Compartment folder contains a library of functions to run segmented cable neural models stimulated by a disc electrode. This family of functions is a reimplementation and extension of the cnerve family of simulations described in, among others:
	Mino, H., Rubinstein, J.T., Miller, C.A., Abbas, P.J., 2004. Effects of Electrode-to-Fiber Distance on Temporal Neural Response with Electrical Stimulation. IEEE Trans. Biomed. Eng. 51, 13–20. doi:10.1109/TBME.2003.820383
	
	O'Brien, G.E., Imennov, N.S., Rubinstein, J.T., 2016. Simulating electrical modulation detection thresholds using a biophysical model of the auditory nerve; J. Acoust. Soc. Am. 139, 2448–2462. doi:10.1121/1.4947430
	
	Resnick, J.M., O’Brien, G., Rubinstein, J.T., 2018. Simulated auditory nerve axon demyelination alters sensitivity and response timing to extracellular stimulation. Hear. Res. 361, 121–137. doi:10.1016/j.heares.2018.01.014

Function of this library can be most easily understood by the following single fiber and population simulation examples:

Example 1- Single fiber
Stoch_Test function in the TestScripts folder provides an example of a simple use case, it:
	- loads options and model Structures using corresponding functions (model_Config and options_Config)
	- produces a fiber structure and initialize segments based on that structure (using fiber_initialization)
	- generates a simple, single biphasic pulse stimulus (Istim)
	- runs a single fiber stochAN_multi simulation using the provided segments, Istim, model, and option inputs
	- plots heatmap and line plots of membrane potentials within segments
	- plots spike rasters of responses at recording node (node 32/36)
	
Example 2- 'normal', spataily-uniform fiber population
Pop_test is a script that provides a simple fiber populations example, it:
	- loads options and model Structures using corresponding functions (model_Config and options_Config)
	- produces a Fibers population structure based on provided geometry statistics using initialize_Population
	- generates a simple, single biphasic pulse stimulus (Istim)
	- It then calls Population_Run (or Population_Run_Parallel); for each fiber in Fibers population, this function:
		- initializes segments based on indexing into that structure (using fiber_initialization)
		- runs a single fiber stochAN_multi simulation using the  segments, Istim, model, and option inputs
	- plots spike rasters of responses at recording nodes for all fibers and monte carlo simulations
	
Example 3- spataily-uniform fiber population with demyelination
Pop_Test_Demy: Same as above but with incorporation of heterogenously applied demyelination (demyelination procedure is described in Resnick et al., 2018) for single fibers. This approach has been expanded to enable application of demyelination with biological plausible variability.

Example 4- spataily-distributed fiber population
Pop_Test_Spatial: Same as Ex. 2 but with incorporation of spatially distributed fibers based on fiber density along linear length of cochlea.