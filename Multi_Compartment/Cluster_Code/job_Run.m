function ExitCode = job_Run(ExptFile,popIDX,stimIDXs,SaveFile)
%JOB_RUN Runs a fiber population through allStims{stimIDXs} of stimulus
%waveforms where each fiber recieves the same stimulus intensity.


Expt = load(ExptFile,'fibPop','allStims','model','options');
dataDir = pwd;
Fibers = Expt.fibPop{popIDX};
spikeOutput = cell(length(stimIDXs),1);

if Expt.options.recV; vOutput = cell(length(stimIDXs),1); end
if Expt.options.recECAP; eCAPout = cell(length(stimIDXs),1); end

if isfield(Fibers,'demydtoD')
    RunFunc = @Population_Run_Demy;
else
    RunFunc = @Population_Run;
end

for trialIDX = 1:length(stimIDXs)
    stimIDX =stimIDXs(trialIDX);
    Istim = Expt.allStims{stimIDX};
    
    if Expt.options.recV && Expt.options.recECAP
        [spikeOutput{trialIDX},vOutput{trialIDX},eCAPout{trialIDX}] = RunFunc(Fibers,Istim,Expt.model,Expt.options);
    elseif Expt.options.recV
        [spikeOutput{trialIDX},vOutput{trialIDX}] = RunFunc(Fibers,Istim,Expt.model,Expt.options);
    elseif Expt.options.recECAP
        [spikeOutput{trialIDX},~,eCAPout{trialIDX}] = RunFunc(Fibers,Istim,Expt.model,Expt.options);
    else
        spikeOutput{trialIDX} = RunFunc(Fibers,Istim,Expt.model,Expt.options);
    end
end
cd(dataDir); %Make sure we're back in the correct directory
if Expt.options.recV && Expt.options.recECAP
    save(SaveFile,'spikeOutput','vOutput','eCAPout','stimIDXs');
elseif Expt.options.recV
    save(SaveFile,'spikeOutput','vOutput','stimIDXs');
elseif Expt.options.recECAP
    save(SaveFile,'spikeOutput','eCAPout','stimIDXs');
else
    save(SaveFile,'spikeOutput','stimIDXs');
end
ExitCode = 0;


end

