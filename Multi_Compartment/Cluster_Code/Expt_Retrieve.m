function [allSpikeOut, allVoltOut,allECAPout] = Expt_Retrieve(ExptFile,PopDir)
%EXPT_RETRIEVE This function accepts the Expt file for the entire
%experiment and the Population Directory for the fiber population and then
%outputs cell arrays of all of the spike and voltage data in one mat file.

Stemp = load(ExptFile,'fibPop','allStims','options');
fibPop = Stemp.fibPop; allStims = Stemp.allStims; options = Stemp.options;
if options.recV
    allVoltOut = cell(size(allStims));
end
if options.recECAP
    allECAPOut = cell(size(allStims));
end
allSpikeOut = cell(size(allStims));

numFibers = length(fibPop{1}.diameters); % assuming same number of fibers in all pops
numMontes = options.numMonte;
jobList = jobList_Allocate(numFibers,numMontes,allStims);

for jobIDX = 1:numel(jobList)
    savefile = sprintf('%s%sSpikesVoltsData_%02d.mat',PopDir,filesep,jobIDX);
    try
    if options.recV && options.recECAP
        load(savefile,'spikeOutput','vOutput','eCAPout','stimIDXs');
        allVoltOut(stimIDXs) = vOutput;
        allECAPout(stimIDXs) = eCAPout;
    elseif options.recV
        load(savefile,'spikeOutput','vOutput','stimIDXs');
        allVoltOut(stimIDXs) = vOutput;
    elseif options.recECAP
        load(savefile,'spikeOutput','eCAPout','stimIDXs');
        allECAPout(stimIDXs) = eCAPout;
    else
        load(savefile,'spikeOutput','stimIDXs');
    end
    allSpikeOut(stimIDXs) = spikeOutput;
    catch
        warning('Savefile %02d not found.',jobIDX);
    end
end

ResultsFile = [PopDir filesep 'ResultsOut.mat'];
if options.recV && options.recECAP
    save(ResultsFile,'allSpikeOut','allVoltOut','allECAPout','-v7.3');
elseif options.recV
    save(ResultsFile,'allSpikeOut','allVoltOut','-v7.3');
elseif options.recECAP
    save(ResultsFile,'allSpikeOut','allECAPout','-v7.3');
else
    save(ResultsFile,'allSpikeOut','-v7.3');
end
