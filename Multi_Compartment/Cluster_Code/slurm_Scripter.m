function slurm_Scripter(ExptName,nodesRequested,fibIDX,codeDir,popDir)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

SQLlist = sprintf('FiberPop%02u',fibIDX);
cd ..; exptDir = pwd;

% Generate parallel_sql job run
parallelFile = sprintf('%s%sparallelSubmit%02u.slurm',popDir,filesep,fibIDX);
fileID = fopen(parallelFile,'w');
fprintf(fileID,'#!/bin/bash\n');
fprintf(fileID,'#\n');
fprintf(fileID,'#SBATCH --job-name %s_Job%02u\n',ExptName,fibIDX);
fprintf(fileID,'#SBATCH --account stf-ckpt\n');
fprintf(fileID,'#SBATCH --partition ckpt\n\n');

fprintf(fileID,'## Resources\n');
fprintf(fileID,'#SBATCH --nodes=1\n');
fprintf(fileID,'#SBATCH --exclusive\n');
fprintf(fileID,'#SBATCH --time=4:00:00\n');
fprintf(fileID,'#SBATCH --mem=100G\n');
fprintf(fileID,'#SBATCH --workdir=%s\n',codeDir);
fprintf(fileID,'#SBATCH --output=%s/output_%%A\n\n',popDir);
fprintf(fileID,'export TZ="America/Los_Angeles"\n');

fprintf(fileID,'module load matlab_2018a\n');
fprintf(fileID,'module load parallel_sql\n');

% Grab matlab task from parallel_sql database. Delay if terminated with
% error to allow for reseting jobs.
fprintf(fileID,'# Create runcommand program \n');
fprintf(fileID,'runcommand()\n');
fprintf(fileID,'{\n');
fprintf(fileID,'# Exec replaces terminal with spawned process so termination codes are redirected. \n');
fprintf(fileID,'exec parallel-sql --sql --sql-set %s -a parallel --exit-on-term -j 1 --delay 300\n',SQLlist);
fprintf(fileID,'}\n\n');
fprintf(fileID,'# Create trap to catch termination/interrupt signals so that parallel sql will reset task to available. \n');
fprintf(fileID,'waituntilkill()\n');
fprintf(fileID,'{\n');
fprintf(fileID,'while true;\n');
fprintf(fileID,'do\n');
fprintf(fileID,'  sleep 1\n');
fprintf(fileID,'done\n');
fprintf(fileID,'}\n');
fprintf(fileID,'trap ''waituntilkill'' SIGTERM SIGINT\n');
fprintf(fileID,'runcommand\n');
fclose(fileID);

%% Retrieval job
%% Create MATLAB retrieval PBS file
fileID = fopen(sprintf('%s%sretrieve.slurm',popDir,filesep),'w');
fprintf(fileID,'#!/bin/bash\n');
fprintf(fileID,'#\n');
fprintf(fileID,'#SBATCH --job-name=%s_retrieval\n',ExptName);
fprintf(fileID,'#SBATCH --account stf-ckpt\n');
fprintf(fileID,'#SBATCH --partition ckpt\n');
fprintf(fileID,'#SBATCH --nodes=1\n');
fprintf(fileID,'#SBATCH --mem=100G\n');
fprintf(fileID,'#SBATCH --time=03:00:00\n');
fprintf(fileID,'#SBATCH --output=%s%sRetrieveOut\n',popDir,filesep);
fprintf(fileID,'#SBATCH --workdir=%s/Cluster_Code\n',codeDir);
fprintf(fileID,'#\n');
fprintf(fileID,'module load matlab_2018a\n');
fprintf(fileID,'#\n');
fprintf(fileID,'matlab -nosplash -nodisplay <<retrieveSub\n');
fprintf(fileID,'Expt_Retrieve(''%s'',''%s'');\n',[exptDir filesep 'Expt.mat'],popDir);
fprintf(fileID,'exit;\n');
fprintf(fileID,'retrieveSub\n');
fclose(fileID);

%% Generate parallel_sql initializer/submitter script
fileID = fopen(sprintf('%s%sqsubber.sh',popDir,filesep),'w');
fprintf(fileID,'#!/bin/bash\n');
fprintf(fileID,'#\n');

fprintf(fileID,'cd %s\n',popDir);
% Start parallel sql, clear list, and cat commands to it
fprintf(fileID,'module load parallel_sql\n');
fprintf(fileID,'psu --del --sql-set %s -y\n',SQLlist);
fprintf(fileID,'cat commands_%02u | psu --load --sql-set %s\n',fibIDX,SQLlist);

% Submit a bunch of parallel_sql runs
fprintf(fileID,'for job in $(seq 1 %u); do SimJobs+="$(sbatch %s | tail -c -7):"; done\n',nodesRequested,parallelFile);
fprintf(fileID,'SimJobs=$(echo -n $SimJobs | head -c -1)\n');
fprintf(fileID,'echo $SimJobs\n');

% % Call retrieval script after all simulations are done.
% fprintf(fileID,'sbatch -p ckpt -A stf-ckpt --dependency=afterok:$SimJobs ./retrieve.slurm\n');
% fprintf(fileID,'exit 0');

cd(popDir);
if isunix
    fileattrib('*.sh','+x');
    fileattrib('*.slurm','+x');
end

end

