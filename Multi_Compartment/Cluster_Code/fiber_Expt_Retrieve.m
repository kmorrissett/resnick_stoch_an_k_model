function [allSpikeOut, allVoltOut] = fiber_Expt_Retrieve(ExptFile,PopDir)
%EXPT_RETRIEVE This function accepts the Expt file for the entire
%experiment and the Population Directory for the fiber population and then
%outputs cell arrays of all of the spike and voltage data in one mat file.

Stemp = load(ExptFile,'fibPop','allStims','options');
fibPop = Stemp.fibPop; allStims = Stemp.allStims; options = Stemp.options;
if options.recV
    allVoltOut = cell(size(allStims,1),1);
end
allSpikeOut = cell(size(allStims,1),1);

numFibers = length(fibPop{1}.diameters); % assuming same number of fibers in all pops
numMontes = options.numMonte;
jobList = jobList_Fibers_Allocate(numFibers,numMontes,allStims);

for jobIDX = 1:numel(jobList)
    savefile = sprintf('%s%sSpikesVoltsData_%02d.mat',PopDir,filesep,jobIDX);
    if options.recV
        load(savefile,'spikeOutput','vOutput','stimIDXs');
        allVoltOut(stimIDXs) = vOutput;
    else
        load(savefile,'spikeOutput','stimIDXs');
    end
    allSpikeOut(stimIDXs) = spikeOutput;
end

ResultsFile = [PopDir filesep 'ResultsOut.mat'];
if options.recV
    save(ResultsFile,'allSpikeOut','allVoltOut');
else
    save(ResultsFile,'allSpikeOut');
end
