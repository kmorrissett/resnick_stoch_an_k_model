function [jobList,nodesRequested] = jobList_Fibers_Allocate(numFibers,numMontes,allStim)
%JOBLIST_FIBERS_ ALLOCATE Allocates stimuli and, eventually, fibers to different
%trials to optimize job length. Note: in this function each fiber recieves
%a different current intensity.

% Maximum desired job duration (s)
corePerProc = 14;
procPerNode = 2;
realToJobTime = 10000; % simulation duration that runs in ~1s of core time
maxNodeTime = 4*corePerProc*procPerNode*60*60*realToJobTime; % us
maxJobTime = maxNodeTime/32; % Target job time

% Determine durations of all trials
stimLength  = zeros(1,size(allStim,1));
for stimIDX = 1:size(allStim,1)
    stimLength(stimIDX) = length(allStim{stimIDX,1});
end
stimDurs = numFibers*numMontes*stimLength;
totalDur = sum(stimDurs); % Total simulation duration (us)

nodesNeeded = ceil(totalDur/maxNodeTime);

% Determine approximate length of simulation runs
jobList = {}; jobDur = 0; listIDX = 1;

for stimIDX = 1:size(allStim,1)
    jobDur = jobDur + stimDurs(stimIDX);
    if jobDur < maxJobTime
        if stimIDX == 1
            jobList{listIDX} = stimIDX; %#ok<*AGROW>
        else
            jobList{listIDX} = [jobList{listIDX}, stimIDX];
        end
    else
        listIDX = listIDX + 1;
        jobDur = stimDurs(stimIDX);
        jobList{listIDX} = stimIDX;
    end
end

% Request more than strictly necessary to accelerate.
nodesRequested = min(nodesNeeded*5,length(jobList));
