function cluster_Expt_Setup(ExptName,codeDir,fibPop,allStims,options,ExptFolder)
%CLUSTER_EXPT_SETUP This function accepts expetiment and directory
%information and prepares a command list and scripts for Hyak submission.

% See Cluster_Run_Test script for example usage.

format = 'matlab -nosplash -nodisplay -r "try, addpath(genpath(''.'')); cd(''%s''); parpool(14); job_Run(''%s'',%u,%s,''%s''), exit(0); catch ME, warning(ME.message); exit(1), end";  exit $?\n';

%% Create Job for each fiberPop
for fibIDX = 1:numel(fibPop)
    cd(ExptFolder);
    PopDir = sprintf('FiberPop%02u',fibIDX);
    ExptFile = 'Expt.mat';
    if ~isfolder(PopDir); mkdir(PopDir); end; cd(PopDir); dataDir = pwd;
    
    numFibers = length(fibPop{fibIDX}.diameters);
    numMontes = options.numMonte;
    
    [jobList,nodesRequested] = jobList_Allocate(numFibers,numMontes,allStims);
    
    % Build Task list
    cmdfile = sprintf('commands_%02u',fibIDX);
    fid = fopen(cmdfile,'w');
    for jobIDX = 1:numel(jobList)
        savefile = sprintf('.%s%s%sSpikesVoltsData_%02d.mat',filesep,PopDir,filesep,jobIDX);
        stimList = [ '[' sprintf('%d ',jobList{jobIDX}) ']'];
        fprintf(fid,format,ExptFolder,ExptFile,fibIDX,stimList,savefile);
    end
    fclose(fid);
    
    % Generate individual job SLURM scripts and qsubber.sh submitter
    slurm_Scripter(ExptName,nodesRequested,fibIDX,codeDir,dataDir);
end
end

