function eCAP = eCAP_Test(I0)
%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamMean	= 1.477;	

% 0.5 mm Imennov & Rubinstein 2009. 0.22 mm matched to Wan & Corfas 2017
diamStdev	= 0.5;     

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu    = 0.643;   
normdtoDsigma    = 0.0738;

anamProps(1) = diamMean;
anamProps(2) = diamStdev;
anamProps(3) = normdtoDmu;
anamProps(4) = normdtoDsigma;
    
numFibers = 20;
%% Set fiber-independent model properties
model   = model_Config;
model.Terminal.areaCoef = 20;
%% Initialize fiber population
Fibers = initialize_Population(numFibers,anamProps,model);

%% Set experimental options
options = options_Config;
options.numMonte = 1;
options.recV = 0;
options.recECAP = 1;

%% Generate stimulus
stimOptions.I = 0.5;
stimOptions.wait = 2000;           % simulation time in us
stimOptions.delay = 2000;           % pulse onset time in us
stimOptions.pw = 400;               % pulse width in us
stimOptions.IPG = 8;               % interphase gap in us
stimOptions.type = 'cf';

% Calling the stimulating current functions
stimOptsCell = struct2varargin(stimOptions);
Istim_masked = makeStim_SinglePulse(stimOptsCell{:});
maskedI = stimOptions.I;

stimOptions.I = I0;
stimOptions.type = 'cf';

% Calling the stimulating current functions
stimOptsCell = struct2varargin(stimOptions);
Istim_unmasked = makeStim_SinglePulse(stimOptsCell{:});

%% Run the simulations
[~,~,SFAPsMasked] = Population_Run(Fibers,Istim_masked,model,options);
[~,~,SFAPsOut] = Population_Run(Fibers,Istim_unmasked,model,options);

%% Plot out eCAP
eCAPMask = mean(SFAPsMasked,1);
eCAPOut = mean(SFAPsOut,1);
for monteIDX = 1:options.numMonte
    eCAPMask(monteIDX,:) = (eCAPMask(monteIDX,:)-mean(eCAPMask(monteIDX,:)))*(I0/maskedI)+mean(eCAPMask(monteIDX,:));
end
eCAP = eCAPOut-eCAPMask;

figure
plot(mean(eCAP(:,1000:end),1)*1000,'LineWidth',1);
set(gca,'FontSize',14)
xlim([0,3000]); xlabel('Time (\mus)');
ylabel('Extracellular Potential (\muV)')
