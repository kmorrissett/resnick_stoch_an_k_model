function eCAP = SFAP_test(I0)
model = model_Config;
model.Terminal.areaCoef = 15;
%% Set fiber properties
diamMean = 1.5;
fiber.diameter = 1.5;      % axolemma diameter (um)
normdtoDmu      = 0.643;
% normdtoDsigma   = 0.0738;
fiber.dtoD = normdtoDmu;             % d/D
% fiber.cutoff = [];            % PLACEHOLDER: demyelination extent
% fiber.demydtoD = [];          % PLACEHOLDER: demyelination severity

fiber.numNodes = ceil(diamMean/fiber.diameter*36);
fiber.zDistance = 3;            % distance of fiber from electrode center (mm)
fiber.intSegs = ceil(model.intSegs*fiber.diameter/diamMean);

%% Set experimental options
options = options_Config;
options.numMonte = 1;
options.recV = 1;
options.recECAP = 1;
options.maxSpikes = 5;
options.XstimLoc = -(model.LtoD*diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanNodeAbove;
options.eCAPxLoc = options.XstimLoc;    % ecap recording x-location
options.eCAPzLoc = 3;             % ecap recording height

%% Generate subthresh mask and main stimuli
stimOptions.I = 0.5;
stimOptions.wait = 2000;           % simulation time in us
stimOptions.delay = 2000;           % pulse onset time in us
stimOptions.pw = 25;               % pulse width in us
stimOptions.IPG = 8;               % interphase gap in us
stimOptions.type = 'af';

% Calling the stimulating current functions
stimOptsCell = struct2varargin(stimOptions);
Istim_masked = makeStim_SinglePulse(stimOptsCell{:});
maskedI = stimOptions.I;

stimOptions.I = I0;
stimOptions.type = 'af';

% Calling the stimulating current functions
stimOptsCell = struct2varargin(stimOptions);
Istim_unmasked = makeStim_SinglePulse(stimOptsCell{:});

%% Calculate segment properties from fiber and model info
segments = fiber_initialization(fiber,model);

%% Run model
% Need consistent field order for MEX function.
segments = orderfields(segments);
model = orderfields(model);
options = orderfields(options);
tic;
[~,~,eCAPmasked] = stochAN_multi_mex(Istim_masked,segments,model,options);
[~,vOutput,eCAPout] = stochAN_multi_mex(Istim_unmasked,segments,model,options);
toc;

%%
scaledMask = eCAPmasked;
for monteIDX = 1:options.numMonte
    scaledMask(monteIDX,:) = (eCAPmasked(monteIDX,:)-mean(eCAPmasked(monteIDX,:)))*(I0/maskedI)+mean(eCAPmasked(monteIDX,:));
end
eCAP = eCAPout-scaledMask;

if options.recV
    figure
    imagesc(squeeze(vOutput(1,:,:)-84).',[-85,50]);
    c = colorbar; c.Label.String = 'Voltage (mV)';
    ylabel('Segment #');
    xlabel('Timestep (\mus)');
    set(gca,'FontSize',14);
    
%     figure
%     imagesc(squeeze(vMask(1,:,:)-84),[-85,50]);
%     c = colorbar; c.Label.String = 'Voltage (mV)';
%     xlabel('Segment #');
%     ylabel('Timestep (\mus)');
%     figure
%     subplot(2,1,1)
%     simtime = length(Istim_unmasked)-1;
%     for i = 1:(model.intSegs+1):size(vOutput,3)
%         plot(0:options.Vsample/model.dt:simtime,vOutput(1,:,i)-84); hold on;
%     end
%     subplot(2,1,2)
%     plot(0:model.dt:simtime/1000,Istim_unmasked,'k','LineWidth',1.5)
end

figure
plot(mean(eCAP(:,1000:end),1)*1000,'LineWidth',1);
set(gca,'FontSize',14)
xlim([0,3000]); xlabel('Time (\mus)');
ylabel('Extracellular Potential (\muV)')

% DataDir = 'G:\My Drive\Lab\RetrievedData\stoch_AN_based\eCAPs\2018.08.17 Tests\';
% FileName = sprintf('d%0.1_Rterm%0.1d_Vdec%0.1d_X%0.1f_Z%0.1f_ecap.png',...
%     fiber.diameter,model.Terminal.ra,model.Terminal.Vdec,options.eCAPxLoc,options.eCAPzLoc);
% saveas(gcf,[DataDir FileName])
end