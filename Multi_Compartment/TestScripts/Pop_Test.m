%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamMean	= 1.477;	

% 0.5 mm Imennov & Rubinstein 2009. 0.22 mm matched to Wan & Corfas 2017
diamStdev	= 0.22;     

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu    = 0.643;   
normdtoDsigma    = 0.0738;

anamProps(1) = diamMean;
anamProps(2) = diamStdev;
anamProps(3) = normdtoDmu;
anamProps(4) = normdtoDsigma;
    
numFibers = 200;
%% Set fiber-independent model properties
model   = model_Config;

%% Initialize fiber population
Fibers = initialize_Population(numFibers,anamProps,model);

%% Set experimental options
options = options_Config;
options.numMonte = 1;
options.recV = 0;
options.recECAP = 1;

%% Generate stimulus
stimOptions.I = 3;
% stimOptions.testI = 1.15;
% stimOptions.IPI = 2000;
stimOptions.wait = 2000;                 % simulation time in us
stimOptions.delay = 1000;           % pulse onset time in us
stimOptions.pw = 25;                   % pulse width in us
stimOptions.IPG = 8;                  % interphase gap in us
stimOptions.type = 'cf';

stimOptsCell = struct2varargin(stimOptions);
Istim = makeStim_SinglePulse(stimOptsCell{:});

%% Run the simulations
[spikeOutput,~,eCAPout] = Population_Run(Fibers,Istim,model,options);

%% Plot it out
recSpikes = nan(numFibers,options.numMonte,options.maxSpikes);
for fibIDX = 1:numFibers
    nodeInt = round(Fibers.numNodes(fibIDX)*options.posRecord);
    recSpikes(fibIDX,:,:) = squeeze(spikeOutput{fibIDX}(:,nodeInt,:));
end

simtime = length(Istim)-1;
recSpikes = permute(recSpikes,[2,1,3]);
spikeTimes = reshape(recSpikes,[numFibers*options.numMonte,options.maxSpikes]);
spikeTimes = mat2cell(spikeTimes,ones(1,numFibers*options.numMonte),options.maxSpikes);

figure
subplot(2,1,1)
LineFormat.Color = 'k'; LineFormat.LineWidth = 1.5;
plotSpikeRaster(spikeTimes,'PlotType','vertline','XLimForCell',...
    [100,5000],'LineFormat',LineFormat);
ylabel('Trial Number');
xlabel('Spike Time (\mus)');
subplot(2,1,2)
plot(0:simtime,Istim,'k','LineWidth',2)

%% Plot out eCAP
plot(0:simtime,eCAPout)