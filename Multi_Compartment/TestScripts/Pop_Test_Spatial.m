%% Population set-up
% Set Population Moment Parameters based on data
% 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamMean	= 1.477;	
% 0.5 mm Imennov & Rubinstein 2009. 0.22 mm matched to Wan & Corfas 2017
diamStdev	= 0.22;     

% dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDmu    = 0.643;   normdtoDsigma    = 0.133;

% Geometry of model cochlear region
yLimits     = 4;    % Linear length of cochlea to simulate (mm).
zDistance   = 3;    % Distance from electrode to closest fibers (mm).
% Will ultimately want to base on fiber density (fibers/linear mm cochlea)
IHCdensity  = 3500/33.5; % hair cells/ cochlear length (mm), Retzius, 1884.
ANperIHC    = 10;          % afferents/IHC Nadol, 1988.
ANdensity   = ANperIHC*IHCdensity;
% numFibers   = round(ANdensity*yLimits);
numFibers   = 50;

anamProps(1) = diamMean;
anamProps(2) = diamStdev;
anamProps(3) = normdtoDmu;
anamProps(4) = normdtoDsigma;
anamProps(5) = yLimits;
anamProps(6) = zDistance;
    
%% Set fiber-independent model properties
model   = model_Config;

%% Initialize fiber population
Fibers = initialize_Population_Spatial(numFibers,anamProps,model);

%% Set experimental options
options = options_Config();
options.numMonte = 2;
options.recV = 1; 


%% Generate stimulus
condI = .95;        % pre-pulse current
testI = 1.15;       % test-pulse current
IPI = 1000;         % inter-pulse interval (us)
wait = 3000;        % post-stimulus wait time in us
pulseOnset = 1000;  % pulse onset time in us
pw = 100;           % pulse width in us
IPG = 8;            % interphase gap in us
type = 'cf';

stimOptions = {'wait',wait,'delay',pulseOnset,'pw',pw,'IPG',IPG...
    'IPI',IPI,'condI',condI,'testI',testI,'type',type};

Istim = makeStim_PairedPulse(stimOptions{:});

%% Run the simulations
[spikeOutput,vOutput] = Population_Run(Fibers,Istim,model,options);

recSpikes = nan(numFibers,options.numMonte,options.maxSpikes);
for fibIDX = 1:numFibers
    nodeInt = round(Fibers.numNodes(fibIDX)*options.posRecord);
    recSpikes(fibIDX,:,:) = squeeze(spikeOutput{fibIDX}(:,nodeInt,:));
end

%% Plot the spike and voltage results
simtime = length(Istim)-1;
recSpikes = permute(recSpikes,[2,1,3]);
spikeTimes = reshape(recSpikes,[numFibers*options.numMonte,options.maxSpikes]);
spikeTimes = mat2cell(spikeTimes,ones(1,numFibers*options.numMonte),options.maxSpikes);

figure
subplot(2,1,1)
LineFormat.Color = 'k'; LineFormat.LineWidth = 1.5;
plotSpikeRaster(spikeTimes,'PlotType','vertline','XLimForCell',...
    [0,simtime],'LineFormat',LineFormat);
ylabel('Node Number');
xlabel('Spike Time (\mus)');
subplot(2,1,2)
plot(0:simtime,Istim,'k','LineWidth',2)

figure
voltData = squeeze(vOutput{25}(1,:,:)).';
imagesc([0,simtime],[0,size(voltData,1)],voltData);
xlabel('Time (\mus)');
ylabel('Fiber Segment');