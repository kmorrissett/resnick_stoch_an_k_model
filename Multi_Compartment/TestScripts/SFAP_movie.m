function [eCAP,F] = SFAP_movie(I0)
model = model_Config;
model.Terminal.areaCoef = 15;
%% Set fiber properties
diamMean = 1.5;
fiber.diameter = 1.5;      % axolemma diameter (um)
normdtoDmu      = 0.643;
% normdtoDsigma   = 0.0738;
fiber.dtoD = normdtoDmu;             % d/D
% fiber.cutoff = [];            % PLACEHOLDER: demyelination extent
% fiber.demydtoD = [];          % PLACEHOLDER: demyelination severity

fiber.numNodes = ceil(diamMean/fiber.diameter*36);
fiber.zDistance = 3;            % distance of fiber from electrode center (mm)
fiber.intSegs = ceil(model.intSegs*fiber.diameter/diamMean);

%% Set experimental options
options = options_Config;
options.numMonte = 1;
options.recV = 0;
options.recECAP = 1;
options.maxSpikes = 5;
options.XstimLoc = -(model.LtoD*diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanNodeAbove;
options.eCAPxLoc = options.XstimLoc;    % ecap recording x-location
options.eCAPzLoc = 3;             % ecap recording height

%% Generate subthresh mask and main stimuli
stimOptions.I = 0.5;
stimOptions.wait = 2000;           % simulation time in us
stimOptions.delay = 2000;           % pulse onset time in us
stimOptions.pw = 25;               % pulse width in us
stimOptions.IPG = 8;               % interphase gap in us
stimOptions.type = 'af';

% Calling the stimulating current functions
stimOptsCell = struct2varargin(stimOptions);
Istim_masked = makeStim_SinglePulse(stimOptsCell{:});
maskedI = stimOptions.I;

stimOptions.I = I0;
stimOptions.type = 'af';

% Calling the stimulating current functions
stimOptsCell = struct2varargin(stimOptions);
Istim_unmasked = makeStim_SinglePulse(stimOptsCell{:});

%% Calculate segment properties from fiber and model info
segments = fiber_initialization(fiber,model);

%% Run model
% Need consistent field order for MEX function.
segments = orderfields(segments);
model = orderfields(model);
options = orderfields(options);
[~,~,eCAPout] = stochAN_multi_SFAPmovie(Istim_unmasked,segments,model,options);

%%
eCAPxs = -8:0.1:2;
eCAPzs =  0.1:0.1:6;
eCAPout = permute(eCAPout,[2,1,3,4]);

%%
figure('position',[50,50,1200,500])
plot(segments.xApp,zeros(length(segments.xApp),1),'k','LineWidth',3); hold on;
imagesc(-eCAPxs,eCAPzs,squeeze(eCAPout(:,:,1,1)));
plot(-options.eCAPxLoc,options.eCAPzLoc,'.k','MarkerSize',15);
axis tight manual
ax = gca;
ax.NextPlot = 'replaceChildren';

F((3500-1500)/10+1) = struct('cdata',[],'colormap',[]);
for t = 1501:10:3501
imagesc(-eCAPxs,eCAPzs,squeeze(eCAPout(:,:,1,t))); hold on; caxis([-4e-5,4e-5])
plot(segments.xApp,zeros(length(segments.xApp),1),'k','LineWidth',3); 
plot(-options.eCAPxLoc,options.eCAPzLoc,'.k','MarkerSize',15);
xlim([-1,8]); ylim([0,5]); xticklabels([]); yticklabels([]);
drawnow
F((t-1)/10-149) = getframe(gcf);
end

eCAP = eCAPout;
end