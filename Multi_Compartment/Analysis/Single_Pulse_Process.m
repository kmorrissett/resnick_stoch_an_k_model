function [fiberFE,popFE,spikeNodeData] = Single_Pulse_Process(allStims,fibPop,model,options,stimOptions,allSpikeOut)

spikeData = reshape(allSpikeOut,size(allStims));
spikeNodeData = cell(size(allStims));
popFE = zeros(size(allStims));
numFibers = length(fibPop{1}.diameters);
fiberFE = zeros([size(allStims) numFibers]);
for stimIDX = 1:numel(spikeData)
    spikeNodeData{stimIDX} = recordingNodeSpikes(spikeData{stimIDX},fibPop{1},model,options);
    [I,J] = ind2sub(size(spikeData),stimIDX);
    [fiberFE(I,J,:),popFE(stimIDX)] = single_Pulse_FE(spikeNodeData{stimIDX});
    [fiberThresh(I,J),fiberRS(I,J)] = CDF_thresh_fit(fiberFE(I,J,:),stimOptions.I(,[0.005,0.05]);
end
    

for stimIDX = [1:10:61,66]
    spikeTimes = [];
    for fibIDX =  1:numFibers
        spikeTimes = [spikeTimes, spikeNodeData{stimIDX}{fibIDX}];
    end
    figure
    histogram(spikeTimes,'BinLimits',[500,1120],'NumBin',50)
end

logStims = 20*log10(stimOptions.I/0.001);
for fibIDX = 1:numFibers
    plot(logStims,fiberFE(:,1,fibIDX),'LineWidth',1.5); hold on
end
ylabel('Firing Efficiency'); ylim([-.1,1.1])
xlabel('Stimulus Current (db re 1\muA)');xlim([-inf,inf])