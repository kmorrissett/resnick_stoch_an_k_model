function [fiberThresh,fiberRS] = CDF_thresh_fit(fiberFE,Irange,varargin)
%
% This script figures out threshold and relative spreads for all fibers for
% a given stimulus waveform over the range Irange. Usage:
%    [fiberThresh,fiberRS] = CDF_thresh_fit(fiberFE,Irange)

%%

p = inputParser;
addRequired(p,'fiberFE');
addRequired(p,'Irange');
addOptional(p,'initial',[0.01,0.05])
parse(p,fiberFE,Irange,varargin)

numfibers = size(fiberFE,2); %Find # of fibers per trial.

fiberThresh = nan(numfibers,1);
fiberRS = nan(numfibers,1);

% Define fitting function as CDF.
fun = @(x,xdata)normcdf(xdata,x(1),x(2));
x0 = [0.001;0.05];

for fibIDX = 1:numfibers
    if ~all(fiberFE(:,fibIDX) == 0)
        x0(1) = Irange(1);
        x0(2) = 0.05*Irange(1);
        % Find least squares fit for individual fibers.
        x = lsqcurvefit(fun,x0,Irange,squeeze(fiberFE(:,fibIDX)));
        fiberThresh(fibIDX) = x(1);
        fiberRS(fibIDX) = x(2);
        if fiberThresh(fibIDX) > max(Irange)
            fiberThresh(fibIDX) = NaN;
            fiberRS(fibIDX) = NaN;
        end
    end
end % Fiber loop