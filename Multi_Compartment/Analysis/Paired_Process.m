dataDir = 'G:\My Drive\Rubinstein\RetrievedData\stoch_AN_based\scheduleTest';
load(sprintf('%s%sExpt.mat',dataDir,filesep));
load(sprintf('%s%sFiberPop01%sResultsOut.mat',dataDir,filesep,filesep));

%%
spikeData = reshape(allSpikeOut,size(allStims));
spikeNodeData = cell(size(allStims));
popFE = zeros(size(allStims));
fiberFE = zeros([size(allStims) length(fibPop{1}.diameters)]);
for stimIDX = 1:numel(spikeData)
    spikeNodeData{stimIDX} = recordingNodeSpikes(spikeData{stimIDX},fibPop{1},model,options);
    [I,J] = ind2sub(size(spikeData),stimIDX);
    [fiberFE(I,J,:),popFE(stimIDX)] = refractory_Pulse_FE(spikeNodeData{stimIDX});
end

for ipiIDX = 1:5
    plot(0.5:.5:5,popFE(:,ipiIDX)); hold on
end