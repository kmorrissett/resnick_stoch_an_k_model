function [fiberFE,popFE] = single_Pulse_FE(spikeNodeData)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
numFibers   = length(spikeNodeData);
numMonte    = size(spikeNodeData{1},1);
totalSpikes = nan(numFibers,1);

for fibIDX = 1:numFibers
    didSpike = isfinite(spikeNodeData{fibIDX});
    singleMonteSpikes   = sum(didSpike,2);
    findMultiples = find(singleMonteSpikes > 1);
    if any(findMultiples)
        badStimIDXs = mat2str(findMultiples);
        warning('Multiple spikes on at least 1 monte carlo trial. fibIDX: %d, stimIDXs: %s',fibIDX,badStimIDXs);
        singleMonteSpikes = logical(singleMonteSpikes);
    end
    totalSpikes(fibIDX)         = sum(singleMonteSpikes > 0);
end

fiberFE = totalSpikes/numMonte;
popFE   = sum(totalSpikes)/(numFibers*numMonte);
