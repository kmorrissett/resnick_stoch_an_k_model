numFibers = size(voltNodeData,1);
numMontes = size(voltNodeData{1},1);
voltTime = time(1:options.Vsample/model.dt:end);

for fibIDX = 1:numFibers
    theseVolts = voltNodeData{fibIDX};
    for monteIDX = 1:numMontes
        LineStyle = {'LineWidth',1,'Color','k'};
        plot(voltTime,theseVolts(monteIDX,:),LineStyle{:}); hold on;
    end
end
xlim([0,3000]);
xlabel('Time (\mus)');
ylabel('Membrane Potential (mV)');
set(gca,'FontSize',16)
set(gcf,'Units','centimeter','pos',[5,5,18,10])