function [options,initiationNode,initiationLocation] = initiation_Location(options,model,Fibers,spikesData)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

options.XrecordLoc = -(model.LtoD*diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanRecordNode;
options.recordNodes = round(options.XrecordLoc./-(model.LtoD.*Fibers.diameters*1e-3/...
    model.normdtoD+model.nLen));
nodetoNodeLengths = (model.LtoD.*Fibers.diameters*1e-3/...
    model.normdtoD+model.nLen);

numFibers = length(Fibers.diameters);
initiationNode = cell(numFibers,1);
for fibIDX = 1:numFibers
    [~,initiationNode{fibIDX}] = min(spikesData{fibIDX},[],2,'omitnan');
    initiationNode{fibIDX}(initiationNode{fibIDX} == 1) = nan;
    meanInitiation(fibIDX,:) = mean(initiationNode{fibIDX},1,'omitnan');
end
initiationLocation = repmat(nodetoNodeLengths,5,1).'.*meanInitiation;


end

