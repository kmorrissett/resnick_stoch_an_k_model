function [fiberFE,popFE,spikeNodeData] = Single_Pulse_Fibers_Process(allStims,fibPop,model,options,stimOptions,allSpikeOut)

spikeData = reshape(allSpikeOut,size(allStims,1),1);
spikeNodeData = cell(size(allStims,1),1);
popFE = zeros(size(allStims,1),1);
numFibers = length(fibPop{1}.diameters);
fiberFE = zeros([size(allStims,1), numFibers]);
for stimIDX = 1:numel(spikeData)
    spikeNodeData{stimIDX} = recordingNodeSpikes(spikeData{stimIDX},fibPop{1},model,options);
    [I,J] = ind2sub(size(spikeData),stimIDX);
    [fiberFE(I,J,:),popFE(stimIDX)] = single_Pulse_FE(spikeNodeData{stimIDX});
end
for fibIDX = 1:numFibers
    [fiberThresh(fibIDX),fiberRS(fibIDX)] = CDF_thresh_fit(fiberFE,stimOptions.I{fibIDX},[0.005,0.05]);
end

for stimIDX = [1:10:61,66]
    spikeTimes = [];
    for fibIDX =  1:numFibers
        spikeTimes = [spikeTimes, spikeNodeData{stimIDX}{fibIDX}];
    end
    figure
    histogram(spikeTimes,'BinLimits',[500,1120],'NumBin',50)
end

for fibIDX = 1:numFibers
    logStims = 20*log10(stimOptions.I{fibIDX}/0.001);
    plot(logStims,fiberFE(:,1,fibIDX),'LineWidth',1.5); hold on
end
ylabel('Firing Efficiency'); ylim([-.1,1.1])
xlabel('Stimulus Current (db re 1\muA)');xlim([-inf,inf])