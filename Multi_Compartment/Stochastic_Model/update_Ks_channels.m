function numKs = update_Ks_channels(ratesKs,numKs,dt)
% NUMKS: This function updates the number of channels in each state for the
% next timestep using a Jump Monte Carlo method.
%
% Usage: numKs = initialize_Ks_channels(ratesKs,numKs,dt)
%
%   numKs:      Array of # of channels in each state
%   ratesKs:    Voltage dependent transition rates 
%   dt:         Timestep (us)
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

zeta = [ratesKs(1);ratesKs(2)];

Tl = 0;

p_trans_curr = [0,1,2];
p_trans_next = [0,2,1];

while   (Tl<=dt)
    
    lambda = numKs*zeta;
    
    tl = -log(rand) / lambda;
    Tl = Tl + tl;
    
    if (Tl>dt)
        break;
    end
    
    P = cumsum([0,...
        ratesKs(1)*numKs(1),...
        ratesKs(2)*numKs(2)...
        ]/lambda);
    
    ind = find(rand<P,1);
    
    numKs(p_trans_curr(ind)) = numKs(p_trans_curr(ind)) - 1;
    numKs(p_trans_next(ind)) = numKs(p_trans_next(ind)) + 1;
end

