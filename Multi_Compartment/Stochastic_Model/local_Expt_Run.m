function [spikeOutput,vOutput] = local_Expt_Run(Fibers,allStims,varargin)
%JOB_Run Runs a Fiber population through allStims cell array stimuli using
%model and options parameters and save spikeOutput and, if recording,
%vOutput data to saveFile.

%   OUTPUTS:
%   spikeOutput- Cell array of spike time data with same dimensions as
%   allStims. Each element in array contains cell array of size numFibers x
%   1. Each of these cells contains a double array that is numMontes x
%   numNodes in fiber x options.maxSpikes. These inner arrays containing
%   the spike time data for all nodes in a fiber for all monte carlo sims.
%
%   vOutput- similar structure to above except inner arrays are numMontes x
%   numNodes x dt/voltageSample*simTime.
%
%   REQUIRED INPUTS:
%   Fibers: Structure containing arrays of structural parameters for
%   collection of fibers. See initialize_Population function for details.
%
%   allStims: cell array containing stimulus waveforms. Accepts any
%   dimensionality.
%
%   OPTIONAL INPUTS:
%   Model: simulation model parameters. See model_Config function
%
%   options: simulation optional parameters. See options_Config function
%
%   saveFile: save results to this filename.

p = inputParser;
addRequired(p,'Fibers',@(x) isstruct(x))
addRequired(p,'allStims',@(x) iscell(x))
addOptional(p,'model',[],@(x) isstruct(x))
addOptional(p,'options',[],@(x) isstruct(x))
addOptional(p,'saveFile',[],@(x) isstring(x))
parse(p,Fibers,allStims,varargin{:})

if isempty(p.Results.model)
    model = model_Config;
else
    model = p.Results.model;
end

if isempty(p.Results.options)
    options = options_Config;
else
    options = p.Results.options;
end

%% Run the simulations
stimNum = numel(allStims);
spikeOutput = cell(size(allStims));

if options.recV; vOutput = cell(size(allStims)); end

for stimIDX = 1:stimNum
    Istim = allStims{stimIDX};
    if options.recV
        [spikeOutput{stimIDX},vOutput{stimIDX}] = Population_Run(Fibers,Istim,model,options);
    else
        spikeOutput{stimIDX} = Population_Run(Fibers,Istim,model,options);
    end
end

if ~isempty(saveFile)
    if options.recV
        save(saveFile,'spikeOutput','vOutput');
    else
        save(saveFile,'spikeOutput');
    end
end