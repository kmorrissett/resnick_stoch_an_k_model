function ratesK = trans_rates_K(V,Kins)
% trans_rates_K: This function calculates the potassium channel transition
% rates for a given voltage.
%
% Usage: ratesK = trans_rates_K(V,Kins);
%
%   ratesK:    Voltage dependent transition rates 
%   V:          Segment membrane potential (mV)
%   Kins:       Array containing kinematic parameters.
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

ratesK = zeros(2,length(V));
ratesK(1,:) = Kins(1) * (V - Kins(2)) ./...       % a_m
    (1 - exp((Kins(2) - V) / Kins(3)));          
ratesK(2,:) = Kins(4) * (Kins(5) - V) ./...      %b_m
    (1 - exp((V - Kins(5)) / Kins(6)));
end

