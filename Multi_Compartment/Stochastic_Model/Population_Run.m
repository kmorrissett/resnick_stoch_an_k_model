function [spikeOutput,vOutput,eCAPout] = Population_Run(Fibers,Istim,model,options,myFibs)
%POPULATION_RUN: This function accepts a structure containing fiber
%population parameters (Fibers), a 1D array containing the electrode
%current vector, and experiment model and options structures. It then runs
%all of the individual fibers in the population through stochAN_multi using
%the supplied Istim, model, and options.
%
% USAGE: [spikeOutput,vOutput] = Population_Run(Fibers,Istim,model,options);
%
% spikeOutput:  Cell array of arrays of spiketimes for each fiber segment.
% vOutput:      Cell array of arrays of voltages for each fiber segment for
%               each time step. Only generated when options.recV = 1.
% Fibers:       Structure containing arrays of geometric parameters for
%               individual fibers.
% Istim:        Array of stimulus currents for each timestep.
% model:        Structure containing model parameters.
% options:      Structure containing simulation optional arguments.
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

%% Set non-changing fiber parameters
options.XstimLoc = -(model.LtoD*Fibers.diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanNodeAbove;
options.eCAPxLoc = options.XstimLoc;    % ecap recording x-location
%% Unpack Fibers for parfor loop
if ~exist('myFibs','var'); myFibs = 1:length(Fibers.diameters); end
numFibers = length(myFibs);

diameterList = Fibers.diameters(myFibs);
dtoDList = Fibers.dtoD(myFibs);
numNodeList = Fibers.numNodes(myFibs);
zsList = Fibers.zs(myFibs);
intSegsList = Fibers.intSegs(myFibs);

%% Initialize output arrays
spikeOutput = cell(numFibers,1); 
vOutput = cell(numFibers,1);    % Empty cell if unused
tempeCAPs = cell(numFibers,1);    % Empty cell if unused

model = orderfields(model);
options = orderfields(options);

% tic;
%% Run simulation for each fiber in loop
parfor fibIDX = 1:numFibers
    diameter = diameterList(fibIDX);  % axolemma diameter (um)
    dtoD = dtoDList(fibIDX);   % g-ratio for healthy reg.
    numNodes = numNodeList(fibIDX);   % nodes in fiber
    zDistance = zsList(fibIDX);        % distance b/w fiber & electrode (mm)
    intSegs = intSegsList(fibIDX);
    
    fiber = struct('diameter',diameter,'dtoD',dtoD,'numNodes',numNodes,...
        'zDistance',zDistance,'intSegs',intSegs);
    % fiber.cutoff = [];            % PLACEHOLDER: demyelination extent
    % fiber.demydtoD = [];          % PLACEHOLDER: demyelination severity
    
    %% Calculate segment properties from fiber and model info
    segments = fiber_initialization(fiber,model);
    
    %% Run model
    % Need consistent field order for MEX function.
    segments = orderfields(segments);
    
    if options.recV && options.recECAP %#ok<PFBNS>
        [spikeOutput{fibIDX},vOutput{fibIDX},tempeCAPs{fibIDX}] = stochAN_multi_mex(Istim,segments,model,options);
    elseif options.recV
        [spikeOutput{fibIDX},vOutput{fibIDX}] = stochAN_multi_mex(Istim,segments,model,options);
    elseif options.recECAP
        [spikeOutput{fibIDX},~,tempeCAPs{fibIDX}] = stochAN_multi_mex(Istim,segments,model,options);
    else
        spikeOutput{fibIDX} = stochAN_multi_mex(Istim,segments,model,options);
    end
end
eCAPout = zeros(options.numMonte,length(Istim));
for fibIDX = 1:numFibers
    eCAPout = eCAPout + tempeCAPs{fibIDX};
end
% toc;
end

