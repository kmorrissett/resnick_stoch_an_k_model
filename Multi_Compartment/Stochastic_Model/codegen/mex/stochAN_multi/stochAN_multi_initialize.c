/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * stochAN_multi_initialize.c
 *
 * Code generation for function 'stochAN_multi_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "stochAN_multi_initialize.h"
#include "_coder_stochAN_multi_mex.h"
#include "stochAN_multi_data.h"

/* Function Declarations */
static void stochAN_multi_once(void);

/* Function Definitions */
static void stochAN_multi_once(void)
{
  static const int32_T lineInfo[134] = { 19, 22, 23, 24, 25, 26, 27, 28, 29, 30,
    31, 33, 34, 35, 36, 37, 39, 40, 41, 42, 43, 44, 46, 47, 48, 50, 51, 54, 55,
    58, 59, 60, 61, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 77, 78, 79, 80, 81,
    82, 86, 87, 88, 89, 93, 94, 95, 97, 99, 100, 101, 102, 105, 106, 107, 109,
    110, 112, 113, 114, 115, 117, 118, 119, 122, 123, 124, 126, 127, 129, 130,
    131, 132, 133, 134, 135, 137, 138, 139, 140, 144, 145, 146, 149, 150, 151,
    152, 153, 156, 157, 158, 159, 162, 165, 166, 167, 168, 169, 170, 171, 172,
    173, 174, 175, 178, 179, 180, 181, 182, 184, 185, 186, 187, 188, 189, 191,
    192, 193, 194, 195, 196, 198, 199, 200, 201 };

  int32_T i0;
  int32_T iv1[10];
  int32_T iv2[6];
  static const int32_T b_lineInfo[46] = { 15, 17, 20, 21, 22, 23, 24, 25, 26, 27,
    29, 32, 33, 35, 37, 39, 40, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54,
    55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 68, 70, 71, 72, 74 };

  static const int32_T c_lineInfo[31] = { 15, 17, 20, 21, 22, 23, 24, 26, 29, 30,
    32, 34, 36, 37, 39, 40, 41, 43, 44, 45, 46, 47, 48, 49, 50, 51, 53, 55, 56,
    57, 59 };

  static const int32_T d_lineInfo[21] = { 15, 17, 20, 22, 25, 26, 28, 30, 32, 33,
    35, 36, 37, 39, 40, 41, 43, 45, 46, 47, 49 };

  static const int32_T e_lineInfo[44] = { 13, 14, 15, 16, 17, 18, 19, 20, 22, 24,
    25, 27, 29, 31, 32, 34, 35, 36, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
    49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 63, 64, 65 };

  static const int32_T f_lineInfo[30] = { 13, 14, 15, 16, 17, 18, 20, 22, 23, 25,
    27, 29, 30, 32, 33, 34, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 47, 49, 50,
    51 };

  static const int32_T g_lineInfo[19] = { 13, 15, 17, 18, 20, 22, 24, 25, 27, 28,
    29, 31, 32, 33, 34, 36, 38, 39, 40 };

  static const int32_T h_lineInfo[14] = { 14, 15, 16, 17, 18, 21, 22, 23, 24, 25,
    28, 29, 30, 32 };

  tridag_complete_name =
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m>tridag(codegen)";
  c_update_Ks_channels_complete_n =
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Ks_channels.m>update_K"
    "s_channels(codegen)";
  c_update_Kf_channels_complete_n =
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Kf_channels.m>update_K"
    "f_channels(codegen)";
  c_update_Naf_channels_complete_ =
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Naf_channels.m>update_"
    "Naf_channels(codegen)";
  c_initialize_Ks_channels_comple =
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Ks_channels.m>init"
    "ialize_Ks_channels(codegen)";
  c_initialize_Kf_channels_comple =
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Kf_channels.m>init"
    "ialize_Kf_channels(codegen)";
  c_initialize_Naf_channels_compl =
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Naf_channels.m>ini"
    "tialize_Naf_channels(codegen)";
  trans_rates_K_complete_name =
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m>trans_rates_K"
    "(codegen)";
  trans_rates_Naf_complete_name =
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m>trans_rates"
    "_Naf(codegen)";
  stochAN_multi_complete_name =
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m>stochAN_multi"
    "(codegen)";
  isMexOutdated = emlrtProfilerCheckMEXOutdated();
  emlrtProfilerRegisterMEXFcn(stochAN_multi_complete_name,
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",
    "stochAN_multi", 134, lineInfo, isMexOutdated);
  for (i0 = 0; i0 < 10; i0++) {
    iv1[i0] = 13 + i0;
  }

  emlrtProfilerRegisterMEXFcn(trans_rates_Naf_complete_name,
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m",
    "trans_rates_Naf", 10, iv1, isMexOutdated);
  for (i0 = 0; i0 < 6; i0++) {
    iv2[i0] = 13 + i0;
  }

  emlrtProfilerRegisterMEXFcn(trans_rates_K_complete_name,
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m",
    "trans_rates_K", 6, iv2, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(c_initialize_Naf_channels_compl,
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Naf_channels.m",
    "initialize_Naf_channels", 46, b_lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(c_initialize_Kf_channels_comple,
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Kf_channels.m",
    "initialize_Kf_channels", 31, c_lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(c_initialize_Ks_channels_comple,
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Ks_channels.m",
    "initialize_Ks_channels", 21, d_lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(c_update_Naf_channels_complete_,
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Naf_channels.m",
    "update_Naf_channels", 44, e_lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(c_update_Kf_channels_complete_n,
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Kf_channels.m",
    "update_Kf_channels", 30, f_lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(c_update_Ks_channels_complete_n,
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Ks_channels.m",
    "update_Ks_channels", 19, g_lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(tridag_complete_name,
    "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",
    "tridag", 14, h_lineInfo, isMexOutdated);
}

void stochAN_multi_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  if (emlrtFirstTimeR2012b(emlrtRootTLSGlobal)) {
    stochAN_multi_once();
  }
}

/* End of code generation (stochAN_multi_initialize.c) */
