/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * initialize_Kf_channels.h
 *
 * Code generation for function 'initialize_Kf_channels'
 *
 */

#ifndef INITIALIZE_KF_CHANNELS_H
#define INITIALIZE_KF_CHANNELS_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
extern void initialize_Kf_channels(const emlrtStack *sp, const real_T ratesKf[2],
  real_T numK, real_T dt, real_T numKf[5]);

#endif

/* End of code generation (initialize_Kf_channels.h) */
