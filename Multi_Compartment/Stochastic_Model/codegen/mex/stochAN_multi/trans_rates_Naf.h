/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * trans_rates_Naf.h
 *
 * Code generation for function 'trans_rates_Naf'
 *
 */

#ifndef TRANS_RATES_NAF_H
#define TRANS_RATES_NAF_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
extern void trans_rates_Naf(const emlrtStack *sp, const emxArray_real_T *V,
  const real_T Kins[12], emxArray_real_T *ratesNaf);

#endif

/* End of code generation (trans_rates_Naf.h) */
