/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * tridag.h
 *
 * Code generation for function 'tridag'
 *
 */

#ifndef TRIDAG_H
#define TRIDAG_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
extern void tridag(const emlrtStack *sp, const emxArray_real_T *a, const
                   emxArray_real_T *b, const emxArray_real_T *c, const
                   emxArray_real_T *rhs, emxArray_real_T *Vnext);

#endif

/* End of code generation (tridag.h) */
