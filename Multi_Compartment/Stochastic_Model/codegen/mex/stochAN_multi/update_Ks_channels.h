/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * update_Ks_channels.h
 *
 * Code generation for function 'update_Ks_channels'
 *
 */

#ifndef UPDATE_KS_CHANNELS_H
#define UPDATE_KS_CHANNELS_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
extern void update_Ks_channels(const emlrtStack *sp, const real_T ratesKs[2],
  real_T numKs[2], real_T dt);

#endif

/* End of code generation (update_Ks_channels.h) */
