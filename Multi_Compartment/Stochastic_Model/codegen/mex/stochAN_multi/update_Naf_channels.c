/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * update_Naf_channels.c
 *
 * Code generation for function 'update_Naf_channels'
 *
 */

/* Include files */
#include <string.h>
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "update_Naf_channels.h"
#include "error.h"
#include "stochAN_multi_data.h"

/* Variable Definitions */
static emlrtRSInfo tc_emlrtRSI = { 31, /* lineNo */
  "update_Naf_channels",               /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Naf_channels.m"/* pathName */
};

static emlrtRSInfo uc_emlrtRSI = { 61, /* lineNo */
  "update_Naf_channels",               /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Naf_channels.m"/* pathName */
};

static emlrtBCInfo af_emlrtBCI = { 1,  /* iFirst */
  8,                                   /* iLast */
  63,                                  /* lineNo */
  11,                                  /* colNo */
  "numNa",                             /* aName */
  "update_Naf_channels",               /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Naf_channels.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo bf_emlrtBCI = { 1,  /* iFirst */
  8,                                   /* iLast */
  64,                                  /* lineNo */
  11,                                  /* colNo */
  "numNa",                             /* aName */
  "update_Naf_channels",               /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Naf_channels.m",/* pName */
  3                                    /* checkKind */
};

/* Function Definitions */
void update_Naf_channels(const emlrtStack *sp, const real_T ratesNaf[4], real_T
  numNa[8], real_T dt)
{
  real_T zeta[8];
  real_T Tl;
  boolean_T exitg1;
  real_T lambda;
  int32_T idx;
  real_T x;
  real_T P[21];
  int32_T ii_size_idx_1;
  int32_T ii;
  boolean_T exitg2;
  int8_T ii_data[1];
  int8_T ind_data[1];
  static const int8_T iv16[21] = { 0, 1, 2, 2, 3, 3, 4, 1, 5, 2, 6, 3, 7, 4, 8,
    5, 6, 6, 7, 7, 8 };

  real_T numNa_data[1];
  static const int8_T iv17[21] = { 0, 2, 1, 3, 2, 4, 3, 5, 1, 6, 2, 7, 3, 8, 4,
    6, 5, 7, 6, 8, 7 };

  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtMEXProfilingFunctionEntry(c_update_Naf_channels_complete_, isMexOutdated);

  /*  NUMNAF: This function updates the number of channels in each state for the */
  /*  next timestep using a Jump Monte Carlo method. */
  /*  */
  /*  Usage: numNaf = initialize_Naf_channels(ratesNaf,numNaf,dt) */
  /*  */
  /*    numNaf:      Array of # of channels in each state */
  /*    ratesNaf:    Voltage dependent transition rates  */
  /*    dt:         Timestep (us) */
  /*  */
  /*  Jesse M. Resnick (resnick@uw.edu) � 2018 */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  emlrtMEXProfilingStatement(2, isMexOutdated);
  emlrtMEXProfilingStatement(3, isMexOutdated);
  emlrtMEXProfilingStatement(4, isMexOutdated);
  emlrtMEXProfilingStatement(5, isMexOutdated);
  emlrtMEXProfilingStatement(6, isMexOutdated);
  emlrtMEXProfilingStatement(7, isMexOutdated);
  emlrtMEXProfilingStatement(8, isMexOutdated);
  zeta[0] = 3.0 * ratesNaf[0] + ratesNaf[2];
  zeta[1] = (ratesNaf[1] + ratesNaf[2]) + 2.0 * ratesNaf[0];
  zeta[2] = (2.0 * ratesNaf[1] + ratesNaf[2]) + ratesNaf[0];
  zeta[3] = 3.0 * ratesNaf[1] + ratesNaf[2];
  zeta[4] = 3.0 * ratesNaf[0] + ratesNaf[3];
  zeta[5] = (ratesNaf[1] + ratesNaf[3]) + 2.0 * ratesNaf[0];
  zeta[6] = (2.0 * ratesNaf[1] + ratesNaf[3]) + ratesNaf[0];
  zeta[7] = 3.0 * ratesNaf[1] + ratesNaf[3];
  emlrtMEXProfilingStatement(9, isMexOutdated);
  Tl = 0.0;
  emlrtMEXProfilingStatement(10, isMexOutdated);
  emlrtMEXProfilingStatement(11, isMexOutdated);
  emlrtMEXProfilingStatement(12, isMexOutdated);
  exitg1 = false;
  while ((!exitg1) && (Tl <= dt)) {
    emlrtMEXProfilingStatement(13, isMexOutdated);
    lambda = 0.0;
    for (idx = 0; idx < 8; idx++) {
      lambda += numNa[idx] * zeta[idx];
    }

    emlrtMEXProfilingStatement(14, isMexOutdated);
    st.site = &tc_emlrtRSI;
    emlrtRandu(&x, 1);
    st.site = &tc_emlrtRSI;
    if (x < 0.0) {
      b_st.site = &oc_emlrtRSI;
      d_error(&b_st);
    }

    x = muDoubleScalarLog(x);
    emlrtMEXProfilingStatement(15, isMexOutdated);
    Tl += -x / lambda;
    emlrtMEXProfilingStatement(16, isMexOutdated);
    if (Tl > dt) {
      emlrtMEXProfilingStatement(17, isMexOutdated);
      exitg1 = true;
    } else {
      emlrtMEXProfilingStatement(19, isMexOutdated);
      emlrtMEXProfilingStatement(20, isMexOutdated);
      emlrtMEXProfilingStatement(21, isMexOutdated);
      emlrtMEXProfilingStatement(22, isMexOutdated);
      emlrtMEXProfilingStatement(23, isMexOutdated);
      emlrtMEXProfilingStatement(24, isMexOutdated);
      emlrtMEXProfilingStatement(25, isMexOutdated);
      emlrtMEXProfilingStatement(26, isMexOutdated);
      emlrtMEXProfilingStatement(27, isMexOutdated);
      emlrtMEXProfilingStatement(28, isMexOutdated);
      emlrtMEXProfilingStatement(29, isMexOutdated);
      emlrtMEXProfilingStatement(30, isMexOutdated);
      emlrtMEXProfilingStatement(31, isMexOutdated);
      emlrtMEXProfilingStatement(32, isMexOutdated);
      emlrtMEXProfilingStatement(33, isMexOutdated);
      emlrtMEXProfilingStatement(34, isMexOutdated);
      emlrtMEXProfilingStatement(35, isMexOutdated);
      emlrtMEXProfilingStatement(36, isMexOutdated);
      emlrtMEXProfilingStatement(37, isMexOutdated);
      emlrtMEXProfilingStatement(38, isMexOutdated);
      emlrtMEXProfilingStatement(39, isMexOutdated);
      emlrtMEXProfilingStatement(40, isMexOutdated);
      P[0] = 0.0 / lambda;
      P[1] = 3.0 * ratesNaf[0] * numNa[0] / lambda;
      P[2] = ratesNaf[1] * numNa[1] / lambda;
      P[3] = 2.0 * ratesNaf[0] * numNa[1] / lambda;
      P[4] = 2.0 * ratesNaf[1] * numNa[2] / lambda;
      P[5] = ratesNaf[0] * numNa[2] / lambda;
      P[6] = 3.0 * ratesNaf[1] * numNa[3] / lambda;
      P[7] = ratesNaf[2] * numNa[0] / lambda;
      P[8] = ratesNaf[3] * numNa[4] / lambda;
      P[9] = ratesNaf[2] * numNa[1] / lambda;
      P[10] = ratesNaf[3] * numNa[5] / lambda;
      P[11] = ratesNaf[2] * numNa[2] / lambda;
      P[12] = ratesNaf[3] * numNa[6] / lambda;
      P[13] = ratesNaf[2] * numNa[3] / lambda;
      P[14] = ratesNaf[3] * numNa[7] / lambda;
      P[15] = 3.0 * ratesNaf[0] * numNa[4] / lambda;
      P[16] = ratesNaf[1] * numNa[5] / lambda;
      P[17] = 2.0 * ratesNaf[0] * numNa[5] / lambda;
      P[18] = 2.0 * ratesNaf[1] * numNa[6] / lambda;
      P[19] = ratesNaf[0] * numNa[6] / lambda;
      P[20] = 3.0 * ratesNaf[1] * numNa[7] / lambda;
      for (idx = 0; idx < 20; idx++) {
        P[idx + 1] += P[idx];
      }

      emlrtMEXProfilingStatement(41, isMexOutdated);
      st.site = &uc_emlrtRSI;
      emlrtRandu(&lambda, 1);
      idx = 0;
      ii_size_idx_1 = 1;
      ii = 0;
      exitg2 = false;
      while ((!exitg2) && (ii < 21)) {
        if (lambda < P[ii]) {
          idx = 1;
          ii_data[0] = (int8_T)(ii + 1);
          exitg2 = true;
        } else {
          ii++;
        }
      }

      if (idx == 0) {
        ii_size_idx_1 = 0;
      }

      if (0 <= ii_size_idx_1 - 1) {
        memcpy(&ind_data[0], &ii_data[0], (uint32_T)(ii_size_idx_1 * (int32_T)
                sizeof(int8_T)));
      }

      emlrtMEXProfilingStatement(42, isMexOutdated);
      for (idx = 0; idx < ii_size_idx_1; idx++) {
        ii = ind_data[0] - 1;
        if (iv16[ii] < 1) {
          emlrtDynamicBoundsCheckR2012b(0, 1, 8, &af_emlrtBCI, sp);
        }

        ii_data[0] = iv16[ii];
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numNa_data[0] = numNa[ii_data[0] - 1] - 1.0;
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numNa[ii_data[0] - 1] = numNa_data[0];
      }

      emlrtMEXProfilingStatement(43, isMexOutdated);
      for (idx = 0; idx < ii_size_idx_1; idx++) {
        ii = ind_data[0] - 1;
        if (iv17[ii] < 1) {
          emlrtDynamicBoundsCheckR2012b(0, 1, 8, &bf_emlrtBCI, sp);
        }

        ii_data[0] = iv17[ii];
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numNa_data[0] = numNa[ii_data[0] - 1] + 1.0;
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numNa[ii_data[0] - 1] = numNa_data[0];
      }

      emlrtMEXProfilingStatement(44, isMexOutdated);
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }
  }

  emlrtMEXProfilingFunctionExit(isMexOutdated);
}

/* End of code generation (update_Naf_channels.c) */
