/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * update_Naf_channels.h
 *
 * Code generation for function 'update_Naf_channels'
 *
 */

#ifndef UPDATE_NAF_CHANNELS_H
#define UPDATE_NAF_CHANNELS_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
extern void update_Naf_channels(const emlrtStack *sp, const real_T ratesNaf[4],
  real_T numNa[8], real_T dt);

#endif

/* End of code generation (update_Naf_channels.h) */
