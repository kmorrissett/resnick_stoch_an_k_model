MATLAB_ROOT = /Applications/MATLAB_R2018b.app
MAKEFILE = stochAN_multi_mex.mk

include stochAN_multi_mex.mki


SRC_FILES =  \
	stochAN_multi_data.c \
	stochAN_multi_initialize.c \
	stochAN_multi_terminate.c \
	stochAN_multi.c \
	power.c \
	eml_int_forloop_overflow_check.c \
	error.c \
	sqrt.c \
	rdivide_helper.c \
	asin.c \
	toLogicalCheck.c \
	repmat.c \
	trans_rates_Naf.c \
	exp.c \
	trans_rates_K.c \
	initialize_Naf_channels.c \
	initialize_Kf_channels.c \
	initialize_Ks_channels.c \
	update_Naf_channels.c \
	update_Kf_channels.c \
	update_Ks_channels.c \
	tridag.c \
	any.c \
	error1.c \
	mod.c \
	_coder_stochAN_multi_info.c \
	_coder_stochAN_multi_api.c \
	_coder_stochAN_multi_mex.c \
	stochAN_multi_emxutil.c \
	c_mexapi_version.c

MEX_FILE_NAME_WO_EXT = stochAN_multi_mex
MEX_FILE_NAME = $(MEX_FILE_NAME_WO_EXT).mexmaci64
TARGET = $(MEX_FILE_NAME)

SYS_LIBS = -lmwblas 


#
#====================================================================
# gmake makefile fragment for building MEX functions using Unix
# Copyright 2007-2016 The MathWorks, Inc.
#====================================================================
#

OBJEXT = o
.SUFFIXES: .$(OBJEXT)

OBJLISTC = $(SRC_FILES:.c=.$(OBJEXT))
OBJLISTCPP  = $(OBJLISTC:.cpp=.$(OBJEXT))
OBJLIST  = $(OBJLISTCPP:.cu=.$(OBJEXT))

target: $(TARGET)

ML_INCLUDES = -I "$(MATLAB_ROOT)/simulink/include"
ML_INCLUDES+= -I "$(MATLAB_ROOT)/toolbox/shared/simtargets"
SYS_INCLUDE = $(ML_INCLUDES)

# Additional includes

SYS_INCLUDE += -I "/Users/kal/Desktop/Rubinstein\ Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/codegen/mex/stochAN_multi"
SYS_INCLUDE += -I "/Users/kal/Desktop/Rubinstein\ Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model"
SYS_INCLUDE += -I "./interface"
SYS_INCLUDE += -I "$(MATLAB_ROOT)/extern/include"
SYS_INCLUDE += -I "."

EML_LIBS = -lemlrt -lcovrt -lut -lmwmathutil 
SYS_LIBS += $(CLIBS) $(EML_LIBS)


EXPORTFILE = $(MEX_FILE_NAME_WO_EXT)_mex.map
ifeq ($(Arch),maci)
  EXPORTOPT = -Wl,-exported_symbols_list,$(EXPORTFILE)
  COMP_FLAGS = -c $(CFLAGS)
  CXX_FLAGS = -c $(CXXFLAGS)
  LINK_FLAGS = $(filter-out %mexFunction.map, $(LDFLAGS))
else ifeq ($(Arch),maci64)
  EXPORTOPT = -Wl,-exported_symbols_list,$(EXPORTFILE)
  COMP_FLAGS = -c $(CFLAGS)
  CXX_FLAGS = -c $(CXXFLAGS)
  LINK_FLAGS = $(filter-out %mexFunction.map, $(LDFLAGS)) -Wl,-rpath,@loader_path
else
  EXPORTOPT = -Wl,--version-script,$(EXPORTFILE)
  COMP_FLAGS = -c $(CFLAGS) $(OMPFLAGS)
  CXX_FLAGS = -c $(CXXFLAGS) $(OMPFLAGS)
  LINK_FLAGS = $(filter-out %mexFunction.map, $(LDFLAGS)) 
endif
LINK_FLAGS += $(OMPLINKFLAGS)
ifeq ($(Arch),maci)
  LINK_FLAGS += -L$(MATLAB_ROOT)/sys/os/maci
endif
ifeq ($(EMC_CONFIG),optim)
  ifeq ($(Arch),mac)
    COMP_FLAGS += $(CDEBUGFLAGS)
    CXX_FLAGS += $(CXXDEBUGFLAGS)
  else
    COMP_FLAGS += $(COPTIMFLAGS)
    CXX_FLAGS += $(CXXOPTIMFLAGS)
  endif
  LINK_FLAGS += $(LDOPTIMFLAGS)
else
  COMP_FLAGS += $(CDEBUGFLAGS)
  CXX_FLAGS += $(CXXDEBUGFLAGS)
  LINK_FLAGS += $(LDDEBUGFLAGS)
endif
LINK_FLAGS += -o $(TARGET)
LINK_FLAGS +=  -L"$(MATLAB_ROOT)/bin/maci64"

CCFLAGS = $(COMP_FLAGS) -std=c99   $(USER_INCLUDE) $(SYS_INCLUDE)
CPPFLAGS = $(CXX_FLAGS) -std=c++11   $(USER_INCLUDE) $(SYS_INCLUDE)

%.$(OBJEXT) : %.c
	$(CC) $(CCFLAGS) "$<"

%.$(OBJEXT) : %.cpp
	$(CXX) $(CPPFLAGS) "$<"

# Additional sources

%.$(OBJEXT) : /Users/kal/Desktop/Rubinstein\ Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/%.c
	$(CC) $(CCFLAGS) "$<"

%.$(OBJEXT) : /Users/kal/Desktop/Rubinstein\ Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/codegen/mex/stochAN_multi/%.c
	$(CC) $(CCFLAGS) "$<"

%.$(OBJEXT) : interface/%.c
	$(CC) $(CCFLAGS) "$<"



%.$(OBJEXT) : /Users/kal/Desktop/Rubinstein\ Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/%.cpp
	$(CXX) $(CPPFLAGS) "$<"

%.$(OBJEXT) : /Users/kal/Desktop/Rubinstein\ Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/codegen/mex/stochAN_multi/%.cpp
	$(CXX) $(CPPFLAGS) "$<"

%.$(OBJEXT) : interface/%.cpp
	$(CXX) $(CPPFLAGS) "$<"



%.$(OBJEXT) : /Users/kal/Desktop/Rubinstein\ Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/%.cu
	$(CC) $(CCFLAGS) "$<"

%.$(OBJEXT) : /Users/kal/Desktop/Rubinstein\ Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/codegen/mex/stochAN_multi/%.cu
	$(CC) $(CCFLAGS) "$<"

%.$(OBJEXT) : interface/%.cu
	$(CC) $(CCFLAGS) "$<"




$(TARGET): $(OBJLIST) $(MAKEFILE)
	$(LD) $(EXPORTOPT) $(OBJLIST) $(LINK_FLAGS) $(SYS_LIBS)

#====================================================================

