/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * initialize_Kf_channels.c
 *
 * Code generation for function 'initialize_Kf_channels'
 *
 */

/* Include files */
#include <string.h>
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "initialize_Kf_channels.h"
#include "error.h"
#include "stochAN_multi_data.h"

/* Variable Definitions */
static emlrtRSInfo pc_emlrtRSI = { 36, /* lineNo */
  "initialize_Kf_channels",            /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Kf_channels.m"/* pathName */
};

static emlrtRSInfo qc_emlrtRSI = { 53, /* lineNo */
  "initialize_Kf_channels",            /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Kf_channels.m"/* pathName */
};

/* Function Definitions */
void initialize_Kf_channels(const emlrtStack *sp, const real_T ratesKf[2],
  real_T numK, real_T dt, real_T numKf[5])
{
  int32_T idx;
  static const uint8_T a[5] = { 158U, 8U, 0U, 0U, 0U };

  real_T zeta[5];
  real_T Tl;
  boolean_T exitg1;
  real_T lambda;
  real_T x;
  real_T P[8];
  int32_T ii_size_idx_1;
  int32_T ii;
  boolean_T exitg2;
  int8_T ii_data[1];
  int8_T ind_data[1];
  static const int8_T iv9[8] = { 1, 2, 2, 3, 3, 4, 4, 5 };

  real_T numKf_data[1];
  static const int8_T iv10[8] = { 2, 1, 3, 2, 4, 3, 5, 4 };

  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtMEXProfilingFunctionEntry(c_initialize_Kf_channels_comple, isMexOutdated);

  /*  NUMKF: This function initializes the number of channels in each state */
  /*  using a Jump Monte Carlo method. */
  /*  */
  /*  Usage: numKf = initialize_Kf_channels(ratesKf,numK,dt) */
  /*  */
  /*    numKf:      Array of # of channels in each state */
  /*    ratesKf:    Voltage dependent transition rates */
  /*    numK:       Number of K channels at segment */
  /*    dt:         Timestep (us) */
  /*  */
  /*  Jesse M. Resnick (resnick@uw.edu) � 2018 */
  /*  initial destribution of particles in each channel */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  for (idx = 0; idx < 5; idx++) {
    numKf[idx] = muDoubleScalarRound((real_T)a[idx] * numK / 168.0);
  }

  /*  Net transition rates for each channel type. */
  emlrtMEXProfilingStatement(3, isMexOutdated);
  emlrtMEXProfilingStatement(4, isMexOutdated);
  emlrtMEXProfilingStatement(5, isMexOutdated);
  emlrtMEXProfilingStatement(6, isMexOutdated);
  emlrtMEXProfilingStatement(7, isMexOutdated);
  zeta[0] = 4.0 * ratesKf[0];
  zeta[1] = 3.0 * ratesKf[0] + ratesKf[1];
  zeta[2] = 2.0 * ratesKf[0] + 2.0 * ratesKf[1];
  zeta[3] = ratesKf[0] + 3.0 * ratesKf[1];
  zeta[4] = 4.0 * ratesKf[1];
  emlrtMEXProfilingStatement(8, isMexOutdated);
  Tl = 0.0;

  /*  Indices of channel types for different reaction types. */
  emlrtMEXProfilingStatement(9, isMexOutdated);
  emlrtMEXProfilingStatement(10, isMexOutdated);
  emlrtMEXProfilingStatement(11, isMexOutdated);
  exitg1 = false;
  while ((!exitg1) && (Tl <= 100.0 * dt)) {
    emlrtMEXProfilingStatement(12, isMexOutdated);
    lambda = 0.0;
    for (idx = 0; idx < 5; idx++) {
      lambda += numKf[idx] * zeta[idx];
    }

    emlrtMEXProfilingStatement(13, isMexOutdated);
    st.site = &pc_emlrtRSI;
    emlrtRandu(&x, 1);
    st.site = &pc_emlrtRSI;
    if (x < 0.0) {
      b_st.site = &oc_emlrtRSI;
      d_error(&b_st);
    }

    x = muDoubleScalarLog(x);
    emlrtMEXProfilingStatement(14, isMexOutdated);
    Tl += -x / lambda;
    emlrtMEXProfilingStatement(15, isMexOutdated);
    if (Tl > 100.0 * dt) {
      emlrtMEXProfilingStatement(16, isMexOutdated);
      exitg1 = true;
    } else {
      emlrtMEXProfilingStatement(18, isMexOutdated);
      emlrtMEXProfilingStatement(19, isMexOutdated);
      emlrtMEXProfilingStatement(20, isMexOutdated);
      emlrtMEXProfilingStatement(21, isMexOutdated);
      emlrtMEXProfilingStatement(22, isMexOutdated);
      emlrtMEXProfilingStatement(23, isMexOutdated);
      emlrtMEXProfilingStatement(24, isMexOutdated);
      emlrtMEXProfilingStatement(25, isMexOutdated);
      emlrtMEXProfilingStatement(26, isMexOutdated);
      P[0] = 4.0 * ratesKf[0] * numKf[0] / lambda;
      P[1] = ratesKf[1] * numKf[1] / lambda;
      P[2] = 3.0 * ratesKf[0] * numKf[1] / lambda;
      P[3] = 2.0 * ratesKf[1] * numKf[2] / lambda;
      P[4] = 2.0 * ratesKf[0] * numKf[2] / lambda;
      P[5] = 3.0 * ratesKf[1] * numKf[3] / lambda;
      P[6] = ratesKf[0] * numKf[3] / lambda;
      P[7] = 4.0 * ratesKf[1] * numKf[4] / lambda;
      for (idx = 0; idx < 7; idx++) {
        P[idx + 1] += P[idx];
      }

      emlrtMEXProfilingStatement(27, isMexOutdated);
      st.site = &qc_emlrtRSI;
      emlrtRandu(&lambda, 1);
      idx = 0;
      ii_size_idx_1 = 1;
      ii = 0;
      exitg2 = false;
      while ((!exitg2) && (ii < 8)) {
        if (lambda < P[ii]) {
          idx = 1;
          ii_data[0] = (int8_T)(ii + 1);
          exitg2 = true;
        } else {
          ii++;
        }
      }

      if (idx == 0) {
        ii_size_idx_1 = 0;
      }

      if (0 <= ii_size_idx_1 - 1) {
        memcpy(&ind_data[0], &ii_data[0], (uint32_T)(ii_size_idx_1 * (int32_T)
                sizeof(int8_T)));
      }

      emlrtMEXProfilingStatement(28, isMexOutdated);
      for (idx = 0; idx < ii_size_idx_1; idx++) {
        ii_data[0] = iv9[ind_data[0] - 1];
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKf_data[0] = numKf[ii_data[0] - 1] - 1.0;
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKf[ii_data[0] - 1] = numKf_data[0];
      }

      emlrtMEXProfilingStatement(29, isMexOutdated);
      for (idx = 0; idx < ii_size_idx_1; idx++) {
        ii_data[0] = iv10[ind_data[0] - 1];
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKf_data[0] = numKf[ii_data[0] - 1] + 1.0;
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKf[ii_data[0] - 1] = numKf_data[0];
      }

      emlrtMEXProfilingStatement(30, isMexOutdated);
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }
  }

  emlrtMEXProfilingStatement(31, isMexOutdated);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
}

/* End of code generation (initialize_Kf_channels.c) */
