/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * asin.h
 *
 * Code generation for function 'asin'
 *
 */

#ifndef ASIN_H
#define ASIN_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
extern void b_asin(const emlrtStack *sp, emxArray_real_T *x);

#endif

/* End of code generation (asin.h) */
