/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * trans_rates_Naf.c
 *
 * Code generation for function 'trans_rates_Naf'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "trans_rates_Naf.h"
#include "rdivide_helper.h"
#include "stochAN_multi_emxutil.h"
#include "exp.h"
#include "stochAN_multi_data.h"

/* Variable Definitions */
static emlrtRSInfo ac_emlrtRSI = { 15, /* lineNo */
  "trans_rates_Naf",                   /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pathName */
};

static emlrtRSInfo bc_emlrtRSI = { 14, /* lineNo */
  "trans_rates_Naf",                   /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pathName */
};

static emlrtRSInfo cc_emlrtRSI = { 17, /* lineNo */
  "trans_rates_Naf",                   /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pathName */
};

static emlrtRSInfo dc_emlrtRSI = { 16, /* lineNo */
  "trans_rates_Naf",                   /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pathName */
};

static emlrtRSInfo ec_emlrtRSI = { 19, /* lineNo */
  "trans_rates_Naf",                   /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pathName */
};

static emlrtRSInfo fc_emlrtRSI = { 18, /* lineNo */
  "trans_rates_Naf",                   /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pathName */
};

static emlrtRSInfo gc_emlrtRSI = { 21, /* lineNo */
  "trans_rates_Naf",                   /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pathName */
};

static emlrtRTEInfo gd_emlrtRTEI = { 13,/* lineNo */
  1,                                   /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo hd_emlrtRTEI = { 14,/* lineNo */
  12,                                  /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo id_emlrtRTEI = { 15,/* lineNo */
  10,                                  /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo jd_emlrtRTEI = { 15,/* lineNo */
  6,                                   /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo kd_emlrtRTEI = { 16,/* lineNo */
  12,                                  /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo ld_emlrtRTEI = { 17,/* lineNo */
  10,                                  /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo md_emlrtRTEI = { 17,/* lineNo */
  6,                                   /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo nd_emlrtRTEI = { 18,/* lineNo */
  12,                                  /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo od_emlrtRTEI = { 19,/* lineNo */
  10,                                  /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo pd_emlrtRTEI = { 19,/* lineNo */
  6,                                   /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo qd_emlrtRTEI = { 20,/* lineNo */
  12,                                  /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo rd_emlrtRTEI = { 21,/* lineNo */
  10,                                  /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo sd_emlrtRTEI = { 21,/* lineNo */
  6,                                   /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtRTEInfo td_emlrtRTEI = { 1,/* lineNo */
  21,                                  /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtECInfo x_emlrtECI = { -1,  /* nDims */
  14,                                  /* lineNo */
  1,                                   /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtECInfo y_emlrtECI = { -1,  /* nDims */
  16,                                  /* lineNo */
  1,                                   /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtECInfo ab_emlrtECI = { -1, /* nDims */
  18,                                  /* lineNo */
  1,                                   /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

static emlrtECInfo bb_emlrtECI = { -1, /* nDims */
  20,                                  /* lineNo */
  1,                                   /* colNo */
  "trans_rates_Naf",                   /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_Naf.m"/* pName */
};

/* Function Definitions */
void trans_rates_Naf(const emlrtStack *sp, const emxArray_real_T *V, const
                     real_T Kins[12], emxArray_real_T *ratesNaf)
{
  int32_T i12;
  int32_T loop_ub;
  emxArray_int32_T *r6;
  emxArray_real_T *r7;
  emxArray_real_T *b_Kins;
  emxArray_real_T *r8;
  int32_T iv5[2];
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emlrtMEXProfilingFunctionEntry(trans_rates_Naf_complete_name, isMexOutdated);

  /*  trans_rates_Naf: This function calculates the potassium channel transition */
  /*  rates for a given voltage. */
  /*  */
  /*  Usage: ratesNaf = trans_rates_Naf(V,Kins); */
  /*  */
  /*    ratesNaf:    Voltage dependent transition rates  */
  /*    V:          Segment membrane potential (mV) */
  /*    Kins:       Array containing kinematic parameters. */
  /*  */
  /*  Jesse M. Resnick (resnick@uw.edu) � 2018 */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  i12 = ratesNaf->size[0] * ratesNaf->size[1];
  ratesNaf->size[0] = 4;
  ratesNaf->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, ratesNaf, i12, &gd_emlrtRTEI);
  loop_ub = V->size[1] << 2;
  for (i12 = 0; i12 < loop_ub; i12++) {
    ratesNaf->data[i12] = 0.0;
  }

  emxInit_int32_T(sp, &r6, 1, &td_emlrtRTEI, true);

  /*        % a_m */
  emlrtMEXProfilingStatement(2, isMexOutdated);
  emlrtMEXProfilingStatement(3, isMexOutdated);
  loop_ub = V->size[1];
  i12 = r6->size[0];
  r6->size[0] = loop_ub;
  emxEnsureCapacity_int32_T(sp, r6, i12, &hd_emlrtRTEI);
  for (i12 = 0; i12 < loop_ub; i12++) {
    r6->data[i12] = i12;
  }

  emxInit_real_T(sp, &r7, 2, &id_emlrtRTEI, true);
  i12 = r7->size[0] * r7->size[1];
  r7->size[0] = 1;
  r7->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, r7, i12, &id_emlrtRTEI);
  loop_ub = V->size[0] * V->size[1];
  for (i12 = 0; i12 < loop_ub; i12++) {
    r7->data[i12] = (Kins[1] - V->data[i12]) / Kins[2];
  }

  st.site = &ac_emlrtRSI;
  b_exp(&st, r7);
  i12 = r7->size[0] * r7->size[1];
  loop_ub = r7->size[0] * r7->size[1];
  r7->size[0] = 1;
  emxEnsureCapacity_real_T(sp, r7, loop_ub, &jd_emlrtRTEI);
  loop_ub = i12 - 1;
  for (i12 = 0; i12 <= loop_ub; i12++) {
    r7->data[i12] = 1.0 - r7->data[i12];
  }

  emxInit_real_T(sp, &b_Kins, 2, &ib_emlrtRTEI, true);
  i12 = b_Kins->size[0] * b_Kins->size[1];
  b_Kins->size[0] = 1;
  b_Kins->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, b_Kins, i12, &ib_emlrtRTEI);
  loop_ub = V->size[0] * V->size[1];
  for (i12 = 0; i12 < loop_ub; i12++) {
    b_Kins->data[i12] = Kins[0] * (V->data[i12] - Kins[1]);
  }

  emxInit_real_T(sp, &r8, 2, &sd_emlrtRTEI, true);
  st.site = &bc_emlrtRSI;
  b_rdivide_helper(&st, b_Kins, r7, r8);
  iv5[0] = 1;
  iv5[1] = r6->size[0];
  emlrtSubAssignSizeCheckR2012b(&iv5[0], 2, &(*(int32_T (*)[2])r8->size)[0], 2,
    &x_emlrtECI, sp);
  loop_ub = r8->size[1];
  for (i12 = 0; i12 < loop_ub; i12++) {
    ratesNaf->data[r6->data[i12] << 2] = r8->data[i12];
  }

  /*        % b_m */
  emlrtMEXProfilingStatement(4, isMexOutdated);
  emlrtMEXProfilingStatement(5, isMexOutdated);
  loop_ub = ratesNaf->size[1];
  i12 = r6->size[0];
  r6->size[0] = loop_ub;
  emxEnsureCapacity_int32_T(sp, r6, i12, &kd_emlrtRTEI);
  for (i12 = 0; i12 < loop_ub; i12++) {
    r6->data[i12] = i12;
  }

  i12 = r7->size[0] * r7->size[1];
  r7->size[0] = 1;
  r7->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, r7, i12, &ld_emlrtRTEI);
  loop_ub = V->size[0] * V->size[1];
  for (i12 = 0; i12 < loop_ub; i12++) {
    r7->data[i12] = (V->data[i12] - Kins[4]) / Kins[5];
  }

  st.site = &cc_emlrtRSI;
  b_exp(&st, r7);
  i12 = r7->size[0] * r7->size[1];
  loop_ub = r7->size[0] * r7->size[1];
  r7->size[0] = 1;
  emxEnsureCapacity_real_T(sp, r7, loop_ub, &md_emlrtRTEI);
  loop_ub = i12 - 1;
  for (i12 = 0; i12 <= loop_ub; i12++) {
    r7->data[i12] = 1.0 - r7->data[i12];
  }

  i12 = b_Kins->size[0] * b_Kins->size[1];
  b_Kins->size[0] = 1;
  b_Kins->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, b_Kins, i12, &ib_emlrtRTEI);
  loop_ub = V->size[0] * V->size[1];
  for (i12 = 0; i12 < loop_ub; i12++) {
    b_Kins->data[i12] = Kins[3] * (Kins[4] - V->data[i12]);
  }

  st.site = &dc_emlrtRSI;
  b_rdivide_helper(&st, b_Kins, r7, r8);
  iv5[0] = 1;
  iv5[1] = r6->size[0];
  emlrtSubAssignSizeCheckR2012b(&iv5[0], 2, &(*(int32_T (*)[2])r8->size)[0], 2,
    &y_emlrtECI, sp);
  loop_ub = r8->size[1];
  for (i12 = 0; i12 < loop_ub; i12++) {
    ratesNaf->data[1 + (r6->data[i12] << 2)] = r8->data[i12];
  }

  /*        % a_h */
  emlrtMEXProfilingStatement(6, isMexOutdated);
  emlrtMEXProfilingStatement(7, isMexOutdated);
  loop_ub = ratesNaf->size[1];
  i12 = r6->size[0];
  r6->size[0] = loop_ub;
  emxEnsureCapacity_int32_T(sp, r6, i12, &nd_emlrtRTEI);
  for (i12 = 0; i12 < loop_ub; i12++) {
    r6->data[i12] = i12;
  }

  i12 = r7->size[0] * r7->size[1];
  r7->size[0] = 1;
  r7->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, r7, i12, &od_emlrtRTEI);
  loop_ub = V->size[0] * V->size[1];
  for (i12 = 0; i12 < loop_ub; i12++) {
    r7->data[i12] = (V->data[i12] - Kins[7]) / Kins[8];
  }

  st.site = &ec_emlrtRSI;
  b_exp(&st, r7);
  i12 = r7->size[0] * r7->size[1];
  loop_ub = r7->size[0] * r7->size[1];
  r7->size[0] = 1;
  emxEnsureCapacity_real_T(sp, r7, loop_ub, &pd_emlrtRTEI);
  loop_ub = i12 - 1;
  for (i12 = 0; i12 <= loop_ub; i12++) {
    r7->data[i12] = 1.0 - r7->data[i12];
  }

  i12 = b_Kins->size[0] * b_Kins->size[1];
  b_Kins->size[0] = 1;
  b_Kins->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, b_Kins, i12, &ib_emlrtRTEI);
  loop_ub = V->size[0] * V->size[1];
  for (i12 = 0; i12 < loop_ub; i12++) {
    b_Kins->data[i12] = Kins[6] * (Kins[7] - V->data[i12]);
  }

  st.site = &fc_emlrtRSI;
  b_rdivide_helper(&st, b_Kins, r7, r8);
  iv5[0] = 1;
  iv5[1] = r6->size[0];
  emlrtSubAssignSizeCheckR2012b(&iv5[0], 2, &(*(int32_T (*)[2])r8->size)[0], 2,
    &ab_emlrtECI, sp);
  loop_ub = r8->size[1];
  emxFree_real_T(&b_Kins);
  for (i12 = 0; i12 < loop_ub; i12++) {
    ratesNaf->data[2 + (r6->data[i12] << 2)] = r8->data[i12];
  }

  /*                       % b_h */
  emlrtMEXProfilingStatement(8, isMexOutdated);
  emlrtMEXProfilingStatement(9, isMexOutdated);
  loop_ub = ratesNaf->size[1];
  i12 = r6->size[0];
  r6->size[0] = loop_ub;
  emxEnsureCapacity_int32_T(sp, r6, i12, &qd_emlrtRTEI);
  for (i12 = 0; i12 < loop_ub; i12++) {
    r6->data[i12] = i12;
  }

  i12 = r7->size[0] * r7->size[1];
  r7->size[0] = 1;
  r7->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, r7, i12, &rd_emlrtRTEI);
  loop_ub = V->size[0] * V->size[1];
  for (i12 = 0; i12 < loop_ub; i12++) {
    r7->data[i12] = (Kins[10] - V->data[i12]) / Kins[11];
  }

  st.site = &gc_emlrtRSI;
  b_exp(&st, r7);
  i12 = r8->size[0] * r8->size[1];
  r8->size[0] = 1;
  r8->size[1] = r7->size[1];
  emxEnsureCapacity_real_T(sp, r8, i12, &sd_emlrtRTEI);
  loop_ub = r7->size[0] * r7->size[1];
  for (i12 = 0; i12 < loop_ub; i12++) {
    r8->data[i12] = 1.0 + r7->data[i12];
  }

  rdivide_helper(sp, Kins[9], r8, r7);
  iv5[0] = 1;
  iv5[1] = r6->size[0];
  emlrtSubAssignSizeCheckR2012b(&iv5[0], 2, &(*(int32_T (*)[2])r7->size)[0], 2,
    &bb_emlrtECI, sp);
  loop_ub = r7->size[1];
  emxFree_real_T(&r8);
  for (i12 = 0; i12 < loop_ub; i12++) {
    ratesNaf->data[3 + (r6->data[i12] << 2)] = r7->data[i12];
  }

  emxFree_real_T(&r7);
  emxFree_int32_T(&r6);
  emlrtMEXProfilingStatement(10, isMexOutdated);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (trans_rates_Naf.c) */
