/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * initialize_Ks_channels.c
 *
 * Code generation for function 'initialize_Ks_channels'
 *
 */

/* Include files */
#include <string.h>
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "initialize_Ks_channels.h"
#include "error.h"
#include "stochAN_multi_data.h"

/* Variable Definitions */
static emlrtRSInfo rc_emlrtRSI = { 32, /* lineNo */
  "initialize_Ks_channels",            /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Ks_channels.m"/* pathName */
};

static emlrtRSInfo sc_emlrtRSI = { 43, /* lineNo */
  "initialize_Ks_channels",            /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Ks_channels.m"/* pathName */
};

/* Function Definitions */
void initialize_Ks_channels(const emlrtStack *sp, const real_T ratesKs[2],
  real_T numK, real_T dt, real_T numKs[2])
{
  real_T Tl;
  boolean_T exitg1;
  real_T lambda;
  real_T x;
  real_T P[2];
  int32_T idx;
  int32_T ii_size_idx_1;
  int32_T ii;
  boolean_T exitg2;
  int8_T ii_data[1];
  int8_T ind_data[1];
  real_T numKs_data[1];
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtMEXProfilingFunctionEntry(c_initialize_Ks_channels_comple, isMexOutdated);

  /*  NUMKS: This function initializes the number of channels in each state */
  /*  using a Jump Monte Carlo method. */
  /*  */
  /*  Usage: numKs = initialize_Ks_channels(ratesKs,numK,dt) */
  /*  */
  /*    numKs:      Array of # of channels in each state */
  /*    ratesKs:    Voltage dependent transition rates */
  /*    numK:       Number of K channels at segment */
  /*    dt:         Timestep (us) */
  /*  */
  /*  Jesse M. Resnick (resnick@uw.edu) � 2018 */
  /*  initial destribution of particles in each channel */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  numKs[0] = muDoubleScalarRound(158.0 * numK / 168.0);
  numKs[1] = muDoubleScalarRound(8.0 * numK / 168.0);

  /*  Net transition rates for each channel type. */
  emlrtMEXProfilingStatement(3, isMexOutdated);
  emlrtMEXProfilingStatement(4, isMexOutdated);
  Tl = 0.0;

  /*  Indices of channel types for different reaction types. */
  emlrtMEXProfilingStatement(5, isMexOutdated);
  emlrtMEXProfilingStatement(6, isMexOutdated);
  emlrtMEXProfilingStatement(7, isMexOutdated);
  exitg1 = false;
  while ((!exitg1) && (Tl <= 100.0 * dt)) {
    emlrtMEXProfilingStatement(8, isMexOutdated);
    lambda = numKs[0] * ratesKs[0] + numKs[1] * ratesKs[1];
    emlrtMEXProfilingStatement(9, isMexOutdated);
    st.site = &rc_emlrtRSI;
    emlrtRandu(&x, 1);
    st.site = &rc_emlrtRSI;
    if (x < 0.0) {
      b_st.site = &oc_emlrtRSI;
      d_error(&b_st);
    }

    x = muDoubleScalarLog(x);
    emlrtMEXProfilingStatement(10, isMexOutdated);
    Tl += -x / lambda;
    emlrtMEXProfilingStatement(11, isMexOutdated);
    if (Tl > 100.0 * dt) {
      emlrtMEXProfilingStatement(12, isMexOutdated);
      exitg1 = true;
    } else {
      emlrtMEXProfilingStatement(14, isMexOutdated);
      emlrtMEXProfilingStatement(15, isMexOutdated);
      emlrtMEXProfilingStatement(16, isMexOutdated);
      P[0] = ratesKs[0] * numKs[0] / lambda;
      P[1] = ratesKs[1] * numKs[1] / lambda;
      P[1] += P[0];
      emlrtMEXProfilingStatement(17, isMexOutdated);
      st.site = &sc_emlrtRSI;
      emlrtRandu(&lambda, 1);
      idx = 0;
      ii_size_idx_1 = 1;
      ii = 0;
      exitg2 = false;
      while ((!exitg2) && (ii < 2)) {
        if (lambda < P[ii]) {
          idx = 1;
          ii_data[0] = (int8_T)(ii + 1);
          exitg2 = true;
        } else {
          ii++;
        }
      }

      if (idx == 0) {
        ii_size_idx_1 = 0;
      }

      if (0 <= ii_size_idx_1 - 1) {
        memcpy(&ind_data[0], &ii_data[0], (uint32_T)(ii_size_idx_1 * (int32_T)
                sizeof(int8_T)));
      }

      emlrtMEXProfilingStatement(18, isMexOutdated);
      for (idx = 0; idx < ii_size_idx_1; idx++) {
        ii_data[0] = (int8_T)(1 + (int8_T)(ind_data[0] - 1));
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKs_data[0] = numKs[ii_data[0] - 1] - 1.0;
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKs[ii_data[0] - 1] = numKs_data[0];
      }

      emlrtMEXProfilingStatement(19, isMexOutdated);
      for (idx = 0; idx < ii_size_idx_1; idx++) {
        ii_data[0] = (int8_T)(2 - (int8_T)(ind_data[0] - 1));
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKs_data[0] = numKs[ii_data[0] - 1] + 1.0;
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKs[ii_data[0] - 1] = numKs_data[0];
      }

      emlrtMEXProfilingStatement(20, isMexOutdated);
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }
  }

  emlrtMEXProfilingStatement(21, isMexOutdated);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
}

/* End of code generation (initialize_Ks_channels.c) */
