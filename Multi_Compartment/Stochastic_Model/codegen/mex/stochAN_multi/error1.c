/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * error1.c
 *
 * Code generation for function 'error1'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "error1.h"

/* Variable Definitions */
static emlrtMCInfo b_emlrtMCI = { 27,  /* lineNo */
  5,                                   /* colNo */
  "error",                             /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/lang/error.m"/* pName */
};

static emlrtRSInfo gd_emlrtRSI = { 27, /* lineNo */
  "error",                             /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/lang/error.m"/* pathName */
};

/* Function Declarations */
static const mxArray *emlrt_marshallOut(const real_T u_data[], const int32_T
  u_size[2]);
static void g_error(const emlrtStack *sp, const mxArray *b, const mxArray *c,
                    emlrtMCInfo *location);
static void h_error(const emlrtStack *sp, const mxArray *b, const mxArray *c,
                    const mxArray *d, const mxArray *e, emlrtMCInfo *location);

/* Function Definitions */
static const mxArray *emlrt_marshallOut(const real_T u_data[], const int32_T
  u_size[2])
{
  const mxArray *y;
  const mxArray *m4;
  real_T *pData;
  int32_T i19;
  int32_T i;
  y = NULL;
  m4 = emlrtCreateNumericArray(2, u_size, mxDOUBLE_CLASS, mxREAL);
  pData = emlrtMxGetPr(m4);
  i19 = 0;
  i = 0;
  while (i < u_size[1]) {
    pData[i19] = u_data[0];
    i19++;
    i = 1;
  }

  emlrtAssign(&y, m4);
  return y;
}

static void g_error(const emlrtStack *sp, const mxArray *b, const mxArray *c,
                    emlrtMCInfo *location)
{
  const mxArray *pArrays[2];
  pArrays[0] = b;
  pArrays[1] = c;
  emlrtCallMATLABR2012b(sp, 0, NULL, 2, pArrays, "error", true, location);
}

static void h_error(const emlrtStack *sp, const mxArray *b, const mxArray *c,
                    const mxArray *d, const mxArray *e, emlrtMCInfo *location)
{
  const mxArray *pArrays[4];
  pArrays[0] = b;
  pArrays[1] = c;
  pArrays[2] = d;
  pArrays[3] = e;
  emlrtCallMATLABR2012b(sp, 0, NULL, 4, pArrays, "error", true, location);
}

void e_error(const emlrtStack *sp, const real_T varargin_2_data[], const int32_T
             varargin_2_size[2])
{
  const mxArray *y;
  const mxArray *m1;
  static const int32_T iv11[2] = { 1, 31 };

  static const char_T varargin_1[31] = { 'S', 'e', 'g', 'm', 'e', 'n', 't', 's',
    ' ', '%', 'd', ' ', 'e', 'x', 'c', 'e', 'e', 'd', 'e', 'd', ' ', 'm', 'a',
    'x', 'S', 'p', 'i', 'k', 'e', 's', '.' };

  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  y = NULL;
  m1 = emlrtCreateCharArray(2, iv11);
  emlrtInitCharArrayR2013a(sp, 31, m1, &varargin_1[0]);
  emlrtAssign(&y, m1);
  st.site = &gd_emlrtRSI;
  g_error(&st, y, emlrt_marshallOut(varargin_2_data, varargin_2_size),
          &b_emlrtMCI);
}

void f_error(const emlrtStack *sp, real_T varargin_2, const real_T
             varargin_3_data[], const int32_T varargin_3_size[2], const real_T
             varargin_4_data[], const int32_T varargin_4_size[2])
{
  const mxArray *y;
  const mxArray *m2;
  static const int32_T iv12[2] = { 1, 43 };

  static const char_T varargin_1[43] = { 'T', 'i', 'm', 'e', ' ', 'i', 's', ' ',
    '%', '0', '.', '0', 'f', ',', ' ', 'V', 'm', '[', '%', 'd', ']', ' ', 'i',
    's', ' ', 'o', 'u', 't', ' ', 'o', 'f', ' ', 'r', 'a', 'n', 'g', 'e', ':',
    ' ', '%', 'f', '\\', 'n' };

  const mxArray *b_y;
  const mxArray *m3;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  y = NULL;
  m2 = emlrtCreateCharArray(2, iv12);
  emlrtInitCharArrayR2013a(sp, 43, m2, &varargin_1[0]);
  emlrtAssign(&y, m2);
  b_y = NULL;
  m3 = emlrtCreateDoubleScalar(varargin_2);
  emlrtAssign(&b_y, m3);
  st.site = &gd_emlrtRSI;
  h_error(&st, y, b_y, emlrt_marshallOut(varargin_3_data, varargin_3_size),
          emlrt_marshallOut(varargin_4_data, varargin_4_size), &b_emlrtMCI);
}

/* End of code generation (error1.c) */
