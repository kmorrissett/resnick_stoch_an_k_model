/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * any.h
 *
 * Code generation for function 'any'
 *
 */

#ifndef ANY_H
#define ANY_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
extern boolean_T any(const emlrtStack *sp, const emxArray_real_T *x);
extern boolean_T b_any(const emlrtStack *sp, const emxArray_boolean_T *x);

#endif

/* End of code generation (any.h) */
