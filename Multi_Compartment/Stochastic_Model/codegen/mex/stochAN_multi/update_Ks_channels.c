/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * update_Ks_channels.c
 *
 * Code generation for function 'update_Ks_channels'
 *
 */

/* Include files */
#include <string.h>
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "update_Ks_channels.h"
#include "error.h"
#include "stochAN_multi_data.h"

/* Variable Definitions */
static emlrtRSInfo xc_emlrtRSI = { 24, /* lineNo */
  "update_Ks_channels",                /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Ks_channels.m"/* pathName */
};

static emlrtRSInfo yc_emlrtRSI = { 36, /* lineNo */
  "update_Ks_channels",                /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Ks_channels.m"/* pathName */
};

static emlrtBCInfo ef_emlrtBCI = { 1,  /* iFirst */
  2,                                   /* iLast */
  38,                                  /* lineNo */
  11,                                  /* colNo */
  "numKs",                             /* aName */
  "update_Ks_channels",                /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Ks_channels.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo ff_emlrtBCI = { 1,  /* iFirst */
  2,                                   /* iLast */
  39,                                  /* lineNo */
  11,                                  /* colNo */
  "numKs",                             /* aName */
  "update_Ks_channels",                /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/update_Ks_channels.m",/* pName */
  3                                    /* checkKind */
};

/* Function Definitions */
void update_Ks_channels(const emlrtStack *sp, const real_T ratesKs[2], real_T
  numKs[2], real_T dt)
{
  real_T Tl;
  boolean_T exitg1;
  real_T lambda;
  real_T x;
  real_T P[3];
  int32_T idx;
  int32_T ii_size_idx_1;
  int32_T ii;
  boolean_T exitg2;
  int8_T ii_data[1];
  int8_T ind_data[1];
  real_T numKs_data[1];
  static const int8_T iv20[3] = { 0, 2, 1 };

  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtMEXProfilingFunctionEntry(c_update_Ks_channels_complete_n, isMexOutdated);

  /*  NUMKS: This function updates the number of channels in each state for the */
  /*  next timestep using a Jump Monte Carlo method. */
  /*  */
  /*  Usage: numKs = initialize_Ks_channels(ratesKs,numKs,dt) */
  /*  */
  /*    numKs:      Array of # of channels in each state */
  /*    ratesKs:    Voltage dependent transition rates  */
  /*    dt:         Timestep (us) */
  /*  */
  /*  Jesse M. Resnick (resnick@uw.edu) � 2018 */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  emlrtMEXProfilingStatement(2, isMexOutdated);
  Tl = 0.0;
  emlrtMEXProfilingStatement(3, isMexOutdated);
  emlrtMEXProfilingStatement(4, isMexOutdated);
  emlrtMEXProfilingStatement(5, isMexOutdated);
  exitg1 = false;
  while ((!exitg1) && (Tl <= dt)) {
    emlrtMEXProfilingStatement(6, isMexOutdated);
    lambda = numKs[0] * ratesKs[0] + numKs[1] * ratesKs[1];
    emlrtMEXProfilingStatement(7, isMexOutdated);
    st.site = &xc_emlrtRSI;
    emlrtRandu(&x, 1);
    st.site = &xc_emlrtRSI;
    if (x < 0.0) {
      b_st.site = &oc_emlrtRSI;
      d_error(&b_st);
    }

    x = muDoubleScalarLog(x);
    emlrtMEXProfilingStatement(8, isMexOutdated);
    Tl += -x / lambda;
    emlrtMEXProfilingStatement(9, isMexOutdated);
    if (Tl > dt) {
      emlrtMEXProfilingStatement(10, isMexOutdated);
      exitg1 = true;
    } else {
      emlrtMEXProfilingStatement(12, isMexOutdated);
      emlrtMEXProfilingStatement(13, isMexOutdated);
      emlrtMEXProfilingStatement(14, isMexOutdated);
      emlrtMEXProfilingStatement(15, isMexOutdated);
      P[0] = 0.0 / lambda;
      P[1] = ratesKs[0] * numKs[0] / lambda;
      P[2] = ratesKs[1] * numKs[1] / lambda;
      P[1] += P[0];
      P[2] += P[1];
      emlrtMEXProfilingStatement(16, isMexOutdated);
      st.site = &yc_emlrtRSI;
      emlrtRandu(&lambda, 1);
      idx = 0;
      ii_size_idx_1 = 1;
      ii = 0;
      exitg2 = false;
      while ((!exitg2) && (ii < 3)) {
        if (lambda < P[ii]) {
          idx = 1;
          ii_data[0] = (int8_T)(ii + 1);
          exitg2 = true;
        } else {
          ii++;
        }
      }

      if (idx == 0) {
        ii_size_idx_1 = 0;
      }

      if (0 <= ii_size_idx_1 - 1) {
        memcpy(&ind_data[0], &ii_data[0], (uint32_T)(ii_size_idx_1 * (int32_T)
                sizeof(int8_T)));
      }

      emlrtMEXProfilingStatement(17, isMexOutdated);
      for (idx = 0; idx < ii_size_idx_1; idx++) {
        ii = (int8_T)(ind_data[0] - 1);
        if ((ii < 1) || (ii > 2)) {
          emlrtDynamicBoundsCheckR2012b(ii, 1, 2, &ef_emlrtBCI, sp);
        }

        ii_data[0] = (int8_T)ii;
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKs_data[0] = numKs[ii_data[0] - 1] - 1.0;
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKs[ii_data[0] - 1] = numKs_data[0];
      }

      emlrtMEXProfilingStatement(18, isMexOutdated);
      for (idx = 0; idx < ii_size_idx_1; idx++) {
        ii = ind_data[0] - 1;
        if (iv20[ii] < 1) {
          emlrtDynamicBoundsCheckR2012b(0, 1, 2, &ff_emlrtBCI, sp);
        }

        ii_data[0] = iv20[ii];
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKs_data[0] = numKs[ii_data[0] - 1] + 1.0;
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numKs[ii_data[0] - 1] = numKs_data[0];
      }

      emlrtMEXProfilingStatement(19, isMexOutdated);
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }
  }

  emlrtMEXProfilingFunctionExit(isMexOutdated);
}

/* End of code generation (update_Ks_channels.c) */
