/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_stochAN_multi_mex.c
 *
 * Code generation for function '_coder_stochAN_multi_mex'
 *
 */

/* Include files */
#include "stochAN_multi.h"
#include "_coder_stochAN_multi_mex.h"
#include "stochAN_multi_terminate.h"
#include "_coder_stochAN_multi_api.h"
#include "stochAN_multi_initialize.h"
#include "stochAN_multi_data.h"

/* Function Declarations */
static void stochAN_multi_mexFunction(int32_T nlhs, mxArray *plhs[3], int32_T
  nrhs, const mxArray *prhs[4]);

/* Function Definitions */
static void stochAN_multi_mexFunction(int32_T nlhs, mxArray *plhs[3], int32_T
  nrhs, const mxArray *prhs[4])
{
  const mxArray *outputs[3];
  int32_T b_nlhs;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 4) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 4, 4,
                        13, "stochAN_multi");
  }

  if (nlhs > 3) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 13,
                        "stochAN_multi");
  }

  /* Call the function. */
  stochAN_multi_api(prhs, nlhs, outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);
}

void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const mxArray
                 *prhs[])
{
  mexAtExit(stochAN_multi_atexit);

  /* Module initialization. */
  stochAN_multi_initialize();

  /* Dispatch the entry-point. */
  stochAN_multi_mexFunction(nlhs, plhs, nrhs, prhs);

  /* Module termination. */
  stochAN_multi_terminate();
}

emlrtCTX mexFunctionCreateRootTLS(void)
{
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  return emlrtRootTLSGlobal;
}

/* End of code generation (_coder_stochAN_multi_mex.c) */
