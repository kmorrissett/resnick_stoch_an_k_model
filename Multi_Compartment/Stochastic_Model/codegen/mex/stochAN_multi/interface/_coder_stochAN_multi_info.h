/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_stochAN_multi_info.h
 *
 * Code generation for function '_coder_stochAN_multi_info'
 *
 */

#ifndef _CODER_STOCHAN_MULTI_INFO_H
#define _CODER_STOCHAN_MULTI_INFO_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties(void);
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);

#endif

/* End of code generation (_coder_stochAN_multi_info.h) */
