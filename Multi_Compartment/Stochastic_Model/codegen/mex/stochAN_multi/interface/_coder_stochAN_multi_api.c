/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_stochAN_multi_api.c
 *
 * Code generation for function '_coder_stochAN_multi_api'
 *
 */

/* Include files */
#include <string.h>
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "_coder_stochAN_multi_api.h"
#include "stochAN_multi_emxutil.h"
#include "stochAN_multi_data.h"

/* Variable Definitions */
static emlrtRTEInfo fe_emlrtRTEI = { 1,/* lineNo */
  1,                                   /* colNo */
  "_coder_stochAN_multi_api",          /* fName */
  ""                                   /* pName */
};

static const int32_T iv0[3] = { 0, 0, 0 };

/* Function Declarations */
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static const mxArray *b_emlrt_marshallOut(const emxArray_real_T *u);
static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *segments,
  const char_T *identifier, struct0_T *y);
static const mxArray *c_emlrt_marshallOut(const emxArray_real32_T *u);
static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct0_T *y);
static const mxArray *d_emlrt_marshallOut(const emxArray_real32_T *u);
static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *Istim, const
  char_T *identifier, emxArray_real_T *y);
static real_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *model, const
  char_T *identifier, struct1_T *y);
static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct1_T *y);
static struct2_T i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId);
static void j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct3_T *y);
static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[6]);
static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct4_T *y);
static void m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[12]);
static struct5_T n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId);
static void o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *options,
  const char_T *identifier, struct6_T *y);
static void p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct6_T *y);
static void q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static void r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static real_T s_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);
static void t_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[6]);
static void u_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[12]);

/* Function Definitions */
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  q_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static const mxArray *b_emlrt_marshallOut(const emxArray_real_T *u)
{
  const mxArray *y;
  const mxArray *m5;
  y = NULL;
  m5 = emlrtCreateNumericArray(3, iv0, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m5, &u->data[0]);
  emlrtSetDimensions((mxArray *)m5, u->size, 3);
  emlrtAssign(&y, m5);
  return y;
}

static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *segments,
  const char_T *identifier, struct0_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  d_emlrt_marshallIn(sp, emlrtAlias(segments), &thisId, y);
  emlrtDestroyArray(&segments);
}

static const mxArray *c_emlrt_marshallOut(const emxArray_real32_T *u)
{
  const mxArray *y;
  const mxArray *m6;
  y = NULL;
  m6 = emlrtCreateNumericArray(3, iv0, mxSINGLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m6, &u->data[0]);
  emlrtSetDimensions((mxArray *)m6, u->size, 3);
  emlrtAssign(&y, m6);
  return y;
}

static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct0_T *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[10] = { "cm", "nodeIDX", "numKf", "numKs",
    "numNaf", "ra", "rm", "xApp", "xEcap", "zDistance" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 10, fieldNames, 0U, &dims);
  thisId.fIdentifier = "cm";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0, "cm")),
                     &thisId, y->cm);
  thisId.fIdentifier = "nodeIDX";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1, "nodeIDX")),
                     &thisId, y->nodeIDX);
  thisId.fIdentifier = "numKf";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 2, "numKf")),
                     &thisId, y->numKf);
  thisId.fIdentifier = "numKs";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 3, "numKs")),
                     &thisId, y->numKs);
  thisId.fIdentifier = "numNaf";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 4, "numNaf")),
                     &thisId, y->numNaf);
  thisId.fIdentifier = "ra";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 5, "ra")),
                     &thisId, y->ra);
  thisId.fIdentifier = "rm";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 6, "rm")),
                     &thisId, y->rm);
  thisId.fIdentifier = "xApp";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 7, "xApp")),
                     &thisId, y->xApp);
  thisId.fIdentifier = "xEcap";
  e_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 8, "xEcap")),
                     &thisId, y->xEcap);
  thisId.fIdentifier = "zDistance";
  y->zDistance = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    9, "zDistance")), &thisId);
  emlrtDestroyArray(&u);
}

static const mxArray *d_emlrt_marshallOut(const emxArray_real32_T *u)
{
  const mxArray *y;
  const mxArray *m7;
  static const int32_T iv13[2] = { 0, 0 };

  y = NULL;
  m7 = emlrtCreateNumericArray(2, iv13, mxSINGLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m7, &u->data[0]);
  emlrtSetDimensions((mxArray *)m7, u->size, 2);
  emlrtAssign(&y, m7);
  return y;
}

static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  r_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *Istim, const
  char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  b_emlrt_marshallIn(sp, emlrtAlias(Istim), &thisId, y);
  emlrtDestroyArray(&Istim);
}

static real_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = s_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *model, const
  char_T *identifier, struct1_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  h_emlrt_marshallIn(sp, emlrtAlias(model), &thisId, y);
  emlrtDestroyArray(&model);
}

static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct1_T *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[17] = { "IntNode", "Kf", "Ks", "LtoD", "Naf",
    "Node", "Terminal", "Vmax", "Vrest", "Vthresh", "constrict", "dt", "elecR",
    "intSegs", "nLen", "normdtoD", "resMed" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 17, fieldNames, 0U, &dims);
  thisId.fIdentifier = "IntNode";
  y->IntNode = i_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0,
    "IntNode")), &thisId);
  thisId.fIdentifier = "Kf";
  j_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1, "Kf")),
                     &thisId, &y->Kf);
  thisId.fIdentifier = "Ks";
  j_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 2, "Ks")),
                     &thisId, &y->Ks);
  thisId.fIdentifier = "LtoD";
  y->LtoD = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 3,
    "LtoD")), &thisId);
  thisId.fIdentifier = "Naf";
  l_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 4, "Naf")),
                     &thisId, &y->Naf);
  thisId.fIdentifier = "Node";
  y->Node = i_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 5,
    "Node")), &thisId);
  thisId.fIdentifier = "Terminal";
  y->Terminal = n_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    6, "Terminal")), &thisId);
  thisId.fIdentifier = "Vmax";
  y->Vmax = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 7,
    "Vmax")), &thisId);
  thisId.fIdentifier = "Vrest";
  y->Vrest = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 8,
    "Vrest")), &thisId);
  thisId.fIdentifier = "Vthresh";
  y->Vthresh = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 9,
    "Vthresh")), &thisId);
  thisId.fIdentifier = "constrict";
  y->constrict = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    10, "constrict")), &thisId);
  thisId.fIdentifier = "dt";
  y->dt = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 11,
    "dt")), &thisId);
  thisId.fIdentifier = "elecR";
  y->elecR = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 12,
    "elecR")), &thisId);
  thisId.fIdentifier = "intSegs";
  y->intSegs = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    13, "intSegs")), &thisId);
  thisId.fIdentifier = "nLen";
  y->nLen = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 14,
    "nLen")), &thisId);
  thisId.fIdentifier = "normdtoD";
  y->normdtoD = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    15, "normdtoD")), &thisId);
  thisId.fIdentifier = "resMed";
  y->resMed = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 16,
    "resMed")), &thisId);
  emlrtDestroyArray(&u);
}

static struct2_T i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId)
{
  struct2_T y;
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[3] = { "rm", "cm", "ra" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 3, fieldNames, 0U, &dims);
  thisId.fIdentifier = "rm";
  y.rm = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0, "rm")),
    &thisId);
  thisId.fIdentifier = "cm";
  y.cm = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1, "cm")),
    &thisId);
  thisId.fIdentifier = "ra";
  y.ra = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 2, "ra")),
    &thisId);
  emlrtDestroyArray(&u);
  return y;
}

static void j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct3_T *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[4] = { "E_K", "gK", "density", "Kins" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 4, fieldNames, 0U, &dims);
  thisId.fIdentifier = "E_K";
  y->E_K = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0,
    "E_K")), &thisId);
  thisId.fIdentifier = "gK";
  y->gK = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1,
    "gK")), &thisId);
  thisId.fIdentifier = "density";
  y->density = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 2,
    "density")), &thisId);
  thisId.fIdentifier = "Kins";
  k_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 3, "Kins")),
                     &thisId, y->Kins);
  emlrtDestroyArray(&u);
}

static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[6])
{
  t_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct4_T *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[4] = { "E_Na", "gNa", "density", "Kins" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 4, fieldNames, 0U, &dims);
  thisId.fIdentifier = "E_Na";
  y->E_Na = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0,
    "E_Na")), &thisId);
  thisId.fIdentifier = "gNa";
  y->gNa = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1,
    "gNa")), &thisId);
  thisId.fIdentifier = "density";
  y->density = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 2,
    "density")), &thisId);
  thisId.fIdentifier = "Kins";
  m_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 3, "Kins")),
                     &thisId, y->Kins);
  emlrtDestroyArray(&u);
}

static void m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[12])
{
  u_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static struct5_T n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId)
{
  struct5_T y;
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[1] = { "areaCoef" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 1, fieldNames, 0U, &dims);
  thisId.fIdentifier = "areaCoef";
  y.areaCoef = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0,
    "areaCoef")), &thisId);
  emlrtDestroyArray(&u);
  return y;
}

static void o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *options,
  const char_T *identifier, struct6_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  p_emlrt_marshallIn(sp, emlrtAlias(options), &thisId, y);
  emlrtDestroyArray(&options);
}

static void p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct6_T *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[14] = { "Vsample", "XstimLoc", "eCAPxLoc",
    "eCAPzLoc", "maxSpikes", "maxVoltage", "meanNodeAbove", "meanRecordNode",
    "nodeInt", "numMonte", "posRecord", "recECAP", "recV", "seed" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 14, fieldNames, 0U, &dims);
  thisId.fIdentifier = "Vsample";
  y->Vsample = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0,
    "Vsample")), &thisId);
  thisId.fIdentifier = "XstimLoc";
  y->XstimLoc = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    1, "XstimLoc")), &thisId);
  thisId.fIdentifier = "eCAPxLoc";
  y->eCAPxLoc = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    2, "eCAPxLoc")), &thisId);
  thisId.fIdentifier = "eCAPzLoc";
  y->eCAPzLoc = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    3, "eCAPzLoc")), &thisId);
  thisId.fIdentifier = "maxSpikes";
  y->maxSpikes = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    4, "maxSpikes")), &thisId);
  thisId.fIdentifier = "maxVoltage";
  y->maxVoltage = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    5, "maxVoltage")), &thisId);
  thisId.fIdentifier = "meanNodeAbove";
  y->meanNodeAbove = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u,
    0, 6, "meanNodeAbove")), &thisId);
  thisId.fIdentifier = "meanRecordNode";
  y->meanRecordNode = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp,
    u, 0, 7, "meanRecordNode")), &thisId);
  thisId.fIdentifier = "nodeInt";
  y->nodeInt = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 8,
    "nodeInt")), &thisId);
  thisId.fIdentifier = "numMonte";
  y->numMonte = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    9, "numMonte")), &thisId);
  thisId.fIdentifier = "posRecord";
  y->posRecord = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    10, "posRecord")), &thisId);
  thisId.fIdentifier = "recECAP";
  y->recECAP = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0,
    11, "recECAP")), &thisId);
  thisId.fIdentifier = "recV";
  y->recV = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 12,
    "recV")), &thisId);
  thisId.fIdentifier = "seed";
  y->seed = f_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 13,
    "seed")), &thisId);
  emlrtDestroyArray(&u);
}

static void q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[2] = { 1, -1 };

  const boolean_T bv0[2] = { false, true };

  int32_T iv14[2];
  int32_T i20;
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims, &bv0[0],
    iv14);
  ret->allocatedSize = iv14[0] * iv14[1];
  i20 = ret->size[0] * ret->size[1];
  ret->size[0] = iv14[0];
  ret->size[1] = iv14[1];
  emxEnsureCapacity_real_T(sp, ret, i20, (emlrtRTEInfo *)NULL);
  ret->data = (real_T *)emlrtMxGetData(src);
  ret->canFreeData = false;
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

static void r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[2] = { 1, -1 };

  const boolean_T bv1[2] = { false, true };

  int32_T iv15[2];
  int32_T i21;
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims, &bv1[0],
    iv15);
  i21 = ret->size[0] * ret->size[1];
  ret->size[0] = iv15[0];
  ret->size[1] = iv15[1];
  emxEnsureCapacity_real_T(sp, ret, i21, (emlrtRTEInfo *)NULL);
  emlrtImportArrayR2015b(sp, src, ret->data, 8, false);
  emlrtDestroyArray(&src);
}

static real_T s_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static void t_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[6])
{
  static const int32_T dims[1] = { 6 };

  real_T (*r12)[6];
  int32_T i;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  r12 = (real_T (*)[6])emlrtMxGetData(src);
  for (i = 0; i < 6; i++) {
    ret[i] = (*r12)[i];
  }

  emlrtDestroyArray(&src);
}

static void u_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[12])
{
  static const int32_T dims[1] = { 12 };

  real_T (*r13)[12];
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  r13 = (real_T (*)[12])emlrtMxGetData(src);
  memcpy(&ret[0], &(*r13)[0], 12U * sizeof(real_T));
  emlrtDestroyArray(&src);
}

void stochAN_multi_api(const mxArray * const prhs[4], int32_T nlhs, const
  mxArray *plhs[3])
{
  emxArray_real_T *Istim;
  struct0_T segments;
  emxArray_real_T *varargout_1;
  emxArray_real32_T *varargout_2;
  emxArray_real32_T *varargout_3;
  struct1_T model;
  struct6_T options;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtHeapReferenceStackEnterFcnR2012b(&st);
  emxInit_real_T(&st, &Istim, 2, &fe_emlrtRTEI, true);
  emxInitStruct_struct0_T(&st, &segments, &fe_emlrtRTEI, true);
  emxInit_real_T(&st, &varargout_1, 3, &fe_emlrtRTEI, true);
  emxInit_real32_T(&st, &varargout_2, 3, &fe_emlrtRTEI, true);
  emxInit_real32_T(&st, &varargout_3, 2, &fe_emlrtRTEI, true);

  /* Marshall function inputs */
  Istim->canFreeData = false;
  emlrt_marshallIn(&st, emlrtAlias(prhs[0]), "Istim", Istim);
  c_emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "segments", &segments);
  g_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "model", &model);
  o_emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "options", &options);

  /* Invoke the target function */
  stochAN_multi(&st, Istim, &segments, &model, &options, varargout_1,
                varargout_2, varargout_3);

  /* Marshall function outputs */
  varargout_1->canFreeData = false;
  plhs[0] = b_emlrt_marshallOut(varargout_1);
  emxFree_real_T(&varargout_1);
  emxFreeStruct_struct0_T(&segments);
  emxFree_real_T(&Istim);
  if (nlhs > 1) {
    varargout_2->canFreeData = false;
    plhs[1] = c_emlrt_marshallOut(varargout_2);
  }

  emxFree_real32_T(&varargout_2);
  if (nlhs > 2) {
    varargout_3->canFreeData = false;
    plhs[2] = d_emlrt_marshallOut(varargout_3);
  }

  emxFree_real32_T(&varargout_3);
  emlrtHeapReferenceStackLeaveFcnR2012b(&st);
}

/* End of code generation (_coder_stochAN_multi_api.c) */
