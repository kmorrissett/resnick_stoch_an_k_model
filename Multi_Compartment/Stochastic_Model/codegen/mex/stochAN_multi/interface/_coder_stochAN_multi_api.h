/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_stochAN_multi_api.h
 *
 * Code generation for function '_coder_stochAN_multi_api'
 *
 */

#ifndef _CODER_STOCHAN_MULTI_API_H
#define _CODER_STOCHAN_MULTI_API_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
extern void stochAN_multi_api(const mxArray * const prhs[4], int32_T nlhs, const
  mxArray *plhs[3]);

#endif

/* End of code generation (_coder_stochAN_multi_api.h) */
