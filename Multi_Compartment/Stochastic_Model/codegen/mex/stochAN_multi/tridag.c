/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * tridag.c
 *
 * Code generation for function 'tridag'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "tridag.h"
#include "stochAN_multi_emxutil.h"
#include "stochAN_multi_data.h"

/* Variable Definitions */
static emlrtRTEInfo de_emlrtRTEI = { 15,/* lineNo */
  1,                                   /* colNo */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m"/* pName */
};

static emlrtRTEInfo ee_emlrtRTEI = { 16,/* lineNo */
  1,                                   /* colNo */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m"/* pName */
};

static emlrtBCInfo ke_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  18,                                  /* lineNo */
  16,                                  /* colNo */
  "rhs",                               /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo le_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  18,                                  /* lineNo */
  7,                                   /* colNo */
  "Vnext",                             /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo qe_emlrtRTEI = { 28,/* lineNo */
  14,                                  /* colNo */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m"/* pName */
};

static emlrtBCInfo me_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  22,                                  /* lineNo */
  21,                                  /* colNo */
  "c",                                 /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ne_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  22,                                  /* lineNo */
  5,                                   /* colNo */
  "gamma",                             /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  23,                                  /* lineNo */
  12,                                  /* colNo */
  "b",                                 /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  23,                                  /* lineNo */
  22,                                  /* colNo */
  "a",                                 /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  23,                                  /* lineNo */
  32,                                  /* colNo */
  "gamma",                             /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo re_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  24,                                  /* lineNo */
  22,                                  /* colNo */
  "rhs",                               /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo se_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  24,                                  /* lineNo */
  34,                                  /* colNo */
  "a",                                 /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo te_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  24,                                  /* lineNo */
  44,                                  /* colNo */
  "Vnext",                             /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ue_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  24,                                  /* lineNo */
  5,                                   /* colNo */
  "Vnext",                             /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ve_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  29,                                  /* lineNo */
  21,                                  /* colNo */
  "Vnext",                             /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo we_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  29,                                  /* lineNo */
  35,                                  /* colNo */
  "gamma",                             /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  29,                                  /* lineNo */
  51,                                  /* colNo */
  "Vnext",                             /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ye_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  29,                                  /* lineNo */
  5,                                   /* colNo */
  "Vnext",                             /* aName */
  "tridag",                            /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/tridag.m",/* pName */
  0                                    /* checkKind */
};

/* Function Definitions */
void tridag(const emlrtStack *sp, const emxArray_real_T *a, const
            emxArray_real_T *b, const emxArray_real_T *c, const emxArray_real_T *
            rhs, emxArray_real_T *Vnext)
{
  int32_T i14;
  int32_T loop_ub;
  emxArray_real_T *b_gamma;
  real_T beta;
  int32_T segIDX;
  int32_T i15;
  int32_T i16;
  int32_T i17;
  int32_T i18;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emlrtMEXProfilingFunctionEntry(tridag_complete_name, isMexOutdated);

  /*  TRIDAG solves the system of equations A*Vnext = rhs. Where Vnext and rhs */
  /*  are column vectors and A is a tridiagonal matrix of the form: */
  /*        [b(1)   c(1)    0       ...     0       0       0 */
  /*        a(2)    b(2)    c(2)    0       ...     0       0 */
  /*        0       a(3)    b(3)    c(3)    0       ...     0 */
  /*        0       0       ...     ...     ...     ...     0 */
  /*        0       0       ...     ...     ...     ...     0 */
  /*        0       0       ...     ...     a(n-1)  b(n-1)  c(n-1) */
  /*        0       0       0       ...     ...     a(n)    b(n)]  */
  /*  */
  /*  Jesse M. Resnick (resnick@uw.edu) � 2018 */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  emlrtMEXProfilingStatement(2, isMexOutdated);
  i14 = Vnext->size[0] * Vnext->size[1];
  Vnext->size[0] = 1;
  Vnext->size[1] = rhs->size[1];
  emxEnsureCapacity_real_T(sp, Vnext, i14, &de_emlrtRTEI);
  loop_ub = rhs->size[1];
  for (i14 = 0; i14 < loop_ub; i14++) {
    Vnext->data[i14] = 0.0;
  }

  emxInit_real_T(sp, &b_gamma, 2, &ee_emlrtRTEI, true);
  emlrtMEXProfilingStatement(3, isMexOutdated);
  i14 = b_gamma->size[0] * b_gamma->size[1];
  b_gamma->size[0] = 1;
  b_gamma->size[1] = rhs->size[1];
  emxEnsureCapacity_real_T(sp, b_gamma, i14, &ee_emlrtRTEI);
  loop_ub = rhs->size[1];
  for (i14 = 0; i14 < loop_ub; i14++) {
    b_gamma->data[i14] = 0.0;
  }

  emlrtMEXProfilingStatement(4, isMexOutdated);
  beta = b->data[0];
  emlrtMEXProfilingStatement(5, isMexOutdated);
  i14 = rhs->size[1];
  if (1 > i14) {
    emlrtDynamicBoundsCheckR2012b(1, 1, i14, &le_emlrtBCI, sp);
  }

  i14 = rhs->size[1];
  if (1 > i14) {
    emlrtDynamicBoundsCheckR2012b(1, 1, i14, &ke_emlrtBCI, sp);
  }

  Vnext->data[0] = rhs->data[0] / b->data[0];

  /*  Decomposition and forward subsitution phase */
  emlrtMEXProfilingStatement(6, isMexOutdated);
  i14 = rhs->size[1];
  for (segIDX = 0; segIDX <= i14 - 2; segIDX++) {
    emlrtMEXProfilingStatement(7, isMexOutdated);
    i15 = c->size[1];
    i16 = segIDX + 1;
    if ((i16 < 1) || (i16 > i15)) {
      emlrtDynamicBoundsCheckR2012b(i16, 1, i15, &me_emlrtBCI, sp);
    }

    i15 = b_gamma->size[1];
    i17 = 2 + segIDX;
    if ((i17 < 1) || (i17 > i15)) {
      emlrtDynamicBoundsCheckR2012b(i17, 1, i15, &ne_emlrtBCI, sp);
    }

    b_gamma->data[i17 - 1] = c->data[i16 - 1] / beta;
    emlrtMEXProfilingStatement(8, isMexOutdated);
    i15 = b->size[1];
    i16 = 2 + segIDX;
    if ((i16 < 1) || (i16 > i15)) {
      emlrtDynamicBoundsCheckR2012b(i16, 1, i15, &oe_emlrtBCI, sp);
    }

    i15 = a->size[1];
    i17 = 2 + segIDX;
    if ((i17 < 1) || (i17 > i15)) {
      emlrtDynamicBoundsCheckR2012b(i17, 1, i15, &pe_emlrtBCI, sp);
    }

    i15 = b_gamma->size[1];
    loop_ub = 2 + segIDX;
    if ((loop_ub < 1) || (loop_ub > i15)) {
      emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i15, &qe_emlrtBCI, sp);
    }

    beta = b->data[i16 - 1] - a->data[i17 - 1] * b_gamma->data[loop_ub - 1];
    emlrtMEXProfilingStatement(9, isMexOutdated);
    i15 = rhs->size[1];
    i16 = 2 + segIDX;
    if ((i16 < 1) || (i16 > i15)) {
      emlrtDynamicBoundsCheckR2012b(i16, 1, i15, &re_emlrtBCI, sp);
    }

    i15 = a->size[1];
    i17 = 2 + segIDX;
    if ((i17 < 1) || (i17 > i15)) {
      emlrtDynamicBoundsCheckR2012b(i17, 1, i15, &se_emlrtBCI, sp);
    }

    i15 = Vnext->size[1];
    loop_ub = segIDX + 1;
    if ((loop_ub < 1) || (loop_ub > i15)) {
      emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i15, &te_emlrtBCI, sp);
    }

    i15 = Vnext->size[1];
    i18 = 2 + segIDX;
    if ((i18 < 1) || (i18 > i15)) {
      emlrtDynamicBoundsCheckR2012b(i18, 1, i15, &ue_emlrtBCI, sp);
    }

    Vnext->data[i18 - 1] = (rhs->data[i16 - 1] - a->data[i17 - 1] * Vnext->
      data[loop_ub - 1]) / beta;
    emlrtMEXProfilingStatement(10, isMexOutdated);
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  /*  backward substituion phase */
  emlrtMEXProfilingStatement(11, isMexOutdated);
  i14 = (int32_T)((1.0 + (-1.0 - ((real_T)rhs->size[1] - 1.0))) / -1.0);
  emlrtForLoopVectorCheckR2012b((real_T)rhs->size[1] - 1.0, -1.0, 1.0,
    mxDOUBLE_CLASS, i14, &qe_emlrtRTEI, sp);
  for (segIDX = 0; segIDX < i14; segIDX++) {
    loop_ub = rhs->size[1] - segIDX;
    emlrtMEXProfilingStatement(12, isMexOutdated);
    i15 = Vnext->size[1];
    i16 = loop_ub - 1;
    if ((i16 < 1) || (i16 > i15)) {
      emlrtDynamicBoundsCheckR2012b(i16, 1, i15, &ve_emlrtBCI, sp);
    }

    i15 = b_gamma->size[1];
    if ((loop_ub < 1) || (loop_ub > i15)) {
      emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i15, &we_emlrtBCI, sp);
    }

    i15 = Vnext->size[1];
    if (loop_ub > i15) {
      emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i15, &xe_emlrtBCI, sp);
    }

    i15 = Vnext->size[1];
    i17 = loop_ub - 1;
    if ((i17 < 1) || (i17 > i15)) {
      emlrtDynamicBoundsCheckR2012b(i17, 1, i15, &ye_emlrtBCI, sp);
    }

    Vnext->data[i17 - 1] = Vnext->data[i16 - 1] - b_gamma->data[loop_ub - 1] *
      Vnext->data[loop_ub - 1];
    emlrtMEXProfilingStatement(13, isMexOutdated);
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFree_real_T(&b_gamma);
  emlrtMEXProfilingStatement(14, isMexOutdated);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (tridag.c) */
