/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * trans_rates_K.c
 *
 * Code generation for function 'trans_rates_K'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "trans_rates_K.h"
#include "stochAN_multi_emxutil.h"
#include "rdivide_helper.h"
#include "exp.h"
#include "stochAN_multi_data.h"

/* Variable Definitions */
static emlrtRSInfo ic_emlrtRSI = { 15, /* lineNo */
  "trans_rates_K",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pathName */
};

static emlrtRSInfo jc_emlrtRSI = { 14, /* lineNo */
  "trans_rates_K",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pathName */
};

static emlrtRSInfo kc_emlrtRSI = { 17, /* lineNo */
  "trans_rates_K",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pathName */
};

static emlrtRSInfo lc_emlrtRSI = { 16, /* lineNo */
  "trans_rates_K",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pathName */
};

static emlrtRTEInfo ud_emlrtRTEI = { 13,/* lineNo */
  1,                                   /* colNo */
  "trans_rates_K",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pName */
};

static emlrtRTEInfo vd_emlrtRTEI = { 14,/* lineNo */
  10,                                  /* colNo */
  "trans_rates_K",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pName */
};

static emlrtRTEInfo wd_emlrtRTEI = { 15,/* lineNo */
  10,                                  /* colNo */
  "trans_rates_K",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pName */
};

static emlrtRTEInfo xd_emlrtRTEI = { 15,/* lineNo */
  6,                                   /* colNo */
  "trans_rates_K",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pName */
};

static emlrtRTEInfo yd_emlrtRTEI = { 16,/* lineNo */
  10,                                  /* colNo */
  "trans_rates_K",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pName */
};

static emlrtRTEInfo ae_emlrtRTEI = { 17,/* lineNo */
  10,                                  /* colNo */
  "trans_rates_K",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pName */
};

static emlrtRTEInfo be_emlrtRTEI = { 17,/* lineNo */
  6,                                   /* colNo */
  "trans_rates_K",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pName */
};

static emlrtRTEInfo ce_emlrtRTEI = { 1,/* lineNo */
  19,                                  /* colNo */
  "trans_rates_K",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pName */
};

static emlrtECInfo cb_emlrtECI = { -1, /* nDims */
  14,                                  /* lineNo */
  1,                                   /* colNo */
  "trans_rates_K",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pName */
};

static emlrtECInfo db_emlrtECI = { -1, /* nDims */
  16,                                  /* lineNo */
  1,                                   /* colNo */
  "trans_rates_K",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/trans_rates_K.m"/* pName */
};

/* Function Definitions */
void trans_rates_K(const emlrtStack *sp, const emxArray_real_T *V, const real_T
                   Kins[6], emxArray_real_T *ratesK)
{
  int32_T i13;
  int32_T loop_ub;
  emxArray_int32_T *r9;
  emxArray_real_T *r10;
  emxArray_real_T *b_Kins;
  emxArray_real_T *r11;
  int32_T iv6[2];
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emlrtMEXProfilingFunctionEntry(trans_rates_K_complete_name, isMexOutdated);

  /*  trans_rates_K: This function calculates the potassium channel transition */
  /*  rates for a given voltage. */
  /*  */
  /*  Usage: ratesK = trans_rates_K(V,Kins); */
  /*  */
  /*    ratesK:    Voltage dependent transition rates  */
  /*    V:          Segment membrane potential (mV) */
  /*    Kins:       Array containing kinematic parameters. */
  /*  */
  /*  Jesse M. Resnick (resnick@uw.edu) � 2018 */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  i13 = ratesK->size[0] * ratesK->size[1];
  ratesK->size[0] = 2;
  ratesK->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, ratesK, i13, &ud_emlrtRTEI);
  loop_ub = V->size[1] << 1;
  for (i13 = 0; i13 < loop_ub; i13++) {
    ratesK->data[i13] = 0.0;
  }

  emxInit_int32_T(sp, &r9, 1, &ce_emlrtRTEI, true);

  /*        % a_m */
  emlrtMEXProfilingStatement(2, isMexOutdated);
  emlrtMEXProfilingStatement(3, isMexOutdated);
  loop_ub = V->size[1];
  i13 = r9->size[0];
  r9->size[0] = loop_ub;
  emxEnsureCapacity_int32_T(sp, r9, i13, &vd_emlrtRTEI);
  for (i13 = 0; i13 < loop_ub; i13++) {
    r9->data[i13] = i13;
  }

  emxInit_real_T(sp, &r10, 2, &wd_emlrtRTEI, true);
  i13 = r10->size[0] * r10->size[1];
  r10->size[0] = 1;
  r10->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, r10, i13, &wd_emlrtRTEI);
  loop_ub = V->size[0] * V->size[1];
  for (i13 = 0; i13 < loop_ub; i13++) {
    r10->data[i13] = (Kins[1] - V->data[i13]) / Kins[2];
  }

  st.site = &ic_emlrtRSI;
  b_exp(&st, r10);
  i13 = r10->size[0] * r10->size[1];
  loop_ub = r10->size[0] * r10->size[1];
  r10->size[0] = 1;
  emxEnsureCapacity_real_T(sp, r10, loop_ub, &xd_emlrtRTEI);
  loop_ub = i13 - 1;
  for (i13 = 0; i13 <= loop_ub; i13++) {
    r10->data[i13] = 1.0 - r10->data[i13];
  }

  emxInit_real_T(sp, &b_Kins, 2, &ib_emlrtRTEI, true);
  i13 = b_Kins->size[0] * b_Kins->size[1];
  b_Kins->size[0] = 1;
  b_Kins->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, b_Kins, i13, &ib_emlrtRTEI);
  loop_ub = V->size[0] * V->size[1];
  for (i13 = 0; i13 < loop_ub; i13++) {
    b_Kins->data[i13] = Kins[0] * (V->data[i13] - Kins[1]);
  }

  emxInit_real_T(sp, &r11, 2, &ce_emlrtRTEI, true);
  st.site = &jc_emlrtRSI;
  b_rdivide_helper(&st, b_Kins, r10, r11);
  iv6[0] = 1;
  iv6[1] = r9->size[0];
  emlrtSubAssignSizeCheckR2012b(&iv6[0], 2, &(*(int32_T (*)[2])r11->size)[0], 2,
    &cb_emlrtECI, sp);
  loop_ub = r11->size[1];
  for (i13 = 0; i13 < loop_ub; i13++) {
    ratesK->data[r9->data[i13] << 1] = r11->data[i13];
  }

  /*       %b_m */
  emlrtMEXProfilingStatement(4, isMexOutdated);
  emlrtMEXProfilingStatement(5, isMexOutdated);
  loop_ub = ratesK->size[1];
  i13 = r9->size[0];
  r9->size[0] = loop_ub;
  emxEnsureCapacity_int32_T(sp, r9, i13, &yd_emlrtRTEI);
  for (i13 = 0; i13 < loop_ub; i13++) {
    r9->data[i13] = i13;
  }

  i13 = r10->size[0] * r10->size[1];
  r10->size[0] = 1;
  r10->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, r10, i13, &ae_emlrtRTEI);
  loop_ub = V->size[0] * V->size[1];
  for (i13 = 0; i13 < loop_ub; i13++) {
    r10->data[i13] = (V->data[i13] - Kins[4]) / Kins[5];
  }

  st.site = &kc_emlrtRSI;
  b_exp(&st, r10);
  i13 = r10->size[0] * r10->size[1];
  loop_ub = r10->size[0] * r10->size[1];
  r10->size[0] = 1;
  emxEnsureCapacity_real_T(sp, r10, loop_ub, &be_emlrtRTEI);
  loop_ub = i13 - 1;
  for (i13 = 0; i13 <= loop_ub; i13++) {
    r10->data[i13] = 1.0 - r10->data[i13];
  }

  i13 = b_Kins->size[0] * b_Kins->size[1];
  b_Kins->size[0] = 1;
  b_Kins->size[1] = V->size[1];
  emxEnsureCapacity_real_T(sp, b_Kins, i13, &ib_emlrtRTEI);
  loop_ub = V->size[0] * V->size[1];
  for (i13 = 0; i13 < loop_ub; i13++) {
    b_Kins->data[i13] = Kins[3] * (Kins[4] - V->data[i13]);
  }

  st.site = &lc_emlrtRSI;
  b_rdivide_helper(&st, b_Kins, r10, r11);
  iv6[0] = 1;
  iv6[1] = r9->size[0];
  emlrtSubAssignSizeCheckR2012b(&iv6[0], 2, &(*(int32_T (*)[2])r11->size)[0], 2,
    &db_emlrtECI, sp);
  loop_ub = r11->size[1];
  emxFree_real_T(&b_Kins);
  emxFree_real_T(&r10);
  for (i13 = 0; i13 < loop_ub; i13++) {
    ratesK->data[1 + (r9->data[i13] << 1)] = r11->data[i13];
  }

  emxFree_real_T(&r11);
  emxFree_int32_T(&r9);
  emlrtMEXProfilingStatement(6, isMexOutdated);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (trans_rates_K.c) */
