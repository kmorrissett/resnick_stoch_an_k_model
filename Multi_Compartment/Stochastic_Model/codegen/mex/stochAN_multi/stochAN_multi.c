/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * stochAN_multi.c
 *
 * Code generation for function 'stochAN_multi'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "rdivide_helper.h"
#include "stochAN_multi_emxutil.h"
#include "error1.h"
#include "eml_int_forloop_overflow_check.h"
#include "any.h"
#include "toLogicalCheck.h"
#include "mod.h"
#include "tridag.h"
#include "update_Ks_channels.h"
#include "update_Kf_channels.h"
#include "update_Naf_channels.h"
#include "trans_rates_K.h"
#include "trans_rates_Naf.h"
#include "initialize_Ks_channels.h"
#include "initialize_Kf_channels.h"
#include "initialize_Naf_channels.h"
#include "repmat.h"
#include "sqrt.h"
#include "power.h"
#include "asin.h"
#include "stochAN_multi_data.h"
#include "blas.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 19,    /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 65,  /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 66,  /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 64,  /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 67,  /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 68,  /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 70,  /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 71,  /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 72,  /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 105, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 106, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 107, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 112, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 113, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo o_emlrtRSI = { 114, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo p_emlrtRSI = { 122, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo q_emlrtRSI = { 123, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo r_emlrtRSI = { 124, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo s_emlrtRSI = { 129, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo t_emlrtRSI = { 130, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo u_emlrtRSI = { 131, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo v_emlrtRSI = { 162, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo w_emlrtRSI = { 165, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo x_emlrtRSI = { 166, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo y_emlrtRSI = { 168, /* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo ab_emlrtRSI = { 169,/* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo bb_emlrtRSI = { 178,/* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo cb_emlrtRSI = { 184,/* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo db_emlrtRSI = { 185,/* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo eb_emlrtRSI = { 188,/* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo fb_emlrtRSI = { 191,/* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo gb_emlrtRSI = { 192,/* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo hb_emlrtRSI = { 193,/* lineNo */
  "stochAN_multi",                     /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pathName */
};

static emlrtRSInfo tb_emlrtRSI = { 52, /* lineNo */
  "eml_mtimes_helper",                 /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"/* pathName */
};

static emlrtRSInfo ub_emlrtRSI = { 21, /* lineNo */
  "eml_mtimes_helper",                 /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"/* pathName */
};

static emlrtRSInfo vb_emlrtRSI = { 88, /* lineNo */
  "mtimes",                            /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/eml/+coder/+internal/+blas/mtimes.m"/* pathName */
};

static emlrtRSInfo wb_emlrtRSI = { 32, /* lineNo */
  "xdotu",                             /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/eml/+coder/+internal/+blas/xdotu.m"/* pathName */
};

static emlrtRSInfo ad_emlrtRSI = { 41, /* lineNo */
  "find",                              /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/find.m"/* pathName */
};

static emlrtRSInfo bd_emlrtRSI = { 153,/* lineNo */
  "find",                              /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/find.m"/* pathName */
};

static emlrtRSInfo cd_emlrtRSI = { 377,/* lineNo */
  "find",                              /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/find.m"/* pathName */
};

static emlrtMCInfo emlrtMCI = { 34,    /* lineNo */
  9,                                   /* colNo */
  "rng",                               /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/randfun/rng.m"/* pName */
};

static emlrtRTEInfo emlrtRTEI = { 59,  /* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo b_emlrtRTEI = { 60,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo c_emlrtRTEI = { 61,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo d_emlrtRTEI = { 65,/* lineNo */
  34,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo e_emlrtRTEI = { 65,/* lineNo */
  6,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo f_emlrtRTEI = { 66,/* lineNo */
  33,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo g_emlrtRTEI = { 66,/* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo h_emlrtRTEI = { 64,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo i_emlrtRTEI = { 68,/* lineNo */
  6,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo j_emlrtRTEI = { 67,/* lineNo */
  33,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo k_emlrtRTEI = { 69,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo l_emlrtRTEI = { 58,/* lineNo */
  6,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo m_emlrtRTEI = { 77,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo n_emlrtRTEI = { 72,/* lineNo */
  13,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo o_emlrtRTEI = { 79,/* lineNo */
  10,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo p_emlrtRTEI = { 72,/* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo q_emlrtRTEI = { 79,/* lineNo */
  27,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo r_emlrtRTEI = { 79,/* lineNo */
  42,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo s_emlrtRTEI = { 80,/* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo t_emlrtRTEI = { 78,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo u_emlrtRTEI = { 82,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo v_emlrtRTEI = { 87,/* lineNo */
  9,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo w_emlrtRTEI = { 87,/* lineNo */
  26,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo x_emlrtRTEI = { 87,/* lineNo */
  41,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo y_emlrtRTEI = { 88,/* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo ab_emlrtRTEI = { 86,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo bb_emlrtRTEI = { 93,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo cb_emlrtRTEI = { 94,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo db_emlrtRTEI = { 95,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo eb_emlrtRTEI = { 99,/* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo fb_emlrtRTEI = { 100,/* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo gb_emlrtRTEI = { 101,/* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo hb_emlrtRTEI = { 102,/* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo jb_emlrtRTEI = { 118,/* lineNo */
  9,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo kb_emlrtRTEI = { 119,/* lineNo */
  9,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo lb_emlrtRTEI = { 122,/* lineNo */
  39,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo mb_emlrtRTEI = { 123,/* lineNo */
  37,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo nb_emlrtRTEI = { 124,/* lineNo */
  37,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo ob_emlrtRTEI = { 137,/* lineNo */
  16,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo pb_emlrtRTEI = { 137,/* lineNo */
  9,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo qb_emlrtRTEI = { 138,/* lineNo */
  16,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo rb_emlrtRTEI = { 138,/* lineNo */
  9,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo sb_emlrtRTEI = { 139,/* lineNo */
  16,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo tb_emlrtRTEI = { 139,/* lineNo */
  9,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo ub_emlrtRTEI = { 140,/* lineNo */
  9,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo vb_emlrtRTEI = { 149,/* lineNo */
  33,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo wb_emlrtRTEI = { 150,/* lineNo */
  15,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo xb_emlrtRTEI = { 151,/* lineNo */
  32,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo yb_emlrtRTEI = { 152,/* lineNo */
  15,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo ac_emlrtRTEI = { 152,/* lineNo */
  44,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo bc_emlrtRTEI = { 153,/* lineNo */
  15,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo cc_emlrtRTEI = { 153,/* lineNo */
  33,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo dc_emlrtRTEI = { 165,/* lineNo */
  24,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo ec_emlrtRTEI = { 165,/* lineNo */
  50,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo fc_emlrtRTEI = { 165,/* lineNo */
  23,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo gc_emlrtRTEI = { 153,/* lineNo */
  13,                                  /* colNo */
  "find",                              /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/find.m"/* pName */
};

static emlrtRTEInfo hc_emlrtRTEI = { 41,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/find.m"/* pName */
};

static emlrtRTEInfo ic_emlrtRTEI = { 165,/* lineNo */
  9,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo jc_emlrtRTEI = { 167,/* lineNo */
  23,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo kc_emlrtRTEI = { 180,/* lineNo */
  58,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo lc_emlrtRTEI = { 191,/* lineNo */
  16,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo mc_emlrtRTEI = { 167,/* lineNo */
  33,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo nc_emlrtRTEI = { 192,/* lineNo */
  27,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo oc_emlrtRTEI = { 168,/* lineNo */
  20,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo pc_emlrtRTEI = { 186,/* lineNo */
  13,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo qc_emlrtRTEI = { 169,/* lineNo */
  62,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo rc_emlrtRTEI = { 187,/* lineNo */
  13,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo sc_emlrtRTEI = { 397,/* lineNo */
  5,                                   /* colNo */
  "find",                              /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/find.m"/* pName */
};

static emlrtRTEInfo tc_emlrtRTEI = { 58,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo uc_emlrtRTEI = { 67,/* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo vc_emlrtRTEI = { 105,/* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo wc_emlrtRTEI = { 106,/* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo xc_emlrtRTEI = { 107,/* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo yc_emlrtRTEI = { 1,/* lineNo */
  22,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo ad_emlrtRTEI = { 71,/* lineNo */
  14,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo bd_emlrtRTEI = { 33,/* lineNo */
  6,                                   /* colNo */
  "find",                              /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/find.m"/* pName */
};

static emlrtRTEInfo cd_emlrtRTEI = { 65,/* lineNo */
  33,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtRTEInfo ge_emlrtRTEI = { 387,/* lineNo */
  1,                                   /* colNo */
  "find",                              /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/find.m"/* pName */
};

static emlrtRTEInfo he_emlrtRTEI = { 83,/* lineNo */
  1,                                   /* colNo */
  "find",                              /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/find.m"/* pName */
};

static emlrtRTEInfo ie_emlrtRTEI = { 83,/* lineNo */
  23,                                  /* colNo */
  "eml_mtimes_helper",                 /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"/* pName */
};

static emlrtRTEInfo je_emlrtRTEI = { 88,/* lineNo */
  23,                                  /* colNo */
  "eml_mtimes_helper",                 /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"/* pName */
};

static emlrtECInfo emlrtECI = { -1,    /* nDims */
  72,                                  /* lineNo */
  5,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo emlrtBCI = { -1,    /* iFirst */
  -1,                                  /* iLast */
  72,                                  /* lineNo */
  15,                                  /* colNo */
  "eCAPout",                           /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo b_emlrtECI = { -1,  /* nDims */
  180,                                 /* lineNo */
  17,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo b_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  180,                                 /* lineNo */
  31,                                  /* colNo */
  "vOutput",                           /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo emlrtDCI = { 180,   /* lineNo */
  31,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo c_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  180,                                 /* lineNo */
  25,                                  /* colNo */
  "vOutput",                           /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo c_emlrtECI = { 2,   /* nDims */
  187,                                 /* lineNo */
  18,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtECInfo d_emlrtECI = { 2,   /* nDims */
  186,                                 /* lineNo */
  19,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtECInfo e_emlrtECI = { 2,   /* nDims */
  186,                                 /* lineNo */
  24,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo d_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  172,                                 /* lineNo */
  35,                                  /* colNo */
  "spiked",                            /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo f_emlrtECI = { -1,  /* nDims */
  167,                                 /* lineNo */
  13,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtECInfo g_emlrtECI = { 2,   /* nDims */
  165,                                 /* lineNo */
  23,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtECInfo h_emlrtECI = { -1,  /* nDims */
  151,                                 /* lineNo */
  9,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo e_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  151,                                 /* lineNo */
  15,                                  /* colNo */
  "rhs",                               /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo f_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  151,                                 /* lineNo */
  13,                                  /* colNo */
  "rhs",                               /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo g_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  145,                                 /* lineNo */
  13,                                  /* colNo */
  "rhs",                               /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo i_emlrtECI = { 2,   /* nDims */
  151,                                 /* lineNo */
  32,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo h_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  153,                                 /* lineNo */
  44,                                  /* colNo */
  "Iion",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo i_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  153,                                 /* lineNo */
  42,                                  /* colNo */
  "Iion",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo j_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  153,                                 /* lineNo */
  24,                                  /* colNo */
  "dist",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo k_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  153,                                 /* lineNo */
  22,                                  /* colNo */
  "dist",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo j_emlrtECI = { 2,   /* nDims */
  152,                                 /* lineNo */
  44,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo l_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  152,                                 /* lineNo */
  61,                                  /* colNo */
  "Vm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo m_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  152,                                 /* lineNo */
  48,                                  /* colNo */
  "c",                                 /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo n_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  152,                                 /* lineNo */
  46,                                  /* colNo */
  "c",                                 /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo k_emlrtECI = { 2,   /* nDims */
  152,                                 /* lineNo */
  15,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo o_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  152,                                 /* lineNo */
  35,                                  /* colNo */
  "Vm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo p_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  152,                                 /* lineNo */
  33,                                  /* colNo */
  "Vm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo q_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  152,                                 /* lineNo */
  22,                                  /* colNo */
  "rhsM",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo r_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  151,                                 /* lineNo */
  50,                                  /* colNo */
  "Vm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo s_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  151,                                 /* lineNo */
  48,                                  /* colNo */
  "Vm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo t_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  151,                                 /* lineNo */
  37,                                  /* colNo */
  "a",                                 /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo u_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  151,                                 /* lineNo */
  35,                                  /* colNo */
  "a",                                 /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo l_emlrtECI = { -1,  /* nDims */
  149,                                 /* lineNo */
  9,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo v_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  149,                                 /* lineNo */
  16,                                  /* colNo */
  "dist",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo w_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  149,                                 /* lineNo */
  14,                                  /* colNo */
  "dist",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo m_emlrtECI = { 2,   /* nDims */
  149,                                 /* lineNo */
  33,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtECInfo n_emlrtECI = { 2,   /* nDims */
  150,                                 /* lineNo */
  15,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtECInfo o_emlrtECI = { 2,   /* nDims */
  150,                                 /* lineNo */
  28,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo x_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  150,                                 /* lineNo */
  57,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo y_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  150,                                 /* lineNo */
  55,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ab_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  150,                                 /* lineNo */
  35,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo bb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  150,                                 /* lineNo */
  33,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  150,                                 /* lineNo */
  19,                                  /* colNo */
  "a",                                 /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo db_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  150,                                 /* lineNo */
  17,                                  /* colNo */
  "a",                                 /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo p_emlrtECI = { 2,   /* nDims */
  149,                                 /* lineNo */
  46,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo eb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  149,                                 /* lineNo */
  73,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  149,                                 /* lineNo */
  71,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  149,                                 /* lineNo */
  53,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  149,                                 /* lineNo */
  51,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ib_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  149,                                 /* lineNo */
  37,                                  /* colNo */
  "c",                                 /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  149,                                 /* lineNo */
  35,                                  /* colNo */
  "c",                                 /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  146,                                 /* lineNo */
  36,                                  /* colNo */
  "Iion",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  146,                                 /* lineNo */
  23,                                  /* colNo */
  "dist",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  144,                                 /* lineNo */
  14,                                  /* colNo */
  "dist",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  145,                                 /* lineNo */
  42,                                  /* colNo */
  "Vm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ob_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  145,                                 /* lineNo */
  29,                                  /* colNo */
  "Vm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  144,                                 /* lineNo */
  38,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  144,                                 /* lineNo */
  30,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo q_emlrtECI = { 2,   /* nDims */
  140,                                 /* lineNo */
  16,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtECInfo r_emlrtECI = { 2,   /* nDims */
  139,                                 /* lineNo */
  16,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtECInfo s_emlrtECI = { 2,   /* nDims */
  138,                                 /* lineNo */
  16,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtECInfo t_emlrtECI = { 2,   /* nDims */
  137,                                 /* lineNo */
  16,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo rb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  131,                                 /* lineNo */
  16,                                  /* colNo */
  "Ks",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  131,                                 /* lineNo */
  71,                                  /* colNo */
  "Ks",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  114,                                 /* lineNo */
  12,                                  /* colNo */
  "Ks",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ub_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  130,                                 /* lineNo */
  16,                                  /* colNo */
  "Kf",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  130,                                 /* lineNo */
  71,                                  /* colNo */
  "Kf",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  113,                                 /* lineNo */
  12,                                  /* colNo */
  "Kf",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  129,                                 /* lineNo */
  17,                                  /* colNo */
  "Naf",                               /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo b_emlrtDCI = { 129, /* lineNo */
  17,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo yb_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  129,                                 /* lineNo */
  74,                                  /* colNo */
  "Naf",                               /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo c_emlrtDCI = { 129, /* lineNo */
  74,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo ac_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  112,                                 /* lineNo */
  13,                                  /* colNo */
  "Naf",                               /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo d_emlrtDCI = { 112, /* lineNo */
  13,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo bc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  127,                                 /* lineNo */
  30,                                  /* colNo */
  "nodeIDX",                           /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  118,                                 /* lineNo */
  22,                                  /* colNo */
  "Istim",                             /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  110,                                 /* lineNo */
  26,                                  /* colNo */
  "nodeIDX",                           /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo ke_emlrtRTEI = { 97,/* lineNo */
  13,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtECInfo u_emlrtECI = { 2,   /* nDims */
  87,                                  /* lineNo */
  6,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo ec_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  88,                                  /* lineNo */
  14,                                  /* colNo */
  "cm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  88,                                  /* lineNo */
  12,                                  /* colNo */
  "cm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  87,                                  /* lineNo */
  46,                                  /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  87,                                  /* lineNo */
  44,                                  /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ic_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  87,                                  /* lineNo */
  31,                                  /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo jc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  87,                                  /* lineNo */
  29,                                  /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  87,                                  /* lineNo */
  14,                                  /* colNo */
  "rm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo lc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  87,                                  /* lineNo */
  12,                                  /* colNo */
  "rm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo mc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  86,                                  /* lineNo */
  39,                                  /* colNo */
  "cm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  86,                                  /* lineNo */
  27,                                  /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo oc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  86,                                  /* lineNo */
  16,                                  /* colNo */
  "rm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo v_emlrtECI = { 2,   /* nDims */
  79,                                  /* lineNo */
  7,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtBCInfo pc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  80,                                  /* lineNo */
  14,                                  /* colNo */
  "cm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  80,                                  /* lineNo */
  12,                                  /* colNo */
  "cm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  79,                                  /* lineNo */
  47,                                  /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  79,                                  /* lineNo */
  45,                                  /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo tc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  79,                                  /* lineNo */
  32,                                  /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo uc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  79,                                  /* lineNo */
  30,                                  /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  79,                                  /* lineNo */
  15,                                  /* colNo */
  "rm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  79,                                  /* lineNo */
  13,                                  /* colNo */
  "rm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  78,                                  /* lineNo */
  36,                                  /* colNo */
  "cm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yc_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  78,                                  /* lineNo */
  25,                                  /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ad_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  78,                                  /* lineNo */
  14,                                  /* colNo */
  "rm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo w_emlrtECI = { 2,   /* nDims */
  65,                                  /* lineNo */
  6,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m"/* pName */
};

static emlrtDCInfo e_emlrtDCI = { 59,  /* lineNo */
  17,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo f_emlrtDCI = { 59,  /* lineNo */
  17,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo g_emlrtDCI = { 59,  /* lineNo */
  34,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo h_emlrtDCI = { 59,  /* lineNo */
  34,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo i_emlrtDCI = { 60,  /* lineNo */
  17,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo j_emlrtDCI = { 61,  /* lineNo */
  19,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo k_emlrtDCI = { 61,  /* lineNo */
  45,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo l_emlrtDCI = { 61,  /* lineNo */
  45,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo m_emlrtDCI = { 59,  /* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo n_emlrtDCI = { 60,  /* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo o_emlrtDCI = { 61,  /* lineNo */
  1,                                   /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo bd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  81,                                  /* lineNo */
  5,                                   /* colNo */
  "rm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo cd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  81,                                  /* lineNo */
  5,                                   /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo dd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  81,                                  /* lineNo */
  5,                                   /* colNo */
  "cm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ed_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  89,                                  /* lineNo */
  6,                                   /* colNo */
  "rm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  89,                                  /* lineNo */
  6,                                   /* colNo */
  "ra",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo gd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  89,                                  /* lineNo */
  6,                                   /* colNo */
  "cm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo hd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  112,                                 /* lineNo */
  51,                                  /* colNo */
  "ratesNaf",                          /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo p_emlrtDCI = { 112, /* lineNo */
  51,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo id_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  112,                                 /* lineNo */
  70,                                  /* colNo */
  "numNaf",                            /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo q_emlrtDCI = { 112, /* lineNo */
  70,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo jd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  113,                                 /* lineNo */
  50,                                  /* colNo */
  "ratesKf",                           /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo kd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  113,                                 /* lineNo */
  68,                                  /* colNo */
  "numKf",                             /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ld_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  114,                                 /* lineNo */
  50,                                  /* colNo */
  "ratesKs",                           /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo md_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  114,                                 /* lineNo */
  68,                                  /* colNo */
  "numKs",                             /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo nd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  129,                                 /* lineNo */
  51,                                  /* colNo */
  "ratesNaf",                          /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo r_emlrtDCI = { 129, /* lineNo */
  51,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo od_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  130,                                 /* lineNo */
  50,                                  /* colNo */
  "ratesKf",                           /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo pd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  131,                                 /* lineNo */
  50,                                  /* colNo */
  "ratesKs",                           /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo qd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  156,                                 /* lineNo */
  22,                                  /* colNo */
  "a",                                 /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo rd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  156,                                 /* lineNo */
  31,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo sd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  156,                                 /* lineNo */
  51,                                  /* colNo */
  "Vapp",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo td_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  156,                                 /* lineNo */
  9,                                   /* colNo */
  "dist",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ud_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  157,                                 /* lineNo */
  21,                                  /* colNo */
  "a",                                 /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo vd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  157,                                 /* lineNo */
  31,                                  /* colNo */
  "Vm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo wd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  158,                                 /* lineNo */
  15,                                  /* colNo */
  "rhsM",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo xd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  158,                                 /* lineNo */
  28,                                  /* colNo */
  "Vm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo yd_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  159,                                 /* lineNo */
  19,                                  /* colNo */
  "dist",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ae_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  159,                                 /* lineNo */
  35,                                  /* colNo */
  "Iion",                              /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo be_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  157,                                 /* lineNo */
  9,                                   /* colNo */
  "rhs",                               /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ce_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  165,                                 /* lineNo */
  27,                                  /* colNo */
  "Vm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo s_emlrtDCI = { 165, /* lineNo */
  27,                                  /* colNo */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo de_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  165,                                 /* lineNo */
  56,                                  /* colNo */
  "Vprev",                             /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ee_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  167,                                 /* lineNo */
  23,                                  /* colNo */
  "numSpikes",                         /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo fe_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  167,                                 /* lineNo */
  43,                                  /* colNo */
  "numSpikes",                         /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ge_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  173,                                 /* lineNo */
  17,                                  /* colNo */
  "spikeOutput",                       /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo he_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  173,                                 /* lineNo */
  17,                                  /* colNo */
  "numSpikes",                         /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo ie_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  188,                                 /* lineNo */
  13,                                  /* colNo */
  "eCAPout",                           /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo je_emlrtBCI = { -1, /* iFirst */
  -1,                                  /* iLast */
  193,                                 /* lineNo */
  78,                                  /* colNo */
  "Vm",                                /* aName */
  "stochAN_multi",                     /* fName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/stochAN_multi.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRSInfo fd_emlrtRSI = { 34, /* lineNo */
  "rng",                               /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/randfun/rng.m"/* pathName */
};

/* Function Declarations */
static void rng(const emlrtStack *sp, const mxArray *b, emlrtMCInfo *location);

/* Function Definitions */
static void rng(const emlrtStack *sp, const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(sp, 0, NULL, 1, &pArray, "rng", true, location);
}

void stochAN_multi(const emlrtStack *sp, const emxArray_real_T *Istim, const
                   struct0_T *segments, const struct1_T *model, const struct6_T *
                   options, emxArray_real_T *varargout_1, emxArray_real32_T
                   *varargout_2, emxArray_real32_T *varargout_3)
{
  const mxArray *y;
  const mxArray *m0;
  real_T Vrest;
  real_T Vthresh;
  real_T E_K;
  real_T maxVolt;
  real_T maxSpikes;
  real_T recV;
  real_T recECAP;
  real_T Vsample;
  int32_T varargin_2;
  int32_T numSegments;
  real_T dt;
  real_T eCAP1;
  int32_T i1;
  int32_T i2;
  int32_T loop_ub;
  emxArray_real_T *b_segments;
  emxArray_real_T *r0;
  emxArray_real_T *iKs;
  int32_T iNaf[2];
  int32_T b_iKs[2];
  emxArray_real_T *r1;
  emxArray_real_T *fieldResist;
  emxArray_real_T *ecapRes;
  emxArray_real_T *b_ecapRes;
  emxArray_int32_T *r2;
  emxArray_real_T *a;
  emxArray_real_T *numSpikes;
  int32_T i3;
  ptrdiff_t n_t;
  ptrdiff_t incx_t;
  ptrdiff_t incy_t;
  int32_T i4;
  int32_T i5;
  emxArray_real32_T *r3;
  int32_T iv3[1];
  int32_T c_ecapRes[2];
  emxArray_real_T *r4;
  emxArray_real_T *b;
  emxArray_real_T *c;
  emxArray_real_T *rhsM;
  emxArray_real_T *Naf;
  emxArray_real_T *Kf;
  emxArray_real_T *Ks;
  emxArray_real_T *Vm;
  emxArray_real_T *b_numSpikes;
  emxArray_real_T *rhs;
  emxArray_real_T *dist;
  emxArray_real_T *ratesNaf;
  emxArray_real_T *ratesKf;
  emxArray_real_T *ratesKs;
  emxArray_real_T *Vapp;
  emxArray_real_T *Vprev;
  emxArray_real_T *b_iNaf;
  emxArray_boolean_T *r5;
  emxArray_boolean_T *x;
  emxArray_int32_T *ii;
  emxArray_boolean_T *c_numSpikes;
  int32_T monte;
  int32_T IDX;
  int32_T it;
  real_T dv0[8];
  int32_T nx;
  real_T dv1[5];
  real_T dv2[2];
  int32_T b_Vm[2];
  int32_T i6;
  int32_T i7;
  int32_T i8;
  int32_T idx;
  boolean_T overflow;
  boolean_T exitg1;
  int32_T iv4[3];
  int32_T badIDX_size[2];
  real_T badIDX_data[1];
  int32_T Vm_size[2];
  real_T Vm_data[1];
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emlrtMEXProfilingFunctionEntry(stochAN_multi_complete_name, isMexOutdated);

  /*  This function simulates a neural fiber stimulated by a disc electrode */
  /*  using stochastic Hodgkin and Huxley dynamics modeled via Jump Markov */
  /*  Process. V4 allows expansion of the terminal compartments to provide low */
  /*  impedence return to ground. It requires the same inputs as V1. */
  /*  */
  /*  USAGE: vOutput = stochAN_multiV3(Istim,segments,model,options) */
  /*  */
  /*  spikeOutput:  2D matrix of spike times (# segments x # of spikes) */
  /*  vOutput:  2D matrix of fiber voltages (# segments x # of samples) */
  /*  */
  /*  Istim:    array of stimulus currents segments: structure containing */
  /*  segment electrical/structural properties model:    structure containing */
  /*  HH model parameters options:  structure containing experimental options */
  /*  */
  /*  Jesse M. Resnick (resnick@uw.edu) � 2018 */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  st.site = &emlrtRSI;
  y = NULL;
  m0 = emlrtCreateDoubleScalar(options->seed);
  emlrtAssign(&y, m0);
  b_st.site = &fd_emlrtRSI;
  rng(&b_st, y, &emlrtMCI);

  /*  Set the rng seed using clock time. */
  /*  Extract values from passed structures. */
  emlrtMEXProfilingStatement(2, isMexOutdated);
  Vrest = model->Vrest;
  emlrtMEXProfilingStatement(3, isMexOutdated);
  Vthresh = model->Vthresh;
  emlrtMEXProfilingStatement(4, isMexOutdated);
  emlrtMEXProfilingStatement(5, isMexOutdated);
  emlrtMEXProfilingStatement(6, isMexOutdated);
  emlrtMEXProfilingStatement(7, isMexOutdated);
  emlrtMEXProfilingStatement(8, isMexOutdated);
  emlrtMEXProfilingStatement(9, isMexOutdated);
  E_K = model->Kf.E_K;
  emlrtMEXProfilingStatement(10, isMexOutdated);
  emlrtMEXProfilingStatement(11, isMexOutdated);
  emlrtMEXProfilingStatement(12, isMexOutdated);
  maxVolt = options->maxVoltage;
  emlrtMEXProfilingStatement(13, isMexOutdated);
  maxSpikes = options->maxSpikes;
  emlrtMEXProfilingStatement(14, isMexOutdated);
  recV = options->recV;
  emlrtMEXProfilingStatement(15, isMexOutdated);
  recECAP = options->recECAP;
  emlrtMEXProfilingStatement(16, isMexOutdated);
  Vsample = options->Vsample;
  emlrtMEXProfilingStatement(17, isMexOutdated);
  emlrtMEXProfilingStatement(18, isMexOutdated);
  emlrtMEXProfilingStatement(19, isMexOutdated);
  emlrtMEXProfilingStatement(20, isMexOutdated);
  emlrtMEXProfilingStatement(21, isMexOutdated);
  emlrtMEXProfilingStatement(22, isMexOutdated);
  emlrtMEXProfilingStatement(23, isMexOutdated);
  emlrtMEXProfilingStatement(24, isMexOutdated);
  emlrtMEXProfilingStatement(25, isMexOutdated);
  emlrtMEXProfilingStatement(26, isMexOutdated);
  emlrtMEXProfilingStatement(27, isMexOutdated);
  varargin_2 = segments->rm->size[1];
  numSegments = segments->rm->size[1] - 1;

  /*  Time step */
  emlrtMEXProfilingStatement(28, isMexOutdated);
  dt = model->dt;
  emlrtMEXProfilingStatement(29, isMexOutdated);

  /*  Number of time steps. */
  /*  Initialize output arrays */
  emlrtMEXProfilingStatement(30, isMexOutdated);
  emlrtMEXProfilingStatement(31, isMexOutdated);
  eCAP1 = muDoubleScalarFloor(((real_T)Istim->size[1] - 1.0) * model->dt /
    options->Vsample + 1.0);
  i1 = varargout_2->size[0] * varargout_2->size[1] * varargout_2->size[2];
  if (!(options->numMonte >= 0.0)) {
    emlrtNonNegativeCheckR2012b(options->numMonte, &f_emlrtDCI, sp);
  }

  if (options->numMonte != (int32_T)muDoubleScalarFloor(options->numMonte)) {
    emlrtIntegerCheckR2012b(options->numMonte, &e_emlrtDCI, sp);
  }

  varargout_2->size[0] = (int32_T)options->numMonte;
  if (!(eCAP1 >= 0.0)) {
    emlrtNonNegativeCheckR2012b(eCAP1, &h_emlrtDCI, sp);
  }

  i2 = (int32_T)eCAP1;
  if (eCAP1 != i2) {
    emlrtIntegerCheckR2012b(eCAP1, &g_emlrtDCI, sp);
  }

  varargout_2->size[1] = i2;
  varargout_2->size[2] = segments->rm->size[1];
  emxEnsureCapacity_real32_T(sp, varargout_2, i1, &emlrtRTEI);
  if (options->numMonte != (int32_T)muDoubleScalarFloor(options->numMonte)) {
    emlrtIntegerCheckR2012b(options->numMonte, &m_emlrtDCI, sp);
  }

  if (eCAP1 != (int32_T)eCAP1) {
    emlrtIntegerCheckR2012b(eCAP1, &m_emlrtDCI, sp);
  }

  loop_ub = (int32_T)options->numMonte * (int32_T)eCAP1 * segments->rm->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    varargout_2->data[i1] = 0.0F;
  }

  emlrtMEXProfilingStatement(32, isMexOutdated);
  i1 = varargout_3->size[0] * varargout_3->size[1];
  if (options->numMonte != (int32_T)muDoubleScalarFloor(options->numMonte)) {
    emlrtIntegerCheckR2012b(options->numMonte, &i_emlrtDCI, sp);
  }

  varargout_3->size[0] = (int32_T)options->numMonte;
  varargout_3->size[1] = Istim->size[1];
  emxEnsureCapacity_real32_T(sp, varargout_3, i1, &b_emlrtRTEI);
  if (options->numMonte != (int32_T)muDoubleScalarFloor(options->numMonte)) {
    emlrtIntegerCheckR2012b(options->numMonte, &n_emlrtDCI, sp);
  }

  loop_ub = (int32_T)options->numMonte * Istim->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    varargout_3->data[i1] = 0.0F;
  }

  emlrtMEXProfilingStatement(33, isMexOutdated);
  i1 = varargout_1->size[0] * varargout_1->size[1] * varargout_1->size[2];
  if (options->numMonte != (int32_T)muDoubleScalarFloor(options->numMonte)) {
    emlrtIntegerCheckR2012b(options->numMonte, &j_emlrtDCI, sp);
  }

  varargout_1->size[0] = (int32_T)options->numMonte;
  varargout_1->size[1] = segments->nodeIDX->size[1];
  if (!(options->maxSpikes >= 0.0)) {
    emlrtNonNegativeCheckR2012b(options->maxSpikes, &l_emlrtDCI, sp);
  }

  if (options->maxSpikes != (int32_T)muDoubleScalarFloor(options->maxSpikes)) {
    emlrtIntegerCheckR2012b(options->maxSpikes, &k_emlrtDCI, sp);
  }

  varargout_1->size[2] = (int32_T)options->maxSpikes;
  emxEnsureCapacity_real_T(sp, varargout_1, i1, &c_emlrtRTEI);
  if (options->numMonte != (int32_T)muDoubleScalarFloor(options->numMonte)) {
    emlrtIntegerCheckR2012b(options->numMonte, &o_emlrtDCI, sp);
  }

  if (options->maxSpikes != (int32_T)muDoubleScalarFloor(options->maxSpikes)) {
    emlrtIntegerCheckR2012b(options->maxSpikes, &o_emlrtDCI, sp);
  }

  loop_ub = (int32_T)options->numMonte * segments->nodeIDX->size[1] * (int32_T)
    options->maxSpikes;
  for (i1 = 0; i1 < loop_ub; i1++) {
    varargout_1->data[i1] = rtNaN;
  }

  emxInit_real_T(sp, &b_segments, 2, &d_emlrtRTEI, true);

  /*  Calculate field resistivity between electrode and each segment */
  emlrtMEXProfilingStatement(34, isMexOutdated);
  emlrtMEXProfilingStatement(35, isMexOutdated);
  emlrtMEXProfilingStatement(36, isMexOutdated);
  st.site = &b_emlrtRSI;
  b_st.site = &ib_emlrtRSI;
  eCAP1 = segments->zDistance * segments->zDistance;
  i1 = b_segments->size[0] * b_segments->size[1];
  b_segments->size[0] = 1;
  b_segments->size[1] = segments->xApp->size[1];
  emxEnsureCapacity_real_T(sp, b_segments, i1, &d_emlrtRTEI);
  loop_ub = segments->xApp->size[0] * segments->xApp->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    b_segments->data[i1] = (segments->xApp->data[i1] + options->XstimLoc) -
      model->elecR;
  }

  emxInit_real_T(sp, &r0, 2, &cd_emlrtRTEI, true);
  st.site = &b_emlrtRSI;
  power(&st, b_segments, r0);
  i1 = r0->size[0] * r0->size[1];
  i2 = r0->size[0] * r0->size[1];
  r0->size[0] = 1;
  emxEnsureCapacity_real_T(sp, r0, i2, &e_emlrtRTEI);
  loop_ub = i1 - 1;
  for (i1 = 0; i1 <= loop_ub; i1++) {
    r0->data[i1] += eCAP1;
  }

  st.site = &b_emlrtRSI;
  b_sqrt(&st, r0);
  st.site = &c_emlrtRSI;
  b_st.site = &ib_emlrtRSI;
  i1 = b_segments->size[0] * b_segments->size[1];
  b_segments->size[0] = 1;
  b_segments->size[1] = segments->xApp->size[1];
  emxEnsureCapacity_real_T(sp, b_segments, i1, &f_emlrtRTEI);
  loop_ub = segments->xApp->size[0] * segments->xApp->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    b_segments->data[i1] = (segments->xApp->data[i1] + options->XstimLoc) +
      model->elecR;
  }

  emxInit_real_T(sp, &iKs, 2, &tb_emlrtRTEI, true);
  st.site = &c_emlrtRSI;
  power(&st, b_segments, iKs);
  i1 = iKs->size[0] * iKs->size[1];
  i2 = iKs->size[0] * iKs->size[1];
  iKs->size[0] = 1;
  emxEnsureCapacity_real_T(sp, iKs, i2, &g_emlrtRTEI);
  loop_ub = i1 - 1;
  for (i1 = 0; i1 <= loop_ub; i1++) {
    iKs->data[i1] += eCAP1;
  }

  st.site = &c_emlrtRSI;
  b_sqrt(&st, iKs);
  iNaf[0] = r0->size[0];
  iNaf[1] = r0->size[1];
  b_iKs[0] = iKs->size[0];
  b_iKs[1] = iKs->size[1];
  emxInit_real_T(sp, &r1, 2, &e_emlrtRTEI, true);
  if ((iNaf[0] != b_iKs[0]) || (iNaf[1] != b_iKs[1])) {
    emlrtSizeEqCheckNDR2012b(&iNaf[0], &b_iKs[0], &w_emlrtECI, sp);
  }

  eCAP1 = model->resMed / (12.566370614359172 * model->elecR);
  i1 = r1->size[0] * r1->size[1];
  r1->size[0] = 1;
  r1->size[1] = r0->size[1];
  emxEnsureCapacity_real_T(sp, r1, i1, &e_emlrtRTEI);
  loop_ub = r0->size[0] * r0->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    r1->data[i1] = r0->data[i1] + iKs->data[i1];
  }

  emxInit_real_T(sp, &fieldResist, 2, &h_emlrtRTEI, true);
  rdivide_helper(sp, 2.0 * model->elecR, r1, fieldResist);
  st.site = &d_emlrtRSI;
  b_asin(&st, fieldResist);
  i1 = fieldResist->size[0] * fieldResist->size[1];
  i2 = fieldResist->size[0] * fieldResist->size[1];
  fieldResist->size[0] = 1;
  emxEnsureCapacity_real_T(sp, fieldResist, i2, &h_emlrtRTEI);
  loop_ub = i1 - 1;
  for (i1 = 0; i1 <= loop_ub; i1++) {
    fieldResist->data[i1] *= eCAP1;
  }

  emlrtMEXProfilingStatement(37, isMexOutdated);
  emlrtMEXProfilingStatement(38, isMexOutdated);
  st.site = &e_emlrtRSI;
  b_st.site = &ib_emlrtRSI;
  eCAP1 = options->eCAPzLoc * options->eCAPzLoc;
  i1 = b_segments->size[0] * b_segments->size[1];
  b_segments->size[0] = 1;
  b_segments->size[1] = segments->xEcap->size[1];
  emxEnsureCapacity_real_T(sp, b_segments, i1, &i_emlrtRTEI);
  loop_ub = segments->xEcap->size[0] * segments->xEcap->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    b_segments->data[i1] = segments->xEcap->data[i1] + options->eCAPxLoc;
  }

  st.site = &f_emlrtRSI;
  power(&st, b_segments, r0);
  i1 = r0->size[0] * r0->size[1];
  i2 = r0->size[0] * r0->size[1];
  r0->size[0] = 1;
  emxEnsureCapacity_real_T(sp, r0, i2, &j_emlrtRTEI);
  loop_ub = i1 - 1;
  for (i1 = 0; i1 <= loop_ub; i1++) {
    r0->data[i1] += eCAP1;
  }

  emxInit_real_T(sp, &ecapRes, 2, &uc_emlrtRTEI, true);
  emxInit_real_T(sp, &b_ecapRes, 1, &uc_emlrtRTEI, true);
  st.site = &e_emlrtRSI;
  b_sqrt(&st, r0);
  rdivide_helper(sp, model->resMed / 12.566370614359172, r0, ecapRes);
  emlrtMEXProfilingStatement(39, isMexOutdated);
  i1 = b_ecapRes->size[0];
  b_ecapRes->size[0] = ecapRes->size[1];
  emxEnsureCapacity_real_T(sp, b_ecapRes, i1, &k_emlrtRTEI);
  loop_ub = ecapRes->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    b_ecapRes->data[i1] = ecapRes->data[i1];
  }

  emlrtMEXProfilingStatement(40, isMexOutdated);
  st.site = &g_emlrtRSI;
  toLogicalCheck(&st, options->recECAP);
  emxInit_int32_T(sp, &r2, 1, &yc_emlrtRTEI, true);
  emxInit_real_T(sp, &a, 2, &ad_emlrtRTEI, true);
  emxInit_real_T(sp, &numSpikes, 1, &mc_emlrtRTEI, true);
  if (options->recECAP != 0.0) {
    emlrtMEXProfilingStatement(41, isMexOutdated);
    i1 = r0->size[0] * r0->size[1];
    r0->size[0] = 1;
    r0->size[1] = segments->rm->size[1];
    emxEnsureCapacity_real_T(sp, r0, i1, &l_emlrtRTEI);
    loop_ub = segments->rm->size[1];
    for (i1 = 0; i1 < loop_ub; i1++) {
      r0->data[i1] = 0.0;
    }

    st.site = &h_emlrtRSI;
    b_rdivide_helper(&st, r0, segments->rm, a);
    st.site = &h_emlrtRSI;
    b_st.site = &ub_emlrtRSI;
    if (a->size[1] != b_ecapRes->size[0]) {
      if ((a->size[1] == 1) || (b_ecapRes->size[0] == 1)) {
        emlrtErrorWithMessageIdR2018a(&b_st, &ie_emlrtRTEI,
          "Coder:toolbox:mtimes_noDynamicScalarExpansion",
          "Coder:toolbox:mtimes_noDynamicScalarExpansion", 0);
      } else {
        emlrtErrorWithMessageIdR2018a(&b_st, &je_emlrtRTEI, "MATLAB:innerdim",
          "MATLAB:innerdim", 0);
      }
    }

    if ((a->size[1] == 1) || (b_ecapRes->size[0] == 1)) {
      eCAP1 = 0.0;
      loop_ub = a->size[1];
      for (i1 = 0; i1 < loop_ub; i1++) {
        eCAP1 += a->data[i1] * b_ecapRes->data[i1];
      }
    } else {
      b_st.site = &tb_emlrtRSI;
      c_st.site = &vb_emlrtRSI;
      d_st.site = &wb_emlrtRSI;
      if (a->size[1] < 1) {
        eCAP1 = 0.0;
      } else {
        n_t = (ptrdiff_t)a->size[1];
        incx_t = (ptrdiff_t)1;
        incy_t = (ptrdiff_t)1;
        eCAP1 = ddot(&n_t, &a->data[0], &incx_t, &b_ecapRes->data[0], &incy_t);
      }
    }

    emlrtMEXProfilingStatement(42, isMexOutdated);
    i1 = r2->size[0];
    r2->size[0] = (int32_T)options->numMonte;
    emxEnsureCapacity_int32_T(sp, r2, i1, &n_emlrtRTEI);
    loop_ub = (int32_T)options->numMonte;
    for (i1 = 0; i1 < loop_ub; i1++) {
      r2->data[i1] = i1;
    }

    emxInit_real32_T(sp, &r3, 1, &yc_emlrtRTEI, true);
    i1 = Istim->size[1];
    if (1 > i1) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i1, &emlrtBCI, sp);
    }

    st.site = &i_emlrtRSI;
    repmat(&st, eCAP1, options->numMonte, numSpikes);
    i1 = r3->size[0];
    r3->size[0] = numSpikes->size[0];
    emxEnsureCapacity_real32_T(sp, r3, i1, &p_emlrtRTEI);
    loop_ub = numSpikes->size[0];
    for (i1 = 0; i1 < loop_ub; i1++) {
      r3->data[i1] = (real32_T)numSpikes->data[i1];
    }

    iv3[0] = r2->size[0];
    emlrtSubAssignSizeCheckR2012b(&iv3[0], 1, &(*(int32_T (*)[1])r3->size)[0], 1,
      &emlrtECI, sp);
    loop_ub = r3->size[0];
    for (i1 = 0; i1 < loop_ub; i1++) {
      varargout_3->data[r2->data[i1]] = r3->data[i1];
    }

    emxFree_real32_T(&r3);
    emlrtMEXProfilingStatement(43, isMexOutdated);
  }

  /*  Initialize Crank-Nicholson variables */
  /*  Set boundary conditions for Crank Nicolson */
  emlrtMEXProfilingStatement(44, isMexOutdated);
  c_rdivide_helper(sp, segments->ra, ecapRes);
  i1 = a->size[0] * a->size[1];
  a->size[0] = 1;
  a->size[1] = 1 + ecapRes->size[1];
  emxEnsureCapacity_real_T(sp, a, i1, &m_emlrtRTEI);
  a->data[0] = 0.0;
  loop_ub = ecapRes->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    a->data[i1 + 1] = ecapRes->data[i1];
  }

  /*  sub diagonal of matrix A */
  emlrtMEXProfilingStatement(45, isMexOutdated);
  emlrtMEXProfilingStatement(46, isMexOutdated);
  emlrtMEXProfilingStatement(0, isMexOutdated);
  emlrtMEXProfilingStatement(47, isMexOutdated);
  emlrtMEXProfilingStatement(48, isMexOutdated);
  if (2 > segments->rm->size[1] - 1) {
    i1 = 0;
    i3 = 0;
  } else {
    i1 = segments->rm->size[1];
    if (2 > i1) {
      emlrtDynamicBoundsCheckR2012b(2, 1, i1, &wc_emlrtBCI, sp);
    }

    i1 = 1;
    i2 = segments->rm->size[1];
    i3 = segments->rm->size[1] - 1;
    if ((i3 < 1) || (i3 > i2)) {
      emlrtDynamicBoundsCheckR2012b(i3, 1, i2, &vc_emlrtBCI, sp);
    }
  }

  if (2 > segments->ra->size[1]) {
    i2 = 0;
    i4 = 0;
  } else {
    i2 = segments->ra->size[1];
    if (2 > i2) {
      emlrtDynamicBoundsCheckR2012b(2, 1, i2, &uc_emlrtBCI, sp);
    }

    i2 = 1;
    i5 = segments->ra->size[1];
    i4 = segments->ra->size[1];
    if ((i4 < 1) || (i4 > i5)) {
      emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &tc_emlrtBCI, sp);
    }
  }

  i5 = b_segments->size[0] * b_segments->size[1];
  b_segments->size[0] = 1;
  loop_ub = i3 - i1;
  b_segments->size[1] = loop_ub;
  emxEnsureCapacity_real_T(sp, b_segments, i5, &o_emlrtRTEI);
  for (i3 = 0; i3 < loop_ub; i3++) {
    b_segments->data[i3] = segments->rm->data[i1 + i3];
  }

  c_rdivide_helper(sp, b_segments, r0);
  i1 = b_segments->size[0] * b_segments->size[1];
  b_segments->size[0] = 1;
  loop_ub = i4 - i2;
  b_segments->size[1] = loop_ub;
  emxEnsureCapacity_real_T(sp, b_segments, i1, &q_emlrtRTEI);
  for (i1 = 0; i1 < loop_ub; i1++) {
    b_segments->data[i1] = segments->ra->data[i2 + i1];
  }

  c_rdivide_helper(sp, b_segments, iKs);
  iNaf[0] = r0->size[0];
  iNaf[1] = r0->size[1];
  b_iKs[0] = iKs->size[0];
  b_iKs[1] = iKs->size[1];
  if ((iNaf[0] != b_iKs[0]) || (iNaf[1] != b_iKs[1])) {
    emlrtSizeEqCheckNDR2012b(&iNaf[0], &b_iKs[0], &v_emlrtECI, sp);
  }

  if (1 > segments->ra->size[1] - 1) {
    loop_ub = 0;
  } else {
    i1 = segments->ra->size[1];
    if (1 > i1) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i1, &sc_emlrtBCI, sp);
    }

    i1 = segments->ra->size[1];
    loop_ub = segments->ra->size[1] - 1;
    if ((loop_ub < 1) || (loop_ub > i1)) {
      emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i1, &rc_emlrtBCI, sp);
    }
  }

  i1 = b_segments->size[0] * b_segments->size[1];
  b_segments->size[0] = 1;
  b_segments->size[1] = loop_ub;
  emxEnsureCapacity_real_T(sp, b_segments, i1, &r_emlrtRTEI);
  for (i1 = 0; i1 < loop_ub; i1++) {
    b_segments->data[i1] = segments->ra->data[i1];
  }

  c_rdivide_helper(sp, b_segments, r1);
  iNaf[0] = r0->size[0];
  iNaf[1] = r0->size[1];
  c_ecapRes[0] = r1->size[0];
  c_ecapRes[1] = r1->size[1];
  if ((iNaf[0] != c_ecapRes[0]) || (iNaf[1] != c_ecapRes[1])) {
    emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &v_emlrtECI, sp);
  }

  if (2 > segments->cm->size[1] - 1) {
    i1 = 0;
    i3 = 0;
  } else {
    i1 = segments->cm->size[1];
    if (2 > i1) {
      emlrtDynamicBoundsCheckR2012b(2, 1, i1, &qc_emlrtBCI, sp);
    }

    i1 = 1;
    i2 = segments->cm->size[1];
    i3 = segments->cm->size[1] - 1;
    if ((i3 < 1) || (i3 > i2)) {
      emlrtDynamicBoundsCheckR2012b(i3, 1, i2, &pc_emlrtBCI, sp);
    }
  }

  emxInit_real_T(sp, &r4, 2, &yc_emlrtRTEI, true);
  i2 = r4->size[0] * r4->size[1];
  r4->size[0] = 1;
  loop_ub = i3 - i1;
  r4->size[1] = loop_ub;
  emxEnsureCapacity_real_T(sp, r4, i2, &s_emlrtRTEI);
  for (i2 = 0; i2 < loop_ub; i2++) {
    r4->data[i2] = 2.0 * segments->cm->data[i1 + i2] / model->dt;
  }

  iNaf[0] = r0->size[0];
  iNaf[1] = r0->size[1];
  c_ecapRes[0] = r4->size[0];
  c_ecapRes[1] = r4->size[1];
  emxInit_real_T(sp, &b, 2, &t_emlrtRTEI, true);
  if ((iNaf[0] != c_ecapRes[0]) || (iNaf[1] != c_ecapRes[1])) {
    emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &v_emlrtECI, sp);
  }

  i1 = segments->rm->size[1];
  if (1 > i1) {
    emlrtDynamicBoundsCheckR2012b(1, 1, i1, &ad_emlrtBCI, sp);
  }

  i1 = segments->ra->size[1];
  if (1 > i1) {
    emlrtDynamicBoundsCheckR2012b(1, 1, i1, &yc_emlrtBCI, sp);
  }

  i1 = segments->cm->size[1];
  if (1 > i1) {
    emlrtDynamicBoundsCheckR2012b(1, 1, i1, &xc_emlrtBCI, sp);
  }

  i1 = b->size[0] * b->size[1];
  b->size[0] = 1;
  b->size[1] = 2 + r0->size[1];
  emxEnsureCapacity_real_T(sp, b, i1, &t_emlrtRTEI);
  b->data[0] = -((1.0 / segments->rm->data[0] + 1.0 / segments->ra->data[0]) +
                 2.0 * segments->cm->data[0] / model->dt);
  loop_ub = r0->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    b->data[i1 + 1] = -(((r0->data[i1] + iKs->data[i1]) + r1->data[i1]) +
                        r4->data[i1]);
  }

  emxInit_real_T(sp, &c, 2, &u_emlrtRTEI, true);
  i1 = segments->rm->size[1];
  i2 = segments->rm->size[1];
  if ((i2 < 1) || (i2 > i1)) {
    emlrtDynamicBoundsCheckR2012b(i2, 1, i1, &bd_emlrtBCI, sp);
  }

  i1 = segments->ra->size[1];
  i3 = segments->ra->size[1];
  if ((i3 < 1) || (i3 > i1)) {
    emlrtDynamicBoundsCheckR2012b(i3, 1, i1, &cd_emlrtBCI, sp);
  }

  i1 = segments->cm->size[1];
  i5 = segments->cm->size[1];
  if ((i5 < 1) || (i5 > i1)) {
    emlrtDynamicBoundsCheckR2012b(i5, 1, i1, &dd_emlrtBCI, sp);
  }

  b->data[1 + r0->size[1]] = -((1.0 / segments->rm->data[i2 - 1] + 1.0 /
    segments->ra->data[i3 - 1]) + 2.0 * segments->cm->data[i5 - 1] / model->dt);

  /*  diagonal of A */
  emlrtMEXProfilingStatement(49, isMexOutdated);
  i1 = c->size[0] * c->size[1];
  c->size[0] = 1;
  c->size[1] = ecapRes->size[1] + 1;
  emxEnsureCapacity_real_T(sp, c, i1, &u_emlrtRTEI);
  loop_ub = ecapRes->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    c->data[i1] = ecapRes->data[i1];
  }

  c->data[ecapRes->size[1]] = 0.0;

  /*  supradiagonal of A */
  /*  A = spdiags([a' b' c'],-1:1,numSegments,numSegments); % View it! */
  /*  Calculate coefficients for middle RHS term */
  emlrtMEXProfilingStatement(50, isMexOutdated);
  emlrtMEXProfilingStatement(51, isMexOutdated);
  emlrtMEXProfilingStatement(52, isMexOutdated);
  emlrtMEXProfilingStatement(53, isMexOutdated);
  if (2 > segments->rm->size[1] - 1) {
    i1 = 0;
    i3 = 0;
  } else {
    i1 = segments->rm->size[1];
    if (2 > i1) {
      emlrtDynamicBoundsCheckR2012b(2, 1, i1, &lc_emlrtBCI, sp);
    }

    i1 = 1;
    i2 = segments->rm->size[1];
    i3 = segments->rm->size[1] - 1;
    if ((i3 < 1) || (i3 > i2)) {
      emlrtDynamicBoundsCheckR2012b(i3, 1, i2, &kc_emlrtBCI, sp);
    }
  }

  if (2 > segments->ra->size[1]) {
    i2 = 0;
    i4 = 0;
  } else {
    i2 = segments->ra->size[1];
    if (2 > i2) {
      emlrtDynamicBoundsCheckR2012b(2, 1, i2, &jc_emlrtBCI, sp);
    }

    i2 = 1;
    i5 = segments->ra->size[1];
    i4 = segments->ra->size[1];
    if ((i4 < 1) || (i4 > i5)) {
      emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &ic_emlrtBCI, sp);
    }
  }

  i5 = b_segments->size[0] * b_segments->size[1];
  b_segments->size[0] = 1;
  loop_ub = i3 - i1;
  b_segments->size[1] = loop_ub;
  emxEnsureCapacity_real_T(sp, b_segments, i5, &v_emlrtRTEI);
  for (i3 = 0; i3 < loop_ub; i3++) {
    b_segments->data[i3] = segments->rm->data[i1 + i3];
  }

  c_rdivide_helper(sp, b_segments, r0);
  i1 = b_segments->size[0] * b_segments->size[1];
  b_segments->size[0] = 1;
  loop_ub = i4 - i2;
  b_segments->size[1] = loop_ub;
  emxEnsureCapacity_real_T(sp, b_segments, i1, &w_emlrtRTEI);
  for (i1 = 0; i1 < loop_ub; i1++) {
    b_segments->data[i1] = segments->ra->data[i2 + i1];
  }

  c_rdivide_helper(sp, b_segments, iKs);
  iNaf[0] = r0->size[0];
  iNaf[1] = r0->size[1];
  b_iKs[0] = iKs->size[0];
  b_iKs[1] = iKs->size[1];
  if ((iNaf[0] != b_iKs[0]) || (iNaf[1] != b_iKs[1])) {
    emlrtSizeEqCheckNDR2012b(&iNaf[0], &b_iKs[0], &u_emlrtECI, sp);
  }

  if (1 > segments->ra->size[1] - 1) {
    loop_ub = 0;
  } else {
    i1 = segments->ra->size[1];
    if (1 > i1) {
      emlrtDynamicBoundsCheckR2012b(1, 1, i1, &hc_emlrtBCI, sp);
    }

    i1 = segments->ra->size[1];
    loop_ub = segments->ra->size[1] - 1;
    if ((loop_ub < 1) || (loop_ub > i1)) {
      emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i1, &gc_emlrtBCI, sp);
    }
  }

  i1 = b_segments->size[0] * b_segments->size[1];
  b_segments->size[0] = 1;
  b_segments->size[1] = loop_ub;
  emxEnsureCapacity_real_T(sp, b_segments, i1, &x_emlrtRTEI);
  for (i1 = 0; i1 < loop_ub; i1++) {
    b_segments->data[i1] = segments->ra->data[i1];
  }

  c_rdivide_helper(sp, b_segments, r1);
  iNaf[0] = r0->size[0];
  iNaf[1] = r0->size[1];
  c_ecapRes[0] = r1->size[0];
  c_ecapRes[1] = r1->size[1];
  if ((iNaf[0] != c_ecapRes[0]) || (iNaf[1] != c_ecapRes[1])) {
    emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &u_emlrtECI, sp);
  }

  if (2 > segments->cm->size[1] - 1) {
    i1 = 0;
    i3 = 0;
  } else {
    i1 = segments->cm->size[1];
    if (2 > i1) {
      emlrtDynamicBoundsCheckR2012b(2, 1, i1, &fc_emlrtBCI, sp);
    }

    i1 = 1;
    i2 = segments->cm->size[1];
    i3 = segments->cm->size[1] - 1;
    if ((i3 < 1) || (i3 > i2)) {
      emlrtDynamicBoundsCheckR2012b(i3, 1, i2, &ec_emlrtBCI, sp);
    }
  }

  i2 = r4->size[0] * r4->size[1];
  r4->size[0] = 1;
  loop_ub = i3 - i1;
  r4->size[1] = loop_ub;
  emxEnsureCapacity_real_T(sp, r4, i2, &y_emlrtRTEI);
  for (i2 = 0; i2 < loop_ub; i2++) {
    r4->data[i2] = 2.0 * segments->cm->data[i1 + i2] / model->dt;
  }

  iNaf[0] = r0->size[0];
  iNaf[1] = r0->size[1];
  c_ecapRes[0] = r4->size[0];
  c_ecapRes[1] = r4->size[1];
  emxInit_real_T(sp, &rhsM, 2, &ab_emlrtRTEI, true);
  if ((iNaf[0] != c_ecapRes[0]) || (iNaf[1] != c_ecapRes[1])) {
    emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &u_emlrtECI, sp);
  }

  i1 = segments->rm->size[1];
  if (1 > i1) {
    emlrtDynamicBoundsCheckR2012b(1, 1, i1, &oc_emlrtBCI, sp);
  }

  i1 = segments->ra->size[1];
  if (1 > i1) {
    emlrtDynamicBoundsCheckR2012b(1, 1, i1, &nc_emlrtBCI, sp);
  }

  i1 = segments->cm->size[1];
  if (1 > i1) {
    emlrtDynamicBoundsCheckR2012b(1, 1, i1, &mc_emlrtBCI, sp);
  }

  i1 = rhsM->size[0] * rhsM->size[1];
  rhsM->size[0] = 1;
  rhsM->size[1] = 2 + r0->size[1];
  emxEnsureCapacity_real_T(sp, rhsM, i1, &ab_emlrtRTEI);
  rhsM->data[0] = (1.0 / segments->rm->data[0] + 1.0 / segments->ra->data[0]) -
    2.0 * segments->cm->data[0] / model->dt;
  loop_ub = r0->size[1];
  for (i1 = 0; i1 < loop_ub; i1++) {
    rhsM->data[i1 + 1] = ((r0->data[i1] + iKs->data[i1]) + r1->data[i1]) -
      r4->data[i1];
  }

  emxInit_real_T(sp, &Naf, 2, &bb_emlrtRTEI, true);
  i1 = segments->rm->size[1];
  i2 = segments->rm->size[1];
  if ((i2 < 1) || (i2 > i1)) {
    emlrtDynamicBoundsCheckR2012b(i2, 1, i1, &ed_emlrtBCI, sp);
  }

  i1 = segments->ra->size[1];
  i3 = segments->ra->size[1];
  if ((i3 < 1) || (i3 > i1)) {
    emlrtDynamicBoundsCheckR2012b(i3, 1, i1, &fd_emlrtBCI, sp);
  }

  i1 = segments->cm->size[1];
  i5 = segments->cm->size[1];
  if ((i5 < 1) || (i5 > i1)) {
    emlrtDynamicBoundsCheckR2012b(i5, 1, i1, &gd_emlrtBCI, sp);
  }

  rhsM->data[1 + r0->size[1]] = (1.0 / segments->rm->data[i2 - 1] + 1.0 /
    segments->ra->data[i3 - 1]) - 2.0 * segments->cm->data[i5 - 1] / model->dt;

  /*  ecap = nan(1,k_max); */
  emlrtMEXProfilingStatement(54, isMexOutdated);
  i1 = Naf->size[0] * Naf->size[1];
  Naf->size[0] = segments->rm->size[1];
  Naf->size[1] = 8;
  emxEnsureCapacity_real_T(sp, Naf, i1, &bb_emlrtRTEI);
  loop_ub = segments->rm->size[1] << 3;
  for (i1 = 0; i1 < loop_ub; i1++) {
    Naf->data[i1] = 0.0;
  }

  emxInit_real_T(sp, &Kf, 2, &cb_emlrtRTEI, true);
  emlrtMEXProfilingStatement(55, isMexOutdated);
  i1 = Kf->size[0] * Kf->size[1];
  Kf->size[0] = segments->rm->size[1];
  Kf->size[1] = 5;
  emxEnsureCapacity_real_T(sp, Kf, i1, &cb_emlrtRTEI);
  loop_ub = segments->rm->size[1] * 5;
  for (i1 = 0; i1 < loop_ub; i1++) {
    Kf->data[i1] = 0.0;
  }

  emxInit_real_T(sp, &Ks, 2, &db_emlrtRTEI, true);
  emlrtMEXProfilingStatement(56, isMexOutdated);
  i1 = Ks->size[0] * Ks->size[1];
  Ks->size[0] = segments->rm->size[1];
  Ks->size[1] = 2;
  emxEnsureCapacity_real_T(sp, Ks, i1, &db_emlrtRTEI);
  loop_ub = segments->rm->size[1] << 1;
  for (i1 = 0; i1 < loop_ub; i1++) {
    Ks->data[i1] = 0.0;
  }

  emlrtMEXProfilingStatement(57, isMexOutdated);
  i1 = (int32_T)options->numMonte;
  emlrtForLoopVectorCheckR2012b(1.0, 1.0, options->numMonte, mxDOUBLE_CLASS,
    (int32_T)options->numMonte, &ke_emlrtRTEI, sp);
  emxInit_real_T(sp, &Vm, 2, &tc_emlrtRTEI, true);
  emxInit_real_T(sp, &b_numSpikes, 2, &eb_emlrtRTEI, true);
  emxInit_real_T(sp, &rhs, 2, &gb_emlrtRTEI, true);
  emxInit_real_T(sp, &dist, 2, &hb_emlrtRTEI, true);
  emxInit_real_T(sp, &ratesNaf, 2, &vc_emlrtRTEI, true);
  emxInit_real_T(sp, &ratesKf, 2, &wc_emlrtRTEI, true);
  emxInit_real_T(sp, &ratesKs, 2, &xc_emlrtRTEI, true);
  emxInit_real_T(sp, &Vapp, 2, &jb_emlrtRTEI, true);
  emxInit_real_T(sp, &Vprev, 2, &kb_emlrtRTEI, true);
  emxInit_real_T(sp, &b_iNaf, 2, &pb_emlrtRTEI, true);
  emxInit_boolean_T(sp, &r5, 2, &yc_emlrtRTEI, true);
  emxInit_boolean_T(sp, &x, 2, &fc_emlrtRTEI, true);
  emxInit_int32_T(sp, &ii, 2, &bd_emlrtRTEI, true);
  emxInit_boolean_T(sp, &c_numSpikes, 2, &oc_emlrtRTEI, true);
  for (monte = 0; monte < i1; monte++) {
    /*  Variables initialization */
    emlrtMEXProfilingStatement(58, isMexOutdated);
    i2 = b_numSpikes->size[0] * b_numSpikes->size[1];
    b_numSpikes->size[0] = 1;
    b_numSpikes->size[1] = varargin_2;
    emxEnsureCapacity_real_T(sp, b_numSpikes, i2, &eb_emlrtRTEI);
    for (i2 = 0; i2 < varargin_2; i2++) {
      b_numSpikes->data[i2] = 0.0;
    }

    emlrtMEXProfilingStatement(59, isMexOutdated);
    i2 = Vm->size[0] * Vm->size[1];
    Vm->size[0] = 1;
    Vm->size[1] = varargin_2;
    emxEnsureCapacity_real_T(sp, Vm, i2, &fb_emlrtRTEI);
    for (i2 = 0; i2 < varargin_2; i2++) {
      Vm->data[i2] = 0.0;
    }

    emlrtMEXProfilingStatement(60, isMexOutdated);
    i2 = rhs->size[0] * rhs->size[1];
    rhs->size[0] = 1;
    rhs->size[1] = varargin_2;
    emxEnsureCapacity_real_T(sp, rhs, i2, &gb_emlrtRTEI);
    for (i2 = 0; i2 < varargin_2; i2++) {
      rhs->data[i2] = 0.0;
    }

    emlrtMEXProfilingStatement(61, isMexOutdated);
    i2 = dist->size[0] * dist->size[1];
    dist->size[0] = 1;
    dist->size[1] = varargin_2;
    emxEnsureCapacity_real_T(sp, dist, i2, &hb_emlrtRTEI);
    for (i2 = 0; i2 < varargin_2; i2++) {
      dist->data[i2] = 0.0;
    }

    /*  Set rates to resting values */
    emlrtMEXProfilingStatement(62, isMexOutdated);
    i2 = b_segments->size[0] * b_segments->size[1];
    b_segments->size[0] = 1;
    b_segments->size[1] = varargin_2;
    emxEnsureCapacity_real_T(sp, b_segments, i2, &ib_emlrtRTEI);
    for (i2 = 0; i2 < varargin_2; i2++) {
      b_segments->data[i2] = Vrest;
    }

    st.site = &j_emlrtRSI;
    trans_rates_Naf(&st, b_segments, model->Naf.Kins, ratesNaf);
    emlrtMEXProfilingStatement(63, isMexOutdated);
    i2 = b_segments->size[0] * b_segments->size[1];
    b_segments->size[0] = 1;
    b_segments->size[1] = varargin_2;
    emxEnsureCapacity_real_T(sp, b_segments, i2, &ib_emlrtRTEI);
    for (i2 = 0; i2 < varargin_2; i2++) {
      b_segments->data[i2] = Vrest;
    }

    st.site = &k_emlrtRSI;
    trans_rates_K(&st, b_segments, model->Kf.Kins, ratesKf);
    emlrtMEXProfilingStatement(64, isMexOutdated);
    i2 = b_segments->size[0] * b_segments->size[1];
    b_segments->size[0] = 1;
    b_segments->size[1] = varargin_2;
    emxEnsureCapacity_real_T(sp, b_segments, i2, &ib_emlrtRTEI);
    for (i2 = 0; i2 < varargin_2; i2++) {
      b_segments->data[i2] = Vrest;
    }

    st.site = &l_emlrtRSI;
    trans_rates_K(&st, b_segments, model->Ks.Kins, ratesKs);
    emlrtMEXProfilingStatement(65, isMexOutdated);
    i2 = segments->nodeIDX->size[1];
    for (IDX = 0; IDX < i2; IDX++) {
      emlrtMEXProfilingStatement(66, isMexOutdated);
      i3 = segments->nodeIDX->size[1];
      i5 = 1 + IDX;
      if ((i5 < 1) || (i5 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i5, 1, i3, &dc_emlrtBCI, sp);
      }

      /*  Initialize channel states using jump Monte Carlo. */
      emlrtMEXProfilingStatement(67, isMexOutdated);
      i3 = ratesNaf->size[1];
      eCAP1 = segments->nodeIDX->data[IDX];
      if (eCAP1 != (int32_T)muDoubleScalarFloor(eCAP1)) {
        emlrtIntegerCheckR2012b(eCAP1, &p_emlrtDCI, sp);
      }

      i5 = (int32_T)eCAP1;
      if ((i5 < 1) || (i5 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i5, 1, i3, &hd_emlrtBCI, sp);
      }

      i3 = segments->numNaf->size[1];
      eCAP1 = segments->nodeIDX->data[IDX];
      if (eCAP1 != (int32_T)muDoubleScalarFloor(eCAP1)) {
        emlrtIntegerCheckR2012b(eCAP1, &q_emlrtDCI, sp);
      }

      i4 = (int32_T)eCAP1;
      if ((i4 < 1) || (i4 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i4, 1, i3, &id_emlrtBCI, sp);
      }

      st.site = &m_emlrtRSI;
      initialize_Naf_channels(&st, *(real_T (*)[4])&ratesNaf->data[(i5 - 1) << 2],
        segments->numNaf->data[i4 - 1], dt, dv0);
      i3 = Naf->size[0];
      eCAP1 = segments->nodeIDX->data[IDX];
      if (eCAP1 != (int32_T)muDoubleScalarFloor(eCAP1)) {
        emlrtIntegerCheckR2012b(eCAP1, &d_emlrtDCI, sp);
      }

      nx = (int32_T)eCAP1;
      if ((nx < 1) || (nx > i3)) {
        emlrtDynamicBoundsCheckR2012b(nx, 1, i3, &ac_emlrtBCI, sp);
      }

      for (i3 = 0; i3 < 8; i3++) {
        Naf->data[(nx + Naf->size[0] * i3) - 1] = dv0[i3];
      }

      emlrtMEXProfilingStatement(68, isMexOutdated);
      i3 = ratesKf->size[1];
      i5 = (int32_T)segments->nodeIDX->data[IDX];
      if ((i5 < 1) || (i5 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i5, 1, i3, &jd_emlrtBCI, sp);
      }

      i3 = segments->numKf->size[1];
      i4 = (int32_T)segments->nodeIDX->data[IDX];
      if ((i4 < 1) || (i4 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i4, 1, i3, &kd_emlrtBCI, sp);
      }

      st.site = &n_emlrtRSI;
      initialize_Kf_channels(&st, *(real_T (*)[2])&ratesKf->data[(i5 - 1) << 1],
        segments->numKf->data[i4 - 1], dt, dv1);
      i3 = Kf->size[0];
      nx = (int32_T)segments->nodeIDX->data[IDX];
      if ((nx < 1) || (nx > i3)) {
        emlrtDynamicBoundsCheckR2012b(nx, 1, i3, &wb_emlrtBCI, sp);
      }

      for (i3 = 0; i3 < 5; i3++) {
        Kf->data[(nx + Kf->size[0] * i3) - 1] = dv1[i3];
      }

      emlrtMEXProfilingStatement(69, isMexOutdated);
      i3 = ratesKs->size[1];
      i5 = (int32_T)segments->nodeIDX->data[IDX];
      if ((i5 < 1) || (i5 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i5, 1, i3, &ld_emlrtBCI, sp);
      }

      i3 = segments->numKs->size[1];
      i4 = (int32_T)segments->nodeIDX->data[IDX];
      if ((i4 < 1) || (i4 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i4, 1, i3, &md_emlrtBCI, sp);
      }

      st.site = &o_emlrtRSI;
      initialize_Ks_channels(&st, *(real_T (*)[2])&ratesKs->data[(i5 - 1) << 1],
        segments->numKs->data[i4 - 1], dt, dv2);
      i3 = Ks->size[0];
      nx = (int32_T)segments->nodeIDX->data[IDX];
      if ((nx < 1) || (nx > i3)) {
        emlrtDynamicBoundsCheckR2012b(nx, 1, i3, &tb_emlrtBCI, sp);
      }

      Ks->data[nx - 1] = dv2[0];
      Ks->data[(nx + Ks->size[0]) - 1] = dv2[1];
      emlrtMEXProfilingStatement(70, isMexOutdated);
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    emlrtMEXProfilingStatement(71, isMexOutdated);
    i2 = Istim->size[1];
    for (it = 0; it <= i2 - 2; it++) {
      emlrtMEXProfilingStatement(72, isMexOutdated);
      i3 = Istim->size[1];
      i5 = 2 + it;
      if ((i5 < 1) || (i5 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i5, 1, i3, &cc_emlrtBCI, sp);
      }

      i3 = Vapp->size[0] * Vapp->size[1];
      Vapp->size[0] = 1;
      Vapp->size[1] = fieldResist->size[1];
      emxEnsureCapacity_real_T(sp, Vapp, i3, &jb_emlrtRTEI);
      eCAP1 = Istim->data[it + 1];
      loop_ub = fieldResist->size[0] * fieldResist->size[1];
      for (i3 = 0; i3 < loop_ub; i3++) {
        Vapp->data[i3] = eCAP1 * fieldResist->data[i3];
      }

      /*  Calculate applied voltages */
      emlrtMEXProfilingStatement(73, isMexOutdated);
      i3 = Vprev->size[0] * Vprev->size[1];
      Vprev->size[0] = 1;
      Vprev->size[1] = Vm->size[1];
      emxEnsureCapacity_real_T(sp, Vprev, i3, &kb_emlrtRTEI);
      loop_ub = Vm->size[0] * Vm->size[1];
      for (i3 = 0; i3 < loop_ub; i3++) {
        Vprev->data[i3] = Vm->data[i3];
      }

      /*  update transition rates */
      emlrtMEXProfilingStatement(74, isMexOutdated);
      i3 = b_segments->size[0] * b_segments->size[1];
      b_segments->size[0] = 1;
      b_segments->size[1] = Vm->size[1];
      emxEnsureCapacity_real_T(sp, b_segments, i3, &lb_emlrtRTEI);
      loop_ub = Vm->size[0] * Vm->size[1];
      for (i3 = 0; i3 < loop_ub; i3++) {
        b_segments->data[i3] = Vm->data[i3] + Vrest;
      }

      st.site = &p_emlrtRSI;
      trans_rates_Naf(&st, b_segments, model->Naf.Kins, ratesNaf);
      emlrtMEXProfilingStatement(75, isMexOutdated);
      i3 = b_segments->size[0] * b_segments->size[1];
      b_segments->size[0] = 1;
      b_segments->size[1] = Vm->size[1];
      emxEnsureCapacity_real_T(sp, b_segments, i3, &mb_emlrtRTEI);
      loop_ub = Vm->size[0] * Vm->size[1];
      for (i3 = 0; i3 < loop_ub; i3++) {
        b_segments->data[i3] = Vm->data[i3] + Vrest;
      }

      st.site = &q_emlrtRSI;
      trans_rates_K(&st, b_segments, model->Kf.Kins, ratesKf);
      emlrtMEXProfilingStatement(76, isMexOutdated);
      i3 = b_segments->size[0] * b_segments->size[1];
      b_segments->size[0] = 1;
      b_segments->size[1] = Vm->size[1];
      emxEnsureCapacity_real_T(sp, b_segments, i3, &nb_emlrtRTEI);
      loop_ub = Vm->size[0] * Vm->size[1];
      for (i3 = 0; i3 < loop_ub; i3++) {
        b_segments->data[i3] = Vm->data[i3] + Vrest;
      }

      st.site = &r_emlrtRSI;
      trans_rates_K(&st, b_segments, model->Ks.Kins, ratesKs);
      emlrtMEXProfilingStatement(77, isMexOutdated);
      i3 = segments->nodeIDX->size[1];
      for (IDX = 0; IDX < i3; IDX++) {
        emlrtMEXProfilingStatement(78, isMexOutdated);
        i5 = segments->nodeIDX->size[1];
        i4 = 1 + IDX;
        if ((i4 < 1) || (i4 > i5)) {
          emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &bc_emlrtBCI, sp);
        }

        /*  Update channel states using jump Markov Process */
        emlrtMEXProfilingStatement(79, isMexOutdated);
        i5 = Naf->size[0];
        eCAP1 = segments->nodeIDX->data[IDX];
        if (eCAP1 != (int32_T)muDoubleScalarFloor(eCAP1)) {
          emlrtIntegerCheckR2012b(eCAP1, &b_emlrtDCI, sp);
        }

        i4 = (int32_T)eCAP1;
        if ((i4 < 1) || (i4 > i5)) {
          emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &xb_emlrtBCI, sp);
        }

        i5 = i4 - 1;
        i4 = Naf->size[0];
        eCAP1 = segments->nodeIDX->data[IDX];
        if (eCAP1 != (int32_T)muDoubleScalarFloor(eCAP1)) {
          emlrtIntegerCheckR2012b(eCAP1, &c_emlrtDCI, sp);
        }

        nx = (int32_T)eCAP1;
        if ((nx < 1) || (nx > i4)) {
          emlrtDynamicBoundsCheckR2012b(nx, 1, i4, &yb_emlrtBCI, sp);
        }

        for (i4 = 0; i4 < 8; i4++) {
          dv0[i4] = Naf->data[(nx + Naf->size[0] * i4) - 1];
        }

        i4 = ratesNaf->size[1];
        eCAP1 = segments->nodeIDX->data[IDX];
        if (eCAP1 != (int32_T)muDoubleScalarFloor(eCAP1)) {
          emlrtIntegerCheckR2012b(eCAP1, &r_emlrtDCI, sp);
        }

        i6 = (int32_T)eCAP1;
        if ((i6 < 1) || (i6 > i4)) {
          emlrtDynamicBoundsCheckR2012b(i6, 1, i4, &nd_emlrtBCI, sp);
        }

        st.site = &s_emlrtRSI;
        update_Naf_channels(&st, *(real_T (*)[4])&ratesNaf->data[(i6 - 1) << 2],
                            dv0, dt);
        for (i4 = 0; i4 < 8; i4++) {
          Naf->data[i5 + Naf->size[0] * i4] = dv0[i4];
        }

        emlrtMEXProfilingStatement(80, isMexOutdated);
        i5 = Kf->size[0];
        i4 = (int32_T)segments->nodeIDX->data[IDX];
        if ((i4 < 1) || (i4 > i5)) {
          emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &ub_emlrtBCI, sp);
        }

        i5 = i4 - 1;
        i4 = Kf->size[0];
        nx = (int32_T)segments->nodeIDX->data[IDX];
        if ((nx < 1) || (nx > i4)) {
          emlrtDynamicBoundsCheckR2012b(nx, 1, i4, &vb_emlrtBCI, sp);
        }

        for (i4 = 0; i4 < 5; i4++) {
          dv1[i4] = Kf->data[(nx + Kf->size[0] * i4) - 1];
        }

        i4 = ratesKf->size[1];
        i6 = (int32_T)segments->nodeIDX->data[IDX];
        if ((i6 < 1) || (i6 > i4)) {
          emlrtDynamicBoundsCheckR2012b(i6, 1, i4, &od_emlrtBCI, sp);
        }

        st.site = &t_emlrtRSI;
        update_Kf_channels(&st, *(real_T (*)[2])&ratesKf->data[(i6 - 1) << 1],
                           dv1, dt);
        for (i4 = 0; i4 < 5; i4++) {
          Kf->data[i5 + Kf->size[0] * i4] = dv1[i4];
        }

        emlrtMEXProfilingStatement(81, isMexOutdated);
        i5 = Ks->size[0];
        i4 = (int32_T)segments->nodeIDX->data[IDX];
        if ((i4 < 1) || (i4 > i5)) {
          emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &rb_emlrtBCI, sp);
        }

        i5 = i4 - 1;
        i4 = Ks->size[0];
        nx = (int32_T)segments->nodeIDX->data[IDX];
        if ((nx < 1) || (nx > i4)) {
          emlrtDynamicBoundsCheckR2012b(nx, 1, i4, &sb_emlrtBCI, sp);
        }

        dv2[0] = Ks->data[nx - 1];
        dv2[1] = Ks->data[(nx + Ks->size[0]) - 1];
        i4 = ratesKs->size[1];
        i6 = (int32_T)segments->nodeIDX->data[IDX];
        if ((i6 < 1) || (i6 > i4)) {
          emlrtDynamicBoundsCheckR2012b(i6, 1, i4, &pd_emlrtBCI, sp);
        }

        st.site = &u_emlrtRSI;
        update_Ks_channels(&st, *(real_T (*)[2])&ratesKs->data[(i6 - 1) << 1],
                           dv2, dt);
        Ks->data[i5] = dv2[0];
        Ks->data[i5 + Ks->size[0]] = dv2[1];
        emlrtMEXProfilingStatement(82, isMexOutdated);
        if (*emlrtBreakCheckR2012bFlagVar != 0) {
          emlrtBreakCheckR2012b(sp);
        }
      }

      emlrtMEXProfilingStatement(83, isMexOutdated);
      emlrtMEXProfilingStatement(84, isMexOutdated);
      emlrtMEXProfilingStatement(85, isMexOutdated);
      emlrtMEXProfilingStatement(86, isMexOutdated);
      loop_ub = Naf->size[0];
      i3 = b_iNaf->size[0] * b_iNaf->size[1];
      b_iNaf->size[0] = 1;
      b_iNaf->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, b_iNaf, i3, &ob_emlrtRTEI);
      for (i3 = 0; i3 < loop_ub; i3++) {
        b_iNaf->data[i3] = model->Naf.gNa * Naf->data[i3 + Naf->size[0] * 7];
      }

      iNaf[0] = b_iNaf->size[0];
      iNaf[1] = b_iNaf->size[1];
      b_Vm[0] = Vm->size[0];
      b_Vm[1] = Vm->size[1];
      if ((iNaf[0] != b_Vm[0]) || (iNaf[1] != b_Vm[1])) {
        emlrtSizeEqCheckNDR2012b(&iNaf[0], &b_Vm[0], &t_emlrtECI, sp);
      }

      i3 = b_iNaf->size[0] * b_iNaf->size[1];
      i5 = b_iNaf->size[0] * b_iNaf->size[1];
      b_iNaf->size[0] = 1;
      emxEnsureCapacity_real_T(sp, b_iNaf, i5, &pb_emlrtRTEI);
      loop_ub = i3 - 1;
      for (i3 = 0; i3 <= loop_ub; i3++) {
        b_iNaf->data[i3] *= (Vm->data[i3] + Vrest) - model->Naf.E_Na;
      }

      emlrtMEXProfilingStatement(87, isMexOutdated);
      loop_ub = Kf->size[0];
      i3 = ecapRes->size[0] * ecapRes->size[1];
      ecapRes->size[0] = 1;
      ecapRes->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, ecapRes, i3, &qb_emlrtRTEI);
      for (i3 = 0; i3 < loop_ub; i3++) {
        ecapRes->data[i3] = model->Kf.gK * Kf->data[i3 + (Kf->size[0] << 2)];
      }

      c_ecapRes[0] = ecapRes->size[0];
      c_ecapRes[1] = ecapRes->size[1];
      b_Vm[0] = Vm->size[0];
      b_Vm[1] = Vm->size[1];
      if ((c_ecapRes[0] != b_Vm[0]) || (c_ecapRes[1] != b_Vm[1])) {
        emlrtSizeEqCheckNDR2012b(&c_ecapRes[0], &b_Vm[0], &s_emlrtECI, sp);
      }

      i3 = ecapRes->size[0] * ecapRes->size[1];
      i5 = ecapRes->size[0] * ecapRes->size[1];
      ecapRes->size[0] = 1;
      emxEnsureCapacity_real_T(sp, ecapRes, i5, &rb_emlrtRTEI);
      loop_ub = i3 - 1;
      for (i3 = 0; i3 <= loop_ub; i3++) {
        ecapRes->data[i3] *= (Vm->data[i3] + Vrest) - E_K;
      }

      emlrtMEXProfilingStatement(88, isMexOutdated);
      loop_ub = Ks->size[0];
      i3 = iKs->size[0] * iKs->size[1];
      iKs->size[0] = 1;
      iKs->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, iKs, i3, &sb_emlrtRTEI);
      for (i3 = 0; i3 < loop_ub; i3++) {
        iKs->data[i3] = model->Ks.gK * Ks->data[i3 + Ks->size[0]];
      }

      b_iKs[0] = iKs->size[0];
      b_iKs[1] = iKs->size[1];
      b_Vm[0] = Vm->size[0];
      b_Vm[1] = Vm->size[1];
      if ((b_iKs[0] != b_Vm[0]) || (b_iKs[1] != b_Vm[1])) {
        emlrtSizeEqCheckNDR2012b(&b_iKs[0], &b_Vm[0], &r_emlrtECI, sp);
      }

      i3 = iKs->size[0] * iKs->size[1];
      i5 = iKs->size[0] * iKs->size[1];
      iKs->size[0] = 1;
      emxEnsureCapacity_real_T(sp, iKs, i5, &tb_emlrtRTEI);
      loop_ub = i3 - 1;
      for (i3 = 0; i3 <= loop_ub; i3++) {
        iKs->data[i3] *= (Vm->data[i3] + Vrest) - E_K;
      }

      emlrtMEXProfilingStatement(89, isMexOutdated);
      iNaf[0] = b_iNaf->size[0];
      iNaf[1] = b_iNaf->size[1];
      c_ecapRes[0] = ecapRes->size[0];
      c_ecapRes[1] = ecapRes->size[1];
      if ((iNaf[0] != c_ecapRes[0]) || (iNaf[1] != c_ecapRes[1])) {
        emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &q_emlrtECI, sp);
      }

      iNaf[0] = b_iNaf->size[0];
      iNaf[1] = b_iNaf->size[1];
      b_iKs[0] = iKs->size[0];
      b_iKs[1] = iKs->size[1];
      if ((iNaf[0] != b_iKs[0]) || (iNaf[1] != b_iKs[1])) {
        emlrtSizeEqCheckNDR2012b(&iNaf[0], &b_iKs[0], &q_emlrtECI, sp);
      }

      i3 = b_iNaf->size[0] * b_iNaf->size[1];
      i5 = b_iNaf->size[0] * b_iNaf->size[1];
      b_iNaf->size[0] = 1;
      emxEnsureCapacity_real_T(sp, b_iNaf, i5, &ub_emlrtRTEI);
      loop_ub = i3 - 1;
      for (i3 = 0; i3 <= loop_ub; i3++) {
        b_iNaf->data[i3] = (b_iNaf->data[i3] + ecapRes->data[i3]) + iKs->data[i3];
      }

      /*  Set rhs for Crank-Nicolson  */
      /*  RHS for first node segment. */
      emlrtMEXProfilingStatement(90, isMexOutdated);
      i3 = dist->size[1];
      if (1 > i3) {
        emlrtDynamicBoundsCheckR2012b(1, 1, i3, &mb_emlrtBCI, sp);
      }

      i3 = Vapp->size[1];
      if (2 > i3) {
        emlrtDynamicBoundsCheckR2012b(2, 1, i3, &qb_emlrtBCI, sp);
      }

      i3 = Vapp->size[1];
      if (1 > i3) {
        emlrtDynamicBoundsCheckR2012b(1, 1, i3, &pb_emlrtBCI, sp);
      }

      dist->data[0] = c->data[0] * (Vapp->data[1] - Vapp->data[0]);
      emlrtMEXProfilingStatement(91, isMexOutdated);
      emlrtMEXProfilingStatement(92, isMexOutdated);
      i3 = rhs->size[1];
      if (1 > i3) {
        emlrtDynamicBoundsCheckR2012b(1, 1, i3, &g_emlrtBCI, sp);
      }

      i3 = Vm->size[1];
      if (1 > i3) {
        emlrtDynamicBoundsCheckR2012b(1, 1, i3, &ob_emlrtBCI, sp);
      }

      i3 = Vm->size[1];
      if (2 > i3) {
        emlrtDynamicBoundsCheckR2012b(2, 1, i3, &nb_emlrtBCI, sp);
      }

      i3 = dist->size[1];
      if (1 > i3) {
        emlrtDynamicBoundsCheckR2012b(1, 1, i3, &lb_emlrtBCI, sp);
      }

      i3 = b_iNaf->size[1];
      if (1 > i3) {
        emlrtDynamicBoundsCheckR2012b(1, 1, i3, &kb_emlrtBCI, sp);
      }

      rhs->data[0] = ((rhsM->data[0] * Vm->data[0] - c->data[0] * Vm->data[1]) -
                      2.0 * dist->data[0]) + 2.0 * b_iNaf->data[0];

      /* RHS for segments 2:end-1. */
      emlrtMEXProfilingStatement(93, isMexOutdated);
      emlrtMEXProfilingStatement(94, isMexOutdated);
      if (2 > c->size[1] - 1) {
        i3 = 0;
        i4 = 0;
      } else {
        i3 = c->size[1];
        if (2 > i3) {
          emlrtDynamicBoundsCheckR2012b(2, 1, i3, &jb_emlrtBCI, sp);
        }

        i3 = 1;
        i5 = c->size[1];
        i4 = c->size[1] - 1;
        if ((i4 < 1) || (i4 > i5)) {
          emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &ib_emlrtBCI, sp);
        }
      }

      if (3 > numSegments + 1) {
        i5 = 0;
        i6 = 0;
      } else {
        i5 = Vapp->size[1];
        if (3 > i5) {
          emlrtDynamicBoundsCheckR2012b(3, 1, i5, &hb_emlrtBCI, sp);
        }

        i5 = 2;
        i6 = Vapp->size[1];
        if ((varargin_2 < 1) || (varargin_2 > i6)) {
          emlrtDynamicBoundsCheckR2012b(varargin_2, 1, i6, &gb_emlrtBCI, sp);
        }

        i6 = varargin_2;
      }

      if (2 > varargin_2 - 1) {
        i7 = 0;
        i8 = 0;
      } else {
        i7 = Vapp->size[1];
        if (2 > i7) {
          emlrtDynamicBoundsCheckR2012b(2, 1, i7, &fb_emlrtBCI, sp);
        }

        i7 = 1;
        i8 = Vapp->size[1];
        if ((numSegments < 1) || (numSegments > i8)) {
          emlrtDynamicBoundsCheckR2012b(numSegments, 1, i8, &eb_emlrtBCI, sp);
        }

        i8 = numSegments;
      }

      iNaf[0] = 1;
      i6 -= i5;
      iNaf[1] = i6;
      c_ecapRes[0] = 1;
      c_ecapRes[1] = i8 - i7;
      if (i6 != c_ecapRes[1]) {
        emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &p_emlrtECI, sp);
      }

      iNaf[0] = 1;
      loop_ub = i4 - i3;
      iNaf[1] = loop_ub;
      c_ecapRes[0] = 1;
      c_ecapRes[1] = i6;
      if (loop_ub != i6) {
        emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &m_emlrtECI, sp);
      }

      if (2 > a->size[1] - 1) {
        i4 = 0;
        i8 = 0;
      } else {
        i4 = a->size[1];
        if (2 > i4) {
          emlrtDynamicBoundsCheckR2012b(2, 1, i4, &db_emlrtBCI, sp);
        }

        i4 = 1;
        i6 = a->size[1];
        i8 = a->size[1] - 1;
        if ((i8 < 1) || (i8 > i6)) {
          emlrtDynamicBoundsCheckR2012b(i8, 1, i6, &cb_emlrtBCI, sp);
        }
      }

      if (2 > varargin_2 - 1) {
        i6 = 0;
        IDX = 0;
      } else {
        i6 = Vapp->size[1];
        if (2 > i6) {
          emlrtDynamicBoundsCheckR2012b(2, 1, i6, &bb_emlrtBCI, sp);
        }

        i6 = 1;
        IDX = Vapp->size[1];
        if ((numSegments < 1) || (numSegments > IDX)) {
          emlrtDynamicBoundsCheckR2012b(numSegments, 1, IDX, &ab_emlrtBCI, sp);
        }

        IDX = numSegments;
      }

      if (1 > varargin_2 - 2) {
        idx = 0;
      } else {
        nx = Vapp->size[1];
        if (1 > nx) {
          emlrtDynamicBoundsCheckR2012b(1, 1, nx, &y_emlrtBCI, sp);
        }

        nx = Vapp->size[1];
        idx = numSegments - 1;
        if ((idx < 1) || (idx > nx)) {
          emlrtDynamicBoundsCheckR2012b(idx, 1, nx, &x_emlrtBCI, sp);
        }
      }

      iNaf[0] = 1;
      IDX -= i6;
      iNaf[1] = IDX;
      c_ecapRes[0] = 1;
      c_ecapRes[1] = idx;
      if (IDX != idx) {
        emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &o_emlrtECI, sp);
      }

      iNaf[0] = 1;
      nx = i8 - i4;
      iNaf[1] = nx;
      c_ecapRes[0] = 1;
      c_ecapRes[1] = IDX;
      if (nx != IDX) {
        emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &n_emlrtECI, sp);
      }

      i8 = r0->size[0] * r0->size[1];
      r0->size[0] = 1;
      r0->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, r0, i8, &vb_emlrtRTEI);
      for (i8 = 0; i8 < loop_ub; i8++) {
        r0->data[i8] = c->data[i3 + i8] * (Vapp->data[i5 + i8] - Vapp->data[i7 +
          i8]);
      }

      i3 = iKs->size[0] * iKs->size[1];
      iKs->size[0] = 1;
      iKs->size[1] = nx;
      emxEnsureCapacity_real_T(sp, iKs, i3, &wb_emlrtRTEI);
      for (i3 = 0; i3 < nx; i3++) {
        iKs->data[i3] = a->data[i4 + i3] * (Vapp->data[i6 + i3] - Vapp->data[i3]);
      }

      iNaf[0] = r0->size[0];
      iNaf[1] = r0->size[1];
      b_iKs[0] = iKs->size[0];
      b_iKs[1] = iKs->size[1];
      if ((iNaf[0] != b_iKs[0]) || (iNaf[1] != b_iKs[1])) {
        emlrtSizeEqCheckNDR2012b(&iNaf[0], &b_iKs[0], &m_emlrtECI, sp);
      }

      if (2 > varargin_2 - 1) {
        i3 = 1;
        i5 = 0;
      } else {
        i3 = dist->size[1];
        if (2 > i3) {
          emlrtDynamicBoundsCheckR2012b(2, 1, i3, &w_emlrtBCI, sp);
        }

        i3 = 2;
        i5 = dist->size[1];
        if ((numSegments < 1) || (numSegments > i5)) {
          emlrtDynamicBoundsCheckR2012b(numSegments, 1, i5, &v_emlrtBCI, sp);
        }

        i5 = numSegments;
      }

      i5 = (i5 - i3) + 1;
      i4 = r0->size[1];
      if (i5 != i4) {
        emlrtSubAssignSizeCheck1dR2017a(i5, i4, &l_emlrtECI, sp);
      }

      loop_ub = r0->size[1];
      for (i5 = 0; i5 < loop_ub; i5++) {
        dist->data[(i3 + i5) - 1] = r0->data[i5] - iKs->data[i5];
      }

      emlrtMEXProfilingStatement(95, isMexOutdated);
      emlrtMEXProfilingStatement(96, isMexOutdated);
      emlrtMEXProfilingStatement(97, isMexOutdated);
      if (2 > a->size[1] - 1) {
        i3 = 0;
        i4 = 0;
      } else {
        i3 = a->size[1];
        if (2 > i3) {
          emlrtDynamicBoundsCheckR2012b(2, 1, i3, &u_emlrtBCI, sp);
        }

        i3 = 1;
        i5 = a->size[1];
        i4 = a->size[1] - 1;
        if ((i4 < 1) || (i4 > i5)) {
          emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &t_emlrtBCI, sp);
        }
      }

      if (1 > Vm->size[1] - 2) {
        i6 = 0;
      } else {
        i5 = Vm->size[1];
        if (1 > i5) {
          emlrtDynamicBoundsCheckR2012b(1, 1, i5, &s_emlrtBCI, sp);
        }

        i5 = Vm->size[1];
        i6 = Vm->size[1] - 2;
        if ((i6 < 1) || (i6 > i5)) {
          emlrtDynamicBoundsCheckR2012b(i6, 1, i5, &r_emlrtBCI, sp);
        }
      }

      i5 = r0->size[0] * r0->size[1];
      r0->size[0] = 1;
      loop_ub = i4 - i3;
      r0->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, r0, i5, &xb_emlrtRTEI);
      for (i5 = 0; i5 < loop_ub; i5++) {
        r0->data[i5] = -a->data[i3 + i5];
      }

      iNaf[0] = r0->size[0];
      iNaf[1] = r0->size[1];
      c_ecapRes[0] = 1;
      c_ecapRes[1] = i6;
      if ((iNaf[0] != 1) || (iNaf[1] != i6)) {
        emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &i_emlrtECI, sp);
      }

      if (2 > rhsM->size[1] - 1) {
        i3 = 0;
        i4 = 0;
      } else {
        i3 = 1;
        i5 = rhsM->size[1];
        i4 = rhsM->size[1] - 1;
        if ((i4 < 1) || (i4 > i5)) {
          emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &q_emlrtBCI, sp);
        }
      }

      if (2 > Vm->size[1] - 1) {
        i5 = 0;
        i7 = 0;
      } else {
        i5 = Vm->size[1];
        if (2 > i5) {
          emlrtDynamicBoundsCheckR2012b(2, 1, i5, &p_emlrtBCI, sp);
        }

        i5 = 1;
        i6 = Vm->size[1];
        i7 = Vm->size[1] - 1;
        if ((i7 < 1) || (i7 > i6)) {
          emlrtDynamicBoundsCheckR2012b(i7, 1, i6, &o_emlrtBCI, sp);
        }
      }

      iNaf[0] = 1;
      loop_ub = i4 - i3;
      iNaf[1] = loop_ub;
      c_ecapRes[0] = 1;
      c_ecapRes[1] = i7 - i5;
      if (loop_ub != c_ecapRes[1]) {
        emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &k_emlrtECI, sp);
      }

      i4 = iKs->size[0] * iKs->size[1];
      iKs->size[0] = 1;
      iKs->size[1] = r0->size[1];
      emxEnsureCapacity_real_T(sp, iKs, i4, &xb_emlrtRTEI);
      nx = r0->size[1];
      for (i4 = 0; i4 < nx; i4++) {
        iKs->data[i4] = r0->data[i4] * Vm->data[i4];
      }

      i4 = r0->size[0] * r0->size[1];
      r0->size[0] = 1;
      r0->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, r0, i4, &yb_emlrtRTEI);
      for (i4 = 0; i4 < loop_ub; i4++) {
        r0->data[i4] = rhsM->data[i3 + i4] * Vm->data[i5 + i4];
      }

      b_iKs[0] = iKs->size[0];
      b_iKs[1] = iKs->size[1];
      iNaf[0] = r0->size[0];
      iNaf[1] = r0->size[1];
      if ((b_iKs[0] != iNaf[0]) || (b_iKs[1] != iNaf[1])) {
        emlrtSizeEqCheckNDR2012b(&b_iKs[0], &iNaf[0], &i_emlrtECI, sp);
      }

      if (2 > c->size[1] - 1) {
        i3 = 0;
        i4 = 0;
      } else {
        i3 = c->size[1];
        if (2 > i3) {
          emlrtDynamicBoundsCheckR2012b(2, 1, i3, &n_emlrtBCI, sp);
        }

        i3 = 1;
        i5 = c->size[1];
        i4 = c->size[1] - 1;
        if ((i4 < 1) || (i4 > i5)) {
          emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &m_emlrtBCI, sp);
        }
      }

      if (3 > Vm->size[1]) {
        i5 = 0;
        i7 = 0;
      } else {
        i5 = 2;
        i6 = Vm->size[1];
        i7 = Vm->size[1];
        if ((i7 < 1) || (i7 > i6)) {
          emlrtDynamicBoundsCheckR2012b(i7, 1, i6, &l_emlrtBCI, sp);
        }
      }

      iNaf[0] = 1;
      loop_ub = i4 - i3;
      iNaf[1] = loop_ub;
      c_ecapRes[0] = 1;
      c_ecapRes[1] = i7 - i5;
      if (loop_ub != c_ecapRes[1]) {
        emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &j_emlrtECI, sp);
      }

      i4 = r1->size[0] * r1->size[1];
      r1->size[0] = 1;
      r1->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, r1, i4, &ac_emlrtRTEI);
      for (i4 = 0; i4 < loop_ub; i4++) {
        r1->data[i4] = c->data[i3 + i4] * Vm->data[i5 + i4];
      }

      b_iKs[0] = iKs->size[0];
      b_iKs[1] = iKs->size[1];
      iNaf[0] = r1->size[0];
      iNaf[1] = r1->size[1];
      if ((b_iKs[0] != iNaf[0]) || (b_iKs[1] != iNaf[1])) {
        emlrtSizeEqCheckNDR2012b(&b_iKs[0], &iNaf[0], &i_emlrtECI, sp);
      }

      if (2 > dist->size[1] - 1) {
        i3 = 0;
        i4 = 0;
      } else {
        i3 = dist->size[1];
        if (2 > i3) {
          emlrtDynamicBoundsCheckR2012b(2, 1, i3, &k_emlrtBCI, sp);
        }

        i3 = 1;
        i5 = dist->size[1];
        i4 = dist->size[1] - 1;
        if ((i4 < 1) || (i4 > i5)) {
          emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &j_emlrtBCI, sp);
        }
      }

      i5 = r4->size[0] * r4->size[1];
      r4->size[0] = 1;
      loop_ub = i4 - i3;
      r4->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, r4, i5, &bc_emlrtRTEI);
      for (i5 = 0; i5 < loop_ub; i5++) {
        r4->data[i5] = 2.0 * dist->data[i3 + i5];
      }

      b_iKs[0] = iKs->size[0];
      b_iKs[1] = iKs->size[1];
      iNaf[0] = r4->size[0];
      iNaf[1] = r4->size[1];
      if ((b_iKs[0] != iNaf[0]) || (b_iKs[1] != iNaf[1])) {
        emlrtSizeEqCheckNDR2012b(&b_iKs[0], &iNaf[0], &i_emlrtECI, sp);
      }

      if (2 > b_iNaf->size[1] - 1) {
        i3 = 0;
        i4 = 0;
      } else {
        i3 = b_iNaf->size[1];
        if (2 > i3) {
          emlrtDynamicBoundsCheckR2012b(2, 1, i3, &i_emlrtBCI, sp);
        }

        i3 = 1;
        i5 = b_iNaf->size[1];
        i4 = b_iNaf->size[1] - 1;
        if ((i4 < 1) || (i4 > i5)) {
          emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &h_emlrtBCI, sp);
        }
      }

      i5 = ecapRes->size[0] * ecapRes->size[1];
      ecapRes->size[0] = 1;
      loop_ub = i4 - i3;
      ecapRes->size[1] = loop_ub;
      emxEnsureCapacity_real_T(sp, ecapRes, i5, &cc_emlrtRTEI);
      for (i5 = 0; i5 < loop_ub; i5++) {
        ecapRes->data[i5] = 2.0 * b_iNaf->data[i3 + i5];
      }

      b_iKs[0] = iKs->size[0];
      b_iKs[1] = iKs->size[1];
      c_ecapRes[0] = ecapRes->size[0];
      c_ecapRes[1] = ecapRes->size[1];
      if ((b_iKs[0] != c_ecapRes[0]) || (b_iKs[1] != c_ecapRes[1])) {
        emlrtSizeEqCheckNDR2012b(&b_iKs[0], &c_ecapRes[0], &i_emlrtECI, sp);
      }

      if (2 > varargin_2 - 1) {
        i3 = 1;
        i5 = 0;
      } else {
        i3 = rhs->size[1];
        if (2 > i3) {
          emlrtDynamicBoundsCheckR2012b(2, 1, i3, &f_emlrtBCI, sp);
        }

        i3 = 2;
        i5 = rhs->size[1];
        if ((numSegments < 1) || (numSegments > i5)) {
          emlrtDynamicBoundsCheckR2012b(numSegments, 1, i5, &e_emlrtBCI, sp);
        }

        i5 = numSegments;
      }

      i5 = (i5 - i3) + 1;
      i4 = iKs->size[1];
      if (i5 != i4) {
        emlrtSubAssignSizeCheck1dR2017a(i5, i4, &h_emlrtECI, sp);
      }

      loop_ub = iKs->size[1];
      for (i5 = 0; i5 < loop_ub; i5++) {
        rhs->data[(i3 + i5) - 1] = (((iKs->data[i5] + r0->data[i5]) - r1->
          data[i5]) - r4->data[i5]) + ecapRes->data[i5];
      }

      /*  Axonal potential flux through segment i. */
      emlrtMEXProfilingStatement(98, isMexOutdated);
      i3 = a->size[1];
      i5 = a->size[1];
      if ((i5 < 1) || (i5 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i5, 1, i3, &qd_emlrtBCI, sp);
      }

      i3 = Vapp->size[1];
      if ((varargin_2 < 1) || (varargin_2 > i3)) {
        emlrtDynamicBoundsCheckR2012b(varargin_2, 1, i3, &rd_emlrtBCI, sp);
      }

      i3 = Vapp->size[1];
      i4 = varargin_2 - 1;
      if ((i4 < 1) || (i4 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i4, 1, i3, &sd_emlrtBCI, sp);
      }

      i3 = dist->size[1];
      i6 = dist->size[1];
      if ((i6 < 1) || (i6 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i6, 1, i3, &td_emlrtBCI, sp);
      }

      dist->data[i6 - 1] = -a->data[i5 - 1] * (Vapp->data[varargin_2 - 1] -
        Vapp->data[i4 - 1]);
      emlrtMEXProfilingStatement(99, isMexOutdated);
      emlrtMEXProfilingStatement(100, isMexOutdated);
      emlrtMEXProfilingStatement(101, isMexOutdated);
      i3 = a->size[1];
      i5 = a->size[1];
      if ((i5 < 1) || (i5 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i5, 1, i3, &ud_emlrtBCI, sp);
      }

      i3 = Vm->size[1];
      i4 = Vm->size[1] - 1;
      if ((i4 < 1) || (i4 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i4, 1, i3, &vd_emlrtBCI, sp);
      }

      i3 = rhsM->size[1];
      i6 = rhsM->size[1];
      if ((i6 < 1) || (i6 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i6, 1, i3, &wd_emlrtBCI, sp);
      }

      i3 = Vm->size[1];
      i7 = Vm->size[1];
      if ((i7 < 1) || (i7 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i7, 1, i3, &xd_emlrtBCI, sp);
      }

      i3 = dist->size[1];
      i8 = dist->size[1];
      if ((i8 < 1) || (i8 > i3)) {
        emlrtDynamicBoundsCheckR2012b(i8, 1, i3, &yd_emlrtBCI, sp);
      }

      i3 = b_iNaf->size[1];
      IDX = b_iNaf->size[1];
      if ((IDX < 1) || (IDX > i3)) {
        emlrtDynamicBoundsCheckR2012b(IDX, 1, i3, &ae_emlrtBCI, sp);
      }

      i3 = rhs->size[1];
      nx = rhs->size[1];
      if ((nx < 1) || (nx > i3)) {
        emlrtDynamicBoundsCheckR2012b(nx, 1, i3, &be_emlrtBCI, sp);
      }

      rhs->data[nx - 1] = ((-a->data[i5 - 1] * Vm->data[i4 - 1] + rhsM->data[i6
                            - 1] * Vm->data[i7 - 1]) - 2.0 * dist->data[i8 - 1])
        - 2.0 * b_iNaf->data[IDX - 1];

      /*  Solve tridiagonal matrix */
      emlrtMEXProfilingStatement(102, isMexOutdated);
      st.site = &v_emlrtRSI;
      tridag(&st, a, b, c, rhs, Vm);

      /*  Record spike times */
      emlrtMEXProfilingStatement(103, isMexOutdated);
      i3 = x->size[0] * x->size[1];
      x->size[0] = 1;
      x->size[1] = segments->nodeIDX->size[1];
      emxEnsureCapacity_boolean_T(sp, x, i3, &dc_emlrtRTEI);
      nx = Vm->size[1];
      loop_ub = segments->nodeIDX->size[0] * segments->nodeIDX->size[1];
      for (i3 = 0; i3 < loop_ub; i3++) {
        eCAP1 = segments->nodeIDX->data[i3];
        if (eCAP1 != (int32_T)muDoubleScalarFloor(eCAP1)) {
          emlrtIntegerCheckR2012b(eCAP1, &s_emlrtDCI, sp);
        }

        i5 = (int32_T)eCAP1;
        if ((i5 < 1) || (i5 > nx)) {
          emlrtDynamicBoundsCheckR2012b(i5, 1, nx, &ce_emlrtBCI, sp);
        }

        x->data[i3] = (Vm->data[i5 - 1] > Vthresh);
      }

      i3 = r5->size[0] * r5->size[1];
      r5->size[0] = 1;
      r5->size[1] = segments->nodeIDX->size[1];
      emxEnsureCapacity_boolean_T(sp, r5, i3, &ec_emlrtRTEI);
      nx = Vprev->size[1];
      loop_ub = segments->nodeIDX->size[0] * segments->nodeIDX->size[1];
      for (i3 = 0; i3 < loop_ub; i3++) {
        i5 = (int32_T)segments->nodeIDX->data[i3];
        if ((i5 < 1) || (i5 > nx)) {
          emlrtDynamicBoundsCheckR2012b(i5, 1, nx, &de_emlrtBCI, sp);
        }

        r5->data[i3] = (Vprev->data[i5 - 1] < Vthresh);
      }

      c_ecapRes[0] = x->size[0];
      c_ecapRes[1] = x->size[1];
      iNaf[0] = r5->size[0];
      iNaf[1] = r5->size[1];
      if ((c_ecapRes[0] != iNaf[0]) || (c_ecapRes[1] != iNaf[1])) {
        emlrtSizeEqCheckNDR2012b(&c_ecapRes[0], &iNaf[0], &g_emlrtECI, sp);
      }

      st.site = &w_emlrtRSI;
      i3 = x->size[0] * x->size[1];
      i5 = x->size[0] * x->size[1];
      x->size[0] = 1;
      emxEnsureCapacity_boolean_T(&st, x, i5, &fc_emlrtRTEI);
      loop_ub = i3 - 1;
      for (i3 = 0; i3 <= loop_ub; i3++) {
        x->data[i3] = (x->data[i3] && r5->data[i3]);
      }

      b_st.site = &ad_emlrtRSI;
      nx = x->size[1];
      c_st.site = &bd_emlrtRSI;
      idx = 0;
      i3 = ii->size[0] * ii->size[1];
      ii->size[0] = 1;
      ii->size[1] = x->size[1];
      emxEnsureCapacity_int32_T(&c_st, ii, i3, &gc_emlrtRTEI);
      d_st.site = &cd_emlrtRSI;
      overflow = ((1 <= x->size[1]) && (x->size[1] > 2147483646));
      if (overflow) {
        e_st.site = &mb_emlrtRSI;
        check_forloop_overflow_error(&e_st);
      }

      IDX = 0;
      exitg1 = false;
      while ((!exitg1) && (IDX <= nx - 1)) {
        if (x->data[IDX]) {
          idx++;
          ii->data[idx - 1] = IDX + 1;
          if (idx >= nx) {
            exitg1 = true;
          } else {
            IDX++;
          }
        } else {
          IDX++;
        }
      }

      if (idx > x->size[1]) {
        emlrtErrorWithMessageIdR2018a(&c_st, &ge_emlrtRTEI,
          "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
      }

      if (x->size[1] == 1) {
        if (idx == 0) {
          ii->size[0] = 1;
          ii->size[1] = 0;
        }
      } else if (1 > idx) {
        ii->size[1] = 0;
      } else {
        i3 = ii->size[0] * ii->size[1];
        ii->size[1] = idx;
        emxEnsureCapacity_int32_T(&c_st, ii, i3, &hc_emlrtRTEI);
      }

      i3 = ecapRes->size[0] * ecapRes->size[1];
      ecapRes->size[0] = 1;
      ecapRes->size[1] = ii->size[1];
      emxEnsureCapacity_real_T(&st, ecapRes, i3, &ic_emlrtRTEI);
      loop_ub = ii->size[0] * ii->size[1];
      for (i3 = 0; i3 < loop_ub; i3++) {
        ecapRes->data[i3] = ii->data[i3];
      }

      emlrtMEXProfilingStatement(104, isMexOutdated);
      st.site = &x_emlrtRSI;
      if (any(&st, ecapRes)) {
        emlrtMEXProfilingStatement(105, isMexOutdated);
        i3 = ii->size[0] * ii->size[1];
        ii->size[0] = 1;
        ii->size[1] = ecapRes->size[1];
        emxEnsureCapacity_int32_T(sp, ii, i3, &jc_emlrtRTEI);
        nx = b_numSpikes->size[1];
        loop_ub = ecapRes->size[0] * ecapRes->size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
          i5 = (int32_T)ecapRes->data[i3];
          if ((i5 < 1) || (i5 > nx)) {
            emlrtDynamicBoundsCheckR2012b(i5, 1, nx, &ee_emlrtBCI, sp);
          }

          ii->data[i3] = i5;
        }

        nx = b_numSpikes->size[1];
        loop_ub = ecapRes->size[0] * ecapRes->size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
          i5 = (int32_T)ecapRes->data[i3];
          if ((i5 < 1) || (i5 > nx)) {
            emlrtDynamicBoundsCheckR2012b(i5, 1, nx, &fe_emlrtBCI, sp);
          }
        }

        i3 = ii->size[1];
        i5 = ecapRes->size[1];
        if (i3 != i5) {
          emlrtSubAssignSizeCheck1dR2017a(i3, i5, &f_emlrtECI, sp);
        }

        loop_ub = ecapRes->size[0] * ecapRes->size[1];
        i3 = numSpikes->size[0];
        numSpikes->size[0] = loop_ub;
        emxEnsureCapacity_real_T(sp, numSpikes, i3, &mc_emlrtRTEI);
        for (i3 = 0; i3 < loop_ub; i3++) {
          numSpikes->data[i3] = b_numSpikes->data[(int32_T)ecapRes->data[i3] - 1]
            + 1.0;
        }

        loop_ub = numSpikes->size[0];
        for (i3 = 0; i3 < loop_ub; i3++) {
          b_numSpikes->data[ii->data[i3] - 1] = numSpikes->data[i3];
        }

        emlrtMEXProfilingStatement(106, isMexOutdated);
        i3 = c_numSpikes->size[0] * c_numSpikes->size[1];
        c_numSpikes->size[0] = 1;
        c_numSpikes->size[1] = b_numSpikes->size[1];
        emxEnsureCapacity_boolean_T(sp, c_numSpikes, i3, &oc_emlrtRTEI);
        loop_ub = b_numSpikes->size[0] * b_numSpikes->size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
          c_numSpikes->data[i3] = (b_numSpikes->data[i3] > maxSpikes);
        }

        st.site = &y_emlrtRSI;
        if (b_any(&st, c_numSpikes)) {
          emlrtMEXProfilingStatement(107, isMexOutdated);
          st.site = &ab_emlrtRSI;
          i3 = x->size[0] * x->size[1];
          x->size[0] = 1;
          x->size[1] = b_numSpikes->size[1];
          emxEnsureCapacity_boolean_T(&st, x, i3, &qc_emlrtRTEI);
          loop_ub = b_numSpikes->size[0] * b_numSpikes->size[1];
          for (i3 = 0; i3 < loop_ub; i3++) {
            x->data[i3] = (b_numSpikes->data[i3] > maxSpikes);
          }

          b_st.site = &ad_emlrtRSI;
          nx = (1 <= x->size[1]);
          if (nx > x->size[1]) {
            emlrtErrorWithMessageIdR2018a(&b_st, &he_emlrtRTEI,
              "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed",
              0);
          }

          c_st.site = &bd_emlrtRSI;
          idx = 0;
          i3 = ii->size[0] * ii->size[1];
          ii->size[0] = 1;
          ii->size[1] = nx;
          emxEnsureCapacity_int32_T(&c_st, ii, i3, &gc_emlrtRTEI);
          d_st.site = &cd_emlrtRSI;
          overflow = ((1 <= x->size[1]) && (x->size[1] > 2147483646));
          if (overflow) {
            e_st.site = &mb_emlrtRSI;
            check_forloop_overflow_error(&e_st);
          }

          IDX = 0;
          exitg1 = false;
          while ((!exitg1) && (IDX <= x->size[1] - 1)) {
            if (x->data[IDX]) {
              idx++;
              ii->data[idx - 1] = IDX + 1;
              if (idx >= nx) {
                exitg1 = true;
              } else {
                IDX++;
              }
            } else {
              IDX++;
            }
          }

          if (idx > nx) {
            emlrtErrorWithMessageIdR2018a(&c_st, &ge_emlrtRTEI,
              "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed",
              0);
          }

          if (nx == 1) {
            if (idx == 0) {
              ii->size[0] = 1;
              ii->size[1] = 0;
            }
          } else if (1 > idx) {
            ii->size[1] = 0;
          } else {
            i3 = ii->size[0] * ii->size[1];
            ii->size[1] = 1;
            emxEnsureCapacity_int32_T(&c_st, ii, i3, &sc_emlrtRTEI);
          }

          i3 = b_segments->size[0] * b_segments->size[1];
          b_segments->size[0] = 1;
          b_segments->size[1] = ii->size[1];
          emxEnsureCapacity_real_T(sp, b_segments, i3, &gc_emlrtRTEI);
          loop_ub = ii->size[0] * ii->size[1];
          for (i3 = 0; i3 < loop_ub; i3++) {
            b_segments->data[i3] = ii->data[i3];
          }

          st.site = &ab_emlrtRSI;
          e_error(&st, b_segments->data, b_segments->size);
        }

        emlrtMEXProfilingStatement(109, isMexOutdated);
        i3 = ecapRes->size[1];
        for (nx = 0; nx < i3; nx++) {
          emlrtMEXProfilingStatement(110, isMexOutdated);
          i5 = ecapRes->size[1];
          i4 = 1 + nx;
          if ((i4 < 1) || (i4 > i5)) {
            emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &d_emlrtBCI, sp);
          }

          emlrtMEXProfilingStatement(111, isMexOutdated);
          i5 = varargout_1->size[0];
          i4 = (int32_T)(1U + monte);
          if ((i4 < 1) || (i4 > i5)) {
            emlrtDynamicBoundsCheckR2012b(i4, 1, i5, &ge_emlrtBCI, sp);
          }

          i5 = varargout_1->size[1];
          i6 = (int32_T)(uint32_T)ecapRes->data[nx];
          if ((i6 < 1) || (i6 > i5)) {
            emlrtDynamicBoundsCheckR2012b(i6, 1, i5, &ge_emlrtBCI, sp);
          }

          i5 = varargout_1->size[2];
          i7 = b_numSpikes->size[1];
          i8 = (int32_T)(uint32_T)ecapRes->data[nx];
          if ((i8 < 1) || (i8 > i7)) {
            emlrtDynamicBoundsCheckR2012b(i8, 1, i7, &he_emlrtBCI, sp);
          }

          i7 = (int32_T)b_numSpikes->data[i8 - 1];
          if ((i7 < 1) || (i7 > i5)) {
            emlrtDynamicBoundsCheckR2012b(i7, 1, i5, &ge_emlrtBCI, sp);
          }

          varargout_1->data[((i4 + varargout_1->size[0] * (i6 - 1)) +
                             varargout_1->size[0] * varargout_1->size[1] * (i7 -
            1)) - 1] = 2.0 + (real_T)it;
          emlrtMEXProfilingStatement(112, isMexOutdated);
          if (*emlrtBreakCheckR2012bFlagVar != 0) {
            emlrtBreakCheckR2012b(sp);
          }
        }

        emlrtMEXProfilingStatement(113, isMexOutdated);
      }

      /*  Record voltage */
      emlrtMEXProfilingStatement(114, isMexOutdated);
      st.site = &bb_emlrtRSI;
      toLogicalCheck(&st, recV);
      if (recV != 0.0) {
        emlrtMEXProfilingStatement(115, isMexOutdated);
        eCAP1 = ((2.0 + (real_T)it) - 1.0) * dt;
        if (b_mod(eCAP1, Vsample) == 0.0) {
          emlrtMEXProfilingStatement(116, isMexOutdated);
          i3 = varargout_2->size[0];
          i5 = 1 + monte;
          if ((i5 < 1) || (i5 > i3)) {
            emlrtDynamicBoundsCheckR2012b(i5, 1, i3, &c_emlrtBCI, sp);
          }

          eCAP1 = muDoubleScalarRound(eCAP1 / Vsample + 1.0);
          i3 = varargout_2->size[1];
          i5 = (int32_T)eCAP1;
          if (eCAP1 != i5) {
            emlrtIntegerCheckR2012b(eCAP1, &emlrtDCI, sp);
          }

          if ((i5 < 1) || (i5 > i3)) {
            emlrtDynamicBoundsCheckR2012b(i5, 1, i3, &b_emlrtBCI, sp);
          }

          loop_ub = varargout_2->size[2];
          i3 = r2->size[0];
          r2->size[0] = loop_ub;
          emxEnsureCapacity_int32_T(sp, r2, i3, &kc_emlrtRTEI);
          for (i3 = 0; i3 < loop_ub; i3++) {
            r2->data[i3] = i3;
          }

          iv4[0] = 1;
          iv4[1] = 1;
          iv4[2] = r2->size[0];
          emlrtSubAssignSizeCheckR2012b(&iv4[0], 3, &(*(int32_T (*)[2])Vm->size)
            [0], 2, &b_emlrtECI, sp);
          nx = r2->size[0];
          for (i3 = 0; i3 < nx; i3++) {
            varargout_2->data[(monte + varargout_2->size[0] * (i5 - 1)) +
              varargout_2->size[0] * varargout_2->size[1] * r2->data[i3]] =
              (real32_T)Vm->data[i3];
          }

          emlrtMEXProfilingStatement(117, isMexOutdated);
        }

        emlrtMEXProfilingStatement(118, isMexOutdated);
      }

      emlrtMEXProfilingStatement(119, isMexOutdated);
      st.site = &cb_emlrtRSI;
      toLogicalCheck(&st, recECAP);
      if (recECAP != 0.0) {
        emlrtMEXProfilingStatement(120, isMexOutdated);
        st.site = &db_emlrtRSI;
        b_rdivide_helper(&st, Vm, segments->rm, ecapRes);
        emlrtMEXProfilingStatement(121, isMexOutdated);
        b_Vm[0] = Vm->size[0];
        b_Vm[1] = Vm->size[1];
        c_ecapRes[0] = Vprev->size[0];
        c_ecapRes[1] = Vprev->size[1];
        if ((b_Vm[0] != c_ecapRes[0]) || (b_Vm[1] != c_ecapRes[1])) {
          emlrtSizeEqCheckNDR2012b(&b_Vm[0], &c_ecapRes[0], &e_emlrtECI, sp);
        }

        c_ecapRes[0] = segments->cm->size[0];
        c_ecapRes[1] = segments->cm->size[1];
        b_Vm[0] = Vm->size[0];
        b_Vm[1] = Vm->size[1];
        if ((c_ecapRes[0] != b_Vm[0]) || (c_ecapRes[1] != b_Vm[1])) {
          emlrtSizeEqCheckNDR2012b(&c_ecapRes[0], &b_Vm[0], &d_emlrtECI, sp);
        }

        i3 = segments->cm->size[0] * segments->cm->size[1];
        i5 = Vprev->size[0] * Vprev->size[1];
        Vprev->size[0] = 1;
        Vprev->size[1] = segments->cm->size[1];
        emxEnsureCapacity_real_T(sp, Vprev, i5, &pc_emlrtRTEI);
        loop_ub = i3 - 1;
        for (i3 = 0; i3 <= loop_ub; i3++) {
          Vprev->data[i3] = segments->cm->data[i3] * (Vm->data[i3] - Vprev->
            data[i3]) / dt;
        }

        emlrtMEXProfilingStatement(122, isMexOutdated);
        iNaf[0] = b_iNaf->size[0];
        iNaf[1] = b_iNaf->size[1];
        c_ecapRes[0] = ecapRes->size[0];
        c_ecapRes[1] = ecapRes->size[1];
        if ((iNaf[0] != c_ecapRes[0]) || (iNaf[1] != c_ecapRes[1])) {
          emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &c_emlrtECI, sp);
        }

        iNaf[0] = b_iNaf->size[0];
        iNaf[1] = b_iNaf->size[1];
        c_ecapRes[0] = Vprev->size[0];
        c_ecapRes[1] = Vprev->size[1];
        if ((iNaf[0] != c_ecapRes[0]) || (iNaf[1] != c_ecapRes[1])) {
          emlrtSizeEqCheckNDR2012b(&iNaf[0], &c_ecapRes[0], &c_emlrtECI, sp);
        }

        i3 = b_iNaf->size[0] * b_iNaf->size[1];
        i5 = b_iNaf->size[0] * b_iNaf->size[1];
        b_iNaf->size[0] = 1;
        emxEnsureCapacity_real_T(sp, b_iNaf, i5, &rc_emlrtRTEI);
        loop_ub = i3 - 1;
        for (i3 = 0; i3 <= loop_ub; i3++) {
          b_iNaf->data[i3] = (b_iNaf->data[i3] + ecapRes->data[i3]) +
            Vprev->data[i3];
        }

        emlrtMEXProfilingStatement(123, isMexOutdated);
        st.site = &eb_emlrtRSI;
        b_st.site = &ub_emlrtRSI;
        if (b_iNaf->size[1] != b_ecapRes->size[0]) {
          if ((b_iNaf->size[1] == 1) || (b_ecapRes->size[0] == 1)) {
            emlrtErrorWithMessageIdR2018a(&b_st, &ie_emlrtRTEI,
              "Coder:toolbox:mtimes_noDynamicScalarExpansion",
              "Coder:toolbox:mtimes_noDynamicScalarExpansion", 0);
          } else {
            emlrtErrorWithMessageIdR2018a(&b_st, &je_emlrtRTEI,
              "MATLAB:innerdim", "MATLAB:innerdim", 0);
          }
        }

        if ((b_iNaf->size[1] == 1) || (b_ecapRes->size[0] == 1)) {
          eCAP1 = 0.0;
          loop_ub = b_iNaf->size[1];
          for (i3 = 0; i3 < loop_ub; i3++) {
            eCAP1 += b_iNaf->data[i3] * b_ecapRes->data[i3];
          }
        } else {
          b_st.site = &tb_emlrtRSI;
          c_st.site = &vb_emlrtRSI;
          d_st.site = &wb_emlrtRSI;
          if (b_iNaf->size[1] < 1) {
            eCAP1 = 0.0;
          } else {
            n_t = (ptrdiff_t)b_iNaf->size[1];
            incx_t = (ptrdiff_t)1;
            incy_t = (ptrdiff_t)1;
            eCAP1 = ddot(&n_t, &b_iNaf->data[0], &incx_t, &b_ecapRes->data[0],
                         &incy_t);
          }
        }

        i3 = varargout_3->size[0];
        i5 = (int32_T)(1U + monte);
        if ((i5 < 1) || (i5 > i3)) {
          emlrtDynamicBoundsCheckR2012b(i5, 1, i3, &ie_emlrtBCI, sp);
        }

        i3 = varargout_3->size[1];
        i4 = 2 + it;
        if ((i4 < 1) || (i4 > i3)) {
          emlrtDynamicBoundsCheckR2012b(i4, 1, i3, &ie_emlrtBCI, sp);
        }

        varargout_3->data[(i5 + varargout_3->size[0] * (i4 - 1)) - 1] =
          (real32_T)eCAP1;
        emlrtMEXProfilingStatement(124, isMexOutdated);
      }

      emlrtMEXProfilingStatement(125, isMexOutdated);
      i3 = c_numSpikes->size[0] * c_numSpikes->size[1];
      c_numSpikes->size[0] = 1;
      c_numSpikes->size[1] = Vm->size[1];
      emxEnsureCapacity_boolean_T(sp, c_numSpikes, i3, &lc_emlrtRTEI);
      loop_ub = Vm->size[0] * Vm->size[1];
      for (i3 = 0; i3 < loop_ub; i3++) {
        c_numSpikes->data[i3] = (Vm->data[i3] > maxVolt);
      }

      st.site = &fb_emlrtRSI;
      if (b_any(&st, c_numSpikes)) {
        emlrtMEXProfilingStatement(126, isMexOutdated);
        st.site = &gb_emlrtRSI;
        i3 = x->size[0] * x->size[1];
        x->size[0] = 1;
        x->size[1] = Vm->size[1];
        emxEnsureCapacity_boolean_T(&st, x, i3, &nc_emlrtRTEI);
        loop_ub = Vm->size[0] * Vm->size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
          x->data[i3] = (Vm->data[i3] > maxVolt);
        }

        b_st.site = &ad_emlrtRSI;
        nx = (1 <= x->size[1]);
        if (nx > x->size[1]) {
          emlrtErrorWithMessageIdR2018a(&b_st, &he_emlrtRTEI,
            "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed",
            0);
        }

        c_st.site = &bd_emlrtRSI;
        idx = 0;
        i3 = ii->size[0] * ii->size[1];
        ii->size[0] = 1;
        ii->size[1] = nx;
        emxEnsureCapacity_int32_T(&c_st, ii, i3, &gc_emlrtRTEI);
        d_st.site = &cd_emlrtRSI;
        overflow = ((1 <= x->size[1]) && (x->size[1] > 2147483646));
        if (overflow) {
          e_st.site = &mb_emlrtRSI;
          check_forloop_overflow_error(&e_st);
        }

        IDX = 0;
        exitg1 = false;
        while ((!exitg1) && (IDX <= x->size[1] - 1)) {
          if (x->data[IDX]) {
            idx++;
            ii->data[idx - 1] = IDX + 1;
            if (idx >= nx) {
              exitg1 = true;
            } else {
              IDX++;
            }
          } else {
            IDX++;
          }
        }

        if (idx > nx) {
          emlrtErrorWithMessageIdR2018a(&c_st, &ge_emlrtRTEI,
            "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed",
            0);
        }

        if (nx == 1) {
          if (idx == 0) {
            ii->size[0] = 1;
            ii->size[1] = 0;
          }
        } else if (1 > idx) {
          ii->size[1] = 0;
        } else {
          i3 = ii->size[0] * ii->size[1];
          ii->size[1] = 1;
          emxEnsureCapacity_int32_T(&c_st, ii, i3, &sc_emlrtRTEI);
        }

        badIDX_size[0] = 1;
        badIDX_size[1] = ii->size[1];
        loop_ub = ii->size[0] * ii->size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
          badIDX_data[i3] = ii->data[i3];
        }

        emlrtMEXProfilingStatement(127, isMexOutdated);
        Vm_size[0] = 1;
        Vm_size[1] = badIDX_size[1];
        nx = Vm->size[1];
        loop_ub = badIDX_size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
          if (((int32_T)badIDX_data[i3] < 1) || ((int32_T)badIDX_data[i3] > nx))
          {
            emlrtDynamicBoundsCheckR2012b((int32_T)badIDX_data[i3], 1, nx,
              &je_emlrtBCI, sp);
          }

          Vm_data[i3] = Vm->data[(int32_T)badIDX_data[i3] - 1];
        }

        st.site = &hb_emlrtRSI;
        f_error(&st, 2.0 + (real_T)it, badIDX_data, badIDX_size, Vm_data,
                Vm_size);
      }

      emlrtMEXProfilingStatement(129, isMexOutdated);
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    emlrtMEXProfilingStatement(130, isMexOutdated);
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFree_real_T(&numSpikes);
  emxFree_boolean_T(&c_numSpikes);
  emxFree_real_T(&b_segments);
  emxFree_real_T(&r1);
  emxFree_real_T(&r0);
  emxFree_int32_T(&ii);
  emxFree_boolean_T(&x);
  emxFree_real_T(&a);
  emxFree_int32_T(&r2);
  emxFree_boolean_T(&r5);
  emxFree_real_T(&r4);
  emxFree_real_T(&b_ecapRes);
  emxFree_real_T(&iKs);
  emxFree_real_T(&b_iNaf);
  emxFree_real_T(&Vprev);
  emxFree_real_T(&Vapp);
  emxFree_real_T(&ratesKs);
  emxFree_real_T(&ratesKf);
  emxFree_real_T(&ratesNaf);
  emxFree_real_T(&dist);
  emxFree_real_T(&rhs);
  emxFree_real_T(&b_numSpikes);
  emxFree_real_T(&Ks);
  emxFree_real_T(&Kf);
  emxFree_real_T(&Naf);
  emxFree_real_T(&rhsM);
  emxFree_real_T(&c);
  emxFree_real_T(&b);
  emxFree_real_T(&ecapRes);
  emxFree_real_T(&fieldResist);
  emxFree_real_T(&Vm);
  emlrtMEXProfilingStatement(131, isMexOutdated);
  emlrtMEXProfilingStatement(132, isMexOutdated);
  emlrtMEXProfilingStatement(133, isMexOutdated);
  emlrtMEXProfilingStatement(134, isMexOutdated);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (stochAN_multi.c) */
