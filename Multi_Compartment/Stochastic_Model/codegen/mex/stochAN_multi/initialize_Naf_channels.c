/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * initialize_Naf_channels.c
 *
 * Code generation for function 'initialize_Naf_channels'
 *
 */

/* Include files */
#include <string.h>
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "initialize_Naf_channels.h"
#include "error.h"
#include "stochAN_multi_data.h"

/* Variable Definitions */
static emlrtRSInfo mc_emlrtRSI = { 39, /* lineNo */
  "initialize_Naf_channels",           /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Naf_channels.m"/* pathName */
};

static emlrtRSInfo nc_emlrtRSI = { 68, /* lineNo */
  "initialize_Naf_channels",           /* fcnName */
  "/Users/kal/Desktop/Rubinstein Lab/Resnick-stoch_an-d3572f1e973e/Multi_Compartment/Stochastic_Model/initialize_Naf_channels.m"/* pathName */
};

/* Function Definitions */
void initialize_Naf_channels(const emlrtStack *sp, const real_T ratesNaf[4],
  real_T numNaf, real_T dt, real_T numNa[8])
{
  int32_T idx;
  static const int16_T a[8] = { 282, 61, 12, 0, 799, 230, 15, 1 };

  real_T zeta[8];
  real_T Tl;
  boolean_T exitg1;
  real_T lambda;
  real_T x;
  real_T P[20];
  int32_T ii_size_idx_1;
  int32_T ii;
  boolean_T exitg2;
  int8_T ii_data[1];
  int8_T ind_data[1];
  static const int8_T iv7[20] = { 1, 2, 2, 3, 3, 4, 1, 5, 2, 6, 3, 7, 4, 8, 5, 6,
    6, 7, 7, 8 };

  real_T numNa_data[1];
  static const int8_T iv8[20] = { 2, 1, 3, 2, 4, 3, 5, 1, 6, 2, 7, 3, 8, 4, 6, 5,
    7, 6, 8, 7 };

  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtMEXProfilingFunctionEntry(c_initialize_Naf_channels_compl, isMexOutdated);

  /*  NUMNA: This function initializes the number of channels in each state */
  /*  using a Jump Monte Carlo method. */
  /*  */
  /*  Usage: numNa = initialize_Naf_channels(ratesNa,numNaf,dt) */
  /*  */
  /*    numNa:      Array of # of channels in each state rates */
  /*    reatesNaf:  Voltage dependent transition rates num */
  /*    Naf:        Number of K channels at segment */
  /*    dt:         Timestep (us) */
  /*  */
  /*  Jesse M. Resnick (resnick@uw.edu) � 2018 */
  /*  initial destribution of particles in each channel */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  for (idx = 0; idx < 8; idx++) {
    numNa[idx] = muDoubleScalarRound((real_T)a[idx] * numNaf / 1400.0);
  }

  /*  Net transition rates for each channel type. */
  emlrtMEXProfilingStatement(3, isMexOutdated);
  emlrtMEXProfilingStatement(4, isMexOutdated);
  emlrtMEXProfilingStatement(5, isMexOutdated);
  emlrtMEXProfilingStatement(6, isMexOutdated);
  emlrtMEXProfilingStatement(7, isMexOutdated);
  emlrtMEXProfilingStatement(8, isMexOutdated);
  emlrtMEXProfilingStatement(9, isMexOutdated);
  emlrtMEXProfilingStatement(10, isMexOutdated);
  zeta[0] = 3.0 * ratesNaf[0] + ratesNaf[2];
  zeta[1] = (ratesNaf[1] + ratesNaf[2]) + 2.0 * ratesNaf[0];
  zeta[2] = (2.0 * ratesNaf[1] + ratesNaf[2]) + ratesNaf[0];
  zeta[3] = 3.0 * ratesNaf[1] + ratesNaf[2];
  zeta[4] = 3.0 * ratesNaf[0] + ratesNaf[3];
  zeta[5] = (ratesNaf[1] + ratesNaf[3]) + 2.0 * ratesNaf[0];
  zeta[6] = (2.0 * ratesNaf[1] + ratesNaf[3]) + ratesNaf[0];
  zeta[7] = 3.0 * ratesNaf[1] + ratesNaf[3];
  emlrtMEXProfilingStatement(11, isMexOutdated);
  Tl = 0.0;

  /*  Indices of channel types for different reaction types. */
  emlrtMEXProfilingStatement(12, isMexOutdated);
  emlrtMEXProfilingStatement(13, isMexOutdated);
  emlrtMEXProfilingStatement(14, isMexOutdated);
  exitg1 = false;
  while ((!exitg1) && (Tl <= 100.0 * dt)) {
    emlrtMEXProfilingStatement(15, isMexOutdated);
    lambda = 0.0;
    for (idx = 0; idx < 8; idx++) {
      lambda += numNa[idx] * zeta[idx];
    }

    emlrtMEXProfilingStatement(16, isMexOutdated);
    st.site = &mc_emlrtRSI;
    emlrtRandu(&x, 1);
    st.site = &mc_emlrtRSI;
    if (x < 0.0) {
      b_st.site = &oc_emlrtRSI;
      d_error(&b_st);
    }

    x = muDoubleScalarLog(x);
    emlrtMEXProfilingStatement(17, isMexOutdated);
    Tl += -x / lambda;
    emlrtMEXProfilingStatement(18, isMexOutdated);
    if (Tl > 100.0 * dt) {
      emlrtMEXProfilingStatement(19, isMexOutdated);
      exitg1 = true;
    } else {
      emlrtMEXProfilingStatement(21, isMexOutdated);
      emlrtMEXProfilingStatement(22, isMexOutdated);
      emlrtMEXProfilingStatement(23, isMexOutdated);
      emlrtMEXProfilingStatement(24, isMexOutdated);
      emlrtMEXProfilingStatement(25, isMexOutdated);
      emlrtMEXProfilingStatement(26, isMexOutdated);
      emlrtMEXProfilingStatement(27, isMexOutdated);
      emlrtMEXProfilingStatement(28, isMexOutdated);
      emlrtMEXProfilingStatement(29, isMexOutdated);
      emlrtMEXProfilingStatement(30, isMexOutdated);
      emlrtMEXProfilingStatement(31, isMexOutdated);
      emlrtMEXProfilingStatement(32, isMexOutdated);
      emlrtMEXProfilingStatement(33, isMexOutdated);
      emlrtMEXProfilingStatement(34, isMexOutdated);
      emlrtMEXProfilingStatement(35, isMexOutdated);
      emlrtMEXProfilingStatement(36, isMexOutdated);
      emlrtMEXProfilingStatement(37, isMexOutdated);
      emlrtMEXProfilingStatement(38, isMexOutdated);
      emlrtMEXProfilingStatement(39, isMexOutdated);
      emlrtMEXProfilingStatement(40, isMexOutdated);
      emlrtMEXProfilingStatement(41, isMexOutdated);
      P[0] = 3.0 * ratesNaf[0] * numNa[0] / lambda;
      P[1] = ratesNaf[1] * numNa[1] / lambda;
      P[2] = 2.0 * ratesNaf[0] * numNa[1] / lambda;
      P[3] = 2.0 * ratesNaf[1] * numNa[2] / lambda;
      P[4] = ratesNaf[0] * numNa[2] / lambda;
      P[5] = 3.0 * ratesNaf[1] * numNa[3] / lambda;
      P[6] = ratesNaf[2] * numNa[0] / lambda;
      P[7] = ratesNaf[3] * numNa[4] / lambda;
      P[8] = ratesNaf[2] * numNa[1] / lambda;
      P[9] = ratesNaf[3] * numNa[5] / lambda;
      P[10] = ratesNaf[2] * numNa[2] / lambda;
      P[11] = ratesNaf[3] * numNa[6] / lambda;
      P[12] = ratesNaf[2] * numNa[3] / lambda;
      P[13] = ratesNaf[3] * numNa[7] / lambda;
      P[14] = 3.0 * ratesNaf[0] * numNa[4] / lambda;
      P[15] = ratesNaf[1] * numNa[5] / lambda;
      P[16] = 2.0 * ratesNaf[0] * numNa[5] / lambda;
      P[17] = 2.0 * ratesNaf[1] * numNa[6] / lambda;
      P[18] = ratesNaf[0] * numNa[6] / lambda;
      P[19] = 3.0 * ratesNaf[1] * numNa[7] / lambda;
      for (idx = 0; idx < 19; idx++) {
        P[idx + 1] += P[idx];
      }

      emlrtMEXProfilingStatement(42, isMexOutdated);
      st.site = &nc_emlrtRSI;
      emlrtRandu(&lambda, 1);
      idx = 0;
      ii_size_idx_1 = 1;
      ii = 0;
      exitg2 = false;
      while ((!exitg2) && (ii < 20)) {
        if (lambda < P[ii]) {
          idx = 1;
          ii_data[0] = (int8_T)(ii + 1);
          exitg2 = true;
        } else {
          ii++;
        }
      }

      if (idx == 0) {
        ii_size_idx_1 = 0;
      }

      if (0 <= ii_size_idx_1 - 1) {
        memcpy(&ind_data[0], &ii_data[0], (uint32_T)(ii_size_idx_1 * (int32_T)
                sizeof(int8_T)));
      }

      emlrtMEXProfilingStatement(43, isMexOutdated);
      for (idx = 0; idx < ii_size_idx_1; idx++) {
        ii_data[0] = iv7[ind_data[0] - 1];
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numNa_data[0] = numNa[ii_data[0] - 1] - 1.0;
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numNa[ii_data[0] - 1] = numNa_data[0];
      }

      emlrtMEXProfilingStatement(44, isMexOutdated);
      for (idx = 0; idx < ii_size_idx_1; idx++) {
        ii_data[0] = iv8[ind_data[0] - 1];
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numNa_data[0] = numNa[ii_data[0] - 1] + 1.0;
      }

      for (idx = 0; idx < ii_size_idx_1; idx++) {
        numNa[ii_data[0] - 1] = numNa_data[0];
      }

      emlrtMEXProfilingStatement(45, isMexOutdated);
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }
  }

  emlrtMEXProfilingStatement(46, isMexOutdated);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
}

/* End of code generation (initialize_Naf_channels.c) */
