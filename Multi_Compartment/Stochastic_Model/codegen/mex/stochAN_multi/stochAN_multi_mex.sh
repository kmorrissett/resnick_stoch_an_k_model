MATLAB="/Applications/MATLAB_R2018b.app"
Arch=maci64
ENTRYPOINT=mexFunction
MAPFILE=$ENTRYPOINT'.map'
PREFDIR="/Users/kal/Library/Application Support/MathWorks/MATLAB/R2018b"
OPTSFILE_NAME="./setEnv.sh"
. $OPTSFILE_NAME
COMPILER=$CC
. $OPTSFILE_NAME
echo "# Make settings for stochAN_multi" > stochAN_multi_mex.mki
echo "CC=$CC" >> stochAN_multi_mex.mki
echo "CFLAGS=$CFLAGS" >> stochAN_multi_mex.mki
echo "CLIBS=$CLIBS" >> stochAN_multi_mex.mki
echo "COPTIMFLAGS=$COPTIMFLAGS" >> stochAN_multi_mex.mki
echo "CDEBUGFLAGS=$CDEBUGFLAGS" >> stochAN_multi_mex.mki
echo "CXX=$CXX" >> stochAN_multi_mex.mki
echo "CXXFLAGS=$CXXFLAGS" >> stochAN_multi_mex.mki
echo "CXXLIBS=$CXXLIBS" >> stochAN_multi_mex.mki
echo "CXXOPTIMFLAGS=$CXXOPTIMFLAGS" >> stochAN_multi_mex.mki
echo "CXXDEBUGFLAGS=$CXXDEBUGFLAGS" >> stochAN_multi_mex.mki
echo "LDFLAGS=$LDFLAGS" >> stochAN_multi_mex.mki
echo "LDOPTIMFLAGS=$LDOPTIMFLAGS" >> stochAN_multi_mex.mki
echo "LDDEBUGFLAGS=$LDDEBUGFLAGS" >> stochAN_multi_mex.mki
echo "Arch=$Arch" >> stochAN_multi_mex.mki
echo "LD=$LD" >> stochAN_multi_mex.mki
echo OMPFLAGS= >> stochAN_multi_mex.mki
echo OMPLINKFLAGS= >> stochAN_multi_mex.mki
echo "EMC_COMPILER=clang" >> stochAN_multi_mex.mki
echo "EMC_CONFIG=optim" >> stochAN_multi_mex.mki
"/Applications/MATLAB_R2018b.app/bin/maci64/gmake" -j 1 -B -f stochAN_multi_mex.mk
