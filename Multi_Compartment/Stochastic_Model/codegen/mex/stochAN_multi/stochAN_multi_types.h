/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * stochAN_multi_types.h
 *
 * Code generation for function 'stochAN_multi'
 *
 */

#ifndef STOCHAN_MULTI_TYPES_H
#define STOCHAN_MULTI_TYPES_H

/* Include files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef struct_emxArray_boolean_T
#define struct_emxArray_boolean_T

struct emxArray_boolean_T
{
  boolean_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_boolean_T*/

#ifndef typedef_emxArray_boolean_T
#define typedef_emxArray_boolean_T

typedef struct emxArray_boolean_T emxArray_boolean_T;

#endif                                 /*typedef_emxArray_boolean_T*/

#ifndef struct_emxArray_int32_T
#define struct_emxArray_int32_T

struct emxArray_int32_T
{
  int32_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_int32_T*/

#ifndef typedef_emxArray_int32_T
#define typedef_emxArray_int32_T

typedef struct emxArray_int32_T emxArray_int32_T;

#endif                                 /*typedef_emxArray_int32_T*/

#ifndef struct_emxArray_real32_T
#define struct_emxArray_real32_T

struct emxArray_real32_T
{
  real32_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_real32_T*/

#ifndef typedef_emxArray_real32_T
#define typedef_emxArray_real32_T

typedef struct emxArray_real32_T emxArray_real32_T;

#endif                                 /*typedef_emxArray_real32_T*/

#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T

struct emxArray_real_T
{
  real_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_real_T*/

#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T

typedef struct emxArray_real_T emxArray_real_T;

#endif                                 /*typedef_emxArray_real_T*/

#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct {
  emxArray_real_T *cm;
  emxArray_real_T *nodeIDX;
  emxArray_real_T *numKf;
  emxArray_real_T *numKs;
  emxArray_real_T *numNaf;
  emxArray_real_T *ra;
  emxArray_real_T *rm;
  emxArray_real_T *xApp;
  emxArray_real_T *xEcap;
  real_T zDistance;
} struct0_T;

#endif                                 /*typedef_struct0_T*/

#ifndef typedef_struct2_T
#define typedef_struct2_T

typedef struct {
  real_T rm;
  real_T cm;
  real_T ra;
} struct2_T;

#endif                                 /*typedef_struct2_T*/

#ifndef typedef_struct3_T
#define typedef_struct3_T

typedef struct {
  real_T E_K;
  real_T gK;
  real_T density;
  real_T Kins[6];
} struct3_T;

#endif                                 /*typedef_struct3_T*/

#ifndef typedef_struct4_T
#define typedef_struct4_T

typedef struct {
  real_T E_Na;
  real_T gNa;
  real_T density;
  real_T Kins[12];
} struct4_T;

#endif                                 /*typedef_struct4_T*/

#ifndef typedef_struct5_T
#define typedef_struct5_T

typedef struct {
  real_T areaCoef;
} struct5_T;

#endif                                 /*typedef_struct5_T*/

#ifndef typedef_struct1_T
#define typedef_struct1_T

typedef struct {
  struct2_T IntNode;
  struct3_T Kf;
  struct3_T Ks;
  real_T LtoD;
  struct4_T Naf;
  struct2_T Node;
  struct5_T Terminal;
  real_T Vmax;
  real_T Vrest;
  real_T Vthresh;
  real_T constrict;
  real_T dt;
  real_T elecR;
  real_T intSegs;
  real_T nLen;
  real_T normdtoD;
  real_T resMed;
} struct1_T;

#endif                                 /*typedef_struct1_T*/

#ifndef typedef_struct6_T
#define typedef_struct6_T

typedef struct {
  real_T Vsample;
  real_T XstimLoc;
  real_T eCAPxLoc;
  real_T eCAPzLoc;
  real_T maxSpikes;
  real_T maxVoltage;
  real_T meanNodeAbove;
  real_T meanRecordNode;
  real_T nodeInt;
  real_T numMonte;
  real_T posRecord;
  real_T recECAP;
  real_T recV;
  real_T seed;
} struct6_T;

#endif                                 /*typedef_struct6_T*/
#endif

/* End of code generation (stochAN_multi_types.h) */
