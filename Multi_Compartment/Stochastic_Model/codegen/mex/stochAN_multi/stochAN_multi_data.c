/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * stochAN_multi_data.c
 *
 * Code generation for function 'stochAN_multi_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "stochAN_multi_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar = NULL;
boolean_T isMexOutdated;
const char * stochAN_multi_complete_name;
const char * trans_rates_Naf_complete_name;
const char * trans_rates_K_complete_name;
const char * c_initialize_Naf_channels_compl;
const char * c_initialize_Kf_channels_comple;
const char * c_initialize_Ks_channels_comple;
const char * c_update_Naf_channels_complete_;
const char * c_update_Kf_channels_complete_n;
const char * c_update_Ks_channels_complete_n;
const char * tridag_complete_name;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131467U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "stochAN_multi",                     /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

emlrtRSInfo ib_emlrtRSI = { 49,        /* lineNo */
  "power",                             /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/ops/power.m"/* pathName */
};

emlrtRSInfo mb_emlrtRSI = { 21,        /* lineNo */
  "eml_int_forloop_overflow_check",    /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"/* pathName */
};

emlrtRSInfo pb_emlrtRSI = { 31,        /* lineNo */
  "applyScalarFunctionInPlace",        /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/eml/+coder/+internal/applyScalarFunctionInPlace.m"/* pathName */
};

emlrtRSInfo xb_emlrtRSI = { 49,        /* lineNo */
  "xdot",                              /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/eml/+coder/+internal/+blas/xdot.m"/* pathName */
};

emlrtRSInfo oc_emlrtRSI = { 13,        /* lineNo */
  "log",                               /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elfun/log.m"/* pathName */
};

emlrtRTEInfo ib_emlrtRTEI = { 50,      /* lineNo */
  9,                                   /* colNo */
  "eml_mtimes_helper",                 /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"/* pName */
};

emlrtRTEInfo re_emlrtRTEI = { 46,      /* lineNo */
  19,                                  /* colNo */
  "allOrAny",                          /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/eml/+coder/+internal/allOrAny.m"/* pName */
};

/* End of code generation (stochAN_multi_data.c) */
