/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * stochAN_multi_data.h
 *
 * Code generation for function 'stochAN_multi_data'
 *
 */

#ifndef STOCHAN_MULTI_DATA_H
#define STOCHAN_MULTI_DATA_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern const volatile char_T *emlrtBreakCheckR2012bFlagVar;
extern boolean_T isMexOutdated;
extern const char * stochAN_multi_complete_name;
extern const char * trans_rates_Naf_complete_name;
extern const char * trans_rates_K_complete_name;
extern const char * c_initialize_Naf_channels_compl;
extern const char * c_initialize_Kf_channels_comple;
extern const char * c_initialize_Ks_channels_comple;
extern const char * c_update_Naf_channels_complete_;
extern const char * c_update_Kf_channels_complete_n;
extern const char * c_update_Ks_channels_complete_n;
extern const char * tridag_complete_name;
extern emlrtContext emlrtContextGlobal;
extern emlrtRSInfo ib_emlrtRSI;
extern emlrtRSInfo mb_emlrtRSI;
extern emlrtRSInfo pb_emlrtRSI;
extern emlrtRSInfo xb_emlrtRSI;
extern emlrtRSInfo oc_emlrtRSI;
extern emlrtRTEInfo ib_emlrtRTEI;
extern emlrtRTEInfo re_emlrtRTEI;

#endif

/* End of code generation (stochAN_multi_data.h) */
