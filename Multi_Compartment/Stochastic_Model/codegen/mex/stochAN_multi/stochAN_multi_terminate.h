/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * stochAN_multi_terminate.h
 *
 * Code generation for function 'stochAN_multi_terminate'
 *
 */

#ifndef STOCHAN_MULTI_TERMINATE_H
#define STOCHAN_MULTI_TERMINATE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
extern void stochAN_multi_atexit(void);
extern void stochAN_multi_terminate(void);

#endif

/* End of code generation (stochAN_multi_terminate.h) */
