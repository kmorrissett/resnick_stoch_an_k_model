/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * rdivide_helper.c
 *
 * Code generation for function 'rdivide_helper'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "rdivide_helper.h"
#include "stochAN_multi_emxutil.h"

/* Variable Definitions */
static emlrtRTEInfo ed_emlrtRTEI = { 28,/* lineNo */
  1,                                   /* colNo */
  "rdivide_helper",                    /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/eml/+coder/+internal/rdivide_helper.m"/* pName */
};

static emlrtRTEInfo ne_emlrtRTEI = { 13,/* lineNo */
  15,                                  /* colNo */
  "rdivide_helper",                    /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/eml/+coder/+internal/rdivide_helper.m"/* pName */
};

/* Function Definitions */
void b_rdivide_helper(const emlrtStack *sp, const emxArray_real_T *x, const
                      emxArray_real_T *y, emxArray_real_T *z)
{
  uint32_T varargin_1[2];
  uint32_T varargin_2[2];
  boolean_T p;
  boolean_T b_p;
  int32_T k;
  boolean_T exitg1;
  int32_T loop_ub;
  varargin_1[0] = (uint32_T)x->size[0];
  varargin_2[0] = (uint32_T)y->size[0];
  varargin_1[1] = (uint32_T)x->size[1];
  varargin_2[1] = (uint32_T)y->size[1];
  p = false;
  b_p = true;
  k = 0;
  exitg1 = false;
  while ((!exitg1) && (k < 2)) {
    if ((int32_T)varargin_1[k] != (int32_T)varargin_2[k]) {
      b_p = false;
      exitg1 = true;
    } else {
      k++;
    }
  }

  if (b_p) {
    p = true;
  }

  if (!p) {
    emlrtErrorWithMessageIdR2018a(sp, &ne_emlrtRTEI, "MATLAB:dimagree",
      "MATLAB:dimagree", 0);
  }

  k = z->size[0] * z->size[1];
  z->size[0] = 1;
  z->size[1] = x->size[1];
  emxEnsureCapacity_real_T(sp, z, k, &ed_emlrtRTEI);
  loop_ub = x->size[0] * x->size[1];
  for (k = 0; k < loop_ub; k++) {
    z->data[k] = x->data[k] / y->data[k];
  }
}

void c_rdivide_helper(const emlrtStack *sp, const emxArray_real_T *y,
                      emxArray_real_T *z)
{
  int32_T i11;
  int32_T loop_ub;
  i11 = z->size[0] * z->size[1];
  z->size[0] = 1;
  z->size[1] = y->size[1];
  emxEnsureCapacity_real_T(sp, z, i11, &ed_emlrtRTEI);
  loop_ub = y->size[0] * y->size[1];
  for (i11 = 0; i11 < loop_ub; i11++) {
    z->data[i11] = 1.0 / y->data[i11];
  }
}

void rdivide_helper(const emlrtStack *sp, real_T x, const emxArray_real_T *y,
                    emxArray_real_T *z)
{
  int32_T i9;
  int32_T loop_ub;
  i9 = z->size[0] * z->size[1];
  z->size[0] = 1;
  z->size[1] = y->size[1];
  emxEnsureCapacity_real_T(sp, z, i9, &ed_emlrtRTEI);
  loop_ub = y->size[0] * y->size[1];
  for (i9 = 0; i9 < loop_ub; i9++) {
    z->data[i9] = x / y->data[i9];
  }
}

/* End of code generation (rdivide_helper.c) */
