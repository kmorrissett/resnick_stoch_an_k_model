/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * error1.h
 *
 * Code generation for function 'error1'
 *
 */

#ifndef ERROR1_H
#define ERROR1_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
extern void e_error(const emlrtStack *sp, const real_T varargin_2_data[], const
                    int32_T varargin_2_size[2]);
extern void f_error(const emlrtStack *sp, real_T varargin_2, const real_T
                    varargin_3_data[], const int32_T varargin_3_size[2], const
                    real_T varargin_4_data[], const int32_T varargin_4_size[2]);

#endif

/* End of code generation (error1.h) */
