/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sqrt.c
 *
 * Code generation for function 'sqrt'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "sqrt.h"
#include "eml_int_forloop_overflow_check.h"
#include "error.h"
#include "stochAN_multi_data.h"

/* Variable Definitions */
static emlrtRSInfo nb_emlrtRSI = { 12, /* lineNo */
  "sqrt",                              /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elfun/sqrt.m"/* pathName */
};

static emlrtRSInfo ob_emlrtRSI = { 15, /* lineNo */
  "sqrt",                              /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elfun/sqrt.m"/* pathName */
};

/* Function Definitions */
void b_sqrt(const emlrtStack *sp, emxArray_real_T *x)
{
  boolean_T overflow;
  int32_T nx;
  int32_T k;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  overflow = false;
  nx = x->size[1];
  for (k = 0; k < nx; k++) {
    if (overflow || (x->data[k] < 0.0)) {
      overflow = true;
    } else {
      overflow = false;
    }
  }

  if (overflow) {
    st.site = &nb_emlrtRSI;
    error(&st);
  }

  st.site = &ob_emlrtRSI;
  nx = x->size[1];
  b_st.site = &pb_emlrtRSI;
  overflow = ((1 <= x->size[1]) && (x->size[1] > 2147483646));
  if (overflow) {
    c_st.site = &mb_emlrtRSI;
    check_forloop_overflow_error(&c_st);
  }

  for (k = 0; k < nx; k++) {
    x->data[k] = muDoubleScalarSqrt(x->data[k]);
  }
}

/* End of code generation (sqrt.c) */
