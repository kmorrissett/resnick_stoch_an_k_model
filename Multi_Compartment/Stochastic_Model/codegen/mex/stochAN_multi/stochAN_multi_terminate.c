/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * stochAN_multi_terminate.c
 *
 * Code generation for function 'stochAN_multi_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "stochAN_multi_terminate.h"
#include "_coder_stochAN_multi_mex.h"
#include "stochAN_multi_data.h"

/* Function Definitions */
void stochAN_multi_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtProfilerUnregisterMEXFcn(stochAN_multi_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(trans_rates_Naf_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(trans_rates_K_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(c_initialize_Naf_channels_compl, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(c_initialize_Kf_channels_comple, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(c_initialize_Ks_channels_comple, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(c_update_Naf_channels_complete_, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(c_update_Kf_channels_complete_n, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(c_update_Ks_channels_complete_n, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(tridag_complete_name, isMexOutdated);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

void stochAN_multi_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (stochAN_multi_terminate.c) */
