/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * repmat.c
 *
 * Code generation for function 'repmat'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "stochAN_multi.h"
#include "repmat.h"
#include "stochAN_multi_emxutil.h"

/* Variable Definitions */
static emlrtRSInfo yb_emlrtRSI = { 27, /* lineNo */
  "repmat",                            /* fcnName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/repmat.m"/* pathName */
};

static emlrtRTEInfo fd_emlrtRTEI = { 52,/* lineNo */
  9,                                   /* colNo */
  "repmat",                            /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/repmat.m"/* pName */
};

static emlrtDCInfo t_emlrtDCI = { 30,  /* lineNo */
  14,                                  /* colNo */
  "repmat",                            /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/lib/matlab/elmat/repmat.m",/* pName */
  4                                    /* checkKind */
};

static emlrtRTEInfo oe_emlrtRTEI = { 55,/* lineNo */
  23,                                  /* colNo */
  "assertValidSizeArg",                /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/eml/+coder/+internal/assertValidSizeArg.m"/* pName */
};

static emlrtRTEInfo pe_emlrtRTEI = { 61,/* lineNo */
  15,                                  /* colNo */
  "assertValidSizeArg",                /* fName */
  "/Applications/MATLAB_R2018b.app/toolbox/eml/eml/+coder/+internal/assertValidSizeArg.m"/* pName */
};

/* Function Definitions */
void repmat(const emlrtStack *sp, real_T a, real_T varargin_1, emxArray_real_T
            *b)
{
  real_T b_varargin_1;
  int32_T i10;
  int32_T loop_ub;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &yb_emlrtRSI;
  if ((varargin_1 != muDoubleScalarFloor(varargin_1)) || muDoubleScalarIsInf
      (varargin_1) || (varargin_1 < -2.147483648E+9) || (varargin_1 >
       2.147483647E+9)) {
    emlrtErrorWithMessageIdR2018a(&st, &oe_emlrtRTEI,
      "Coder:MATLAB:NonIntegerInput", "Coder:MATLAB:NonIntegerInput", 4, 12,
      MIN_int32_T, 12, MAX_int32_T);
  }

  if (varargin_1 <= 0.0) {
    b_varargin_1 = 0.0;
  } else {
    b_varargin_1 = varargin_1;
  }

  if (!(b_varargin_1 <= 2.147483647E+9)) {
    emlrtErrorWithMessageIdR2018a(&st, &pe_emlrtRTEI, "Coder:MATLAB:pmaxsize",
      "Coder:MATLAB:pmaxsize", 0);
  }

  if (!(varargin_1 >= 0.0)) {
    emlrtNonNegativeCheckR2012b(varargin_1, &t_emlrtDCI, sp);
  }

  i10 = b->size[0];
  loop_ub = (int32_T)varargin_1;
  b->size[0] = loop_ub;
  emxEnsureCapacity_real_T(sp, b, i10, &fd_emlrtRTEI);
  for (i10 = 0; i10 < loop_ub; i10++) {
    b->data[i10] = a;
  }
}

/* End of code generation (repmat.c) */
