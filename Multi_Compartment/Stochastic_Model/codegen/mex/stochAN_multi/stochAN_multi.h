/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * stochAN_multi.h
 *
 * Code generation for function 'stochAN_multi'
 *
 */

#ifndef STOCHAN_MULTI_H
#define STOCHAN_MULTI_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "stochAN_multi_types.h"

/* Function Declarations */
extern void stochAN_multi(const emlrtStack *sp, const emxArray_real_T *Istim,
  const struct0_T *segments, const struct1_T *model, const struct6_T *options,
  emxArray_real_T *varargout_1, emxArray_real32_T *varargout_2,
  emxArray_real32_T *varargout_3);

#endif

/* End of code generation (stochAN_multi.h) */
