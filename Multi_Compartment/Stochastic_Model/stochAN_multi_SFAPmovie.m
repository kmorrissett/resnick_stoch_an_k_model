function varargout = stochAN_multi_SFAPmovie(Istim,segments,model,options)

% This function simulates a neural fiber stimulated by a disc electrode
% using stochastic Hodgkin and Huxley dynamics modeled via Jump Markov
% Process. V4 allows expansion of the terminal compartments to provide low
% impedence return to ground. It requires the same inputs as V1.
%
% USAGE: vOutput = stochAN_multiV3(Istim,segments,model,options)
%
% spikeOutput:  2D matrix of spike times (# segments x # of spikes)
% vOutput:  2D matrix of fiber voltages (# segments x # of samples)
%
% Istim:    array of stimulus currents segments: structure containing
% segment electrical/structural properties model:    structure containing
% HH model parameters options:  structure containing experimental options
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

rng(options.seed); % Set the rng seed using clock time.

% Extract values from passed structures.
Vrest   = model.Vrest;
Vthresh = model.Vthresh;
NafKins = model.Naf.Kins;
gNaf    = model.Naf.gNa;
E_Na    = model.Naf.E_Na;
KfKins  = model.Kf.Kins;
gKf     = model.Kf.gK;
E_K     = model.Kf.E_K;
KsKins  = model.Ks.Kins;
gKs     = model.Ks.gK;

maxVolt = options.maxVoltage;
maxSpikes = options.maxSpikes;
recV    = options.recV;
recECAP = options.recECAP;
Vsample = options.Vsample;

nodeIDX = segments.nodeIDX;
ra = segments.ra;
rm = segments.rm;
cm = segments.cm;
xApp  = segments.xApp;
xEcap = segments.xEcap;

numNaf = segments.numNaf;
numKf = segments.numKf;
numKs = segments.numKs;

numNodes = length(nodeIDX);
numSegments = length(rm);

% Time step
dt = model.dt;
kMax = length(Istim); % Number of time steps.

% Initialize output arrays
Vm = zeros(1,numSegments);
vOutput = zeros(options.numMonte,floor((kMax-1)*dt/Vsample+1),numSegments,'single');
spikeOutput = nan(options.numMonte,numNodes,options.maxSpikes);

% Calculate field resistivity between electrode and each segment
fieldResist = model.resMed/(4*pi*model.elecR)*asin(2*model.elecR./...
    (sqrt(segments.zDistance.^2+(xApp+options.XstimLoc-model.elecR).^2)+...
    sqrt(segments.zDistance.^2+(xApp+options.XstimLoc+model.elecR).^2)));
eCAPxs = -8:0.1:2;
eCAPzs =  0.1:0.1:6;
ecapRes = nan(length(xEcap),length(eCAPxs),length(eCAPzs));
for xIDX = 1:length(eCAPxs)
    for zIDX = 1:length(eCAPzs)
        ecapRes(:,xIDX,zIDX) = model.resMed./(4*pi)./sqrt((eCAPzs(zIDX).^2+...
            (xEcap + eCAPxs(xIDX)).^2));
    end
end
eCAPout = zeros(length(eCAPxs),length(eCAPzs),options.numMonte,kMax,'single');

if recECAP
    for xIDX = 1:length(eCAPxs)
        for zIDX = 1:length(eCAPzs)
            eCAP1 = (Vm./rm)*ecapRes(:,xIDX,zIDX);
            eCAPout(xIDX,zIDX,:,1) = repmat(eCAP1,options.numMonte,1);
        end
    end
end

%% Initialize Crank-Nicholson variables
% Set boundary conditions for Crank Nicolson
a = [0 1./ra];          % sub diagonal of matrix A
b = [-(1./rm(1) + 1./ra(1) + 2.*cm(1)/dt),...
    -(1./rm(2:end-1) + 1./ra(2:end) + 1./ra(1:end-1)+...
    2 .*cm(2:end-1)/dt),...
    -(1./rm(end) + 1./ra(end) + 2.*cm(end)/dt)];  % diagonal of A
c = [1./ra 0];             % supradiagonal of A
% A = spdiags([a' b' c'],-1:1,numSegments,numSegments); % View it!

% Calculate coefficients for middle RHS term
rhsM = [(1./rm(1) + 1./ra(1) -  2.*cm(1)/dt),...
    (1./rm(2:end-1) + 1./ra(2:end) + 1./ra(1:end-1)-...
    2 .*cm(2:end-1)/dt),...
    (1./rm(end) + 1./ra(end) - 2.*cm(end)/dt) ];

% ecap = nan(1,k_max);

Naf = zeros(numSegments,8);
Kf = zeros(numSegments,5);
Ks = zeros(numSegments,2);

for monte = 1:options.numMonte
    % Variables initialization
    numSpikes = zeros(1,numSegments);
    Vm = zeros(1,numSegments);
    rhs = zeros(1,numSegments);
    dist = zeros(1,numSegments);
    
    % Set rates to resting values
    ratesNaf    = trans_rates_Naf(ones(1,numSegments)*Vrest,NafKins);
    ratesKf     = trans_rates_K(ones(1,numSegments)*Vrest,KfKins);
    ratesKs     = trans_rates_K(ones(1,numSegments)*Vrest,KsKins);
    
    for IDX = 1:length(nodeIDX)
        segIDX = nodeIDX(IDX);
        % Initialize channel states using jump Monte Carlo.
        Naf(segIDX,:)   = initialize_Naf_channels(ratesNaf(:,segIDX),numNaf(segIDX),dt);
        Kf(segIDX,:)    = initialize_Kf_channels(ratesKf(:,segIDX),numKf(segIDX),dt);
        Ks(segIDX,:)    = initialize_Ks_channels(ratesKs(:,segIDX),numKs(segIDX),dt);
    end
    
    for it = 2:kMax
        Vapp = Istim(it)*fieldResist; % Calculate applied voltages
        Vprev = Vm;
        
        % update transition rates
        ratesNaf    = trans_rates_Naf(Vm + Vrest,NafKins);
        ratesKf     = trans_rates_K(Vm + Vrest,KfKins);
        ratesKs     = trans_rates_K(Vm + Vrest,KsKins);
        
        for IDX = 1:length(nodeIDX)
            segIDX = nodeIDX(IDX);
            % Update channel states using jump Markov Process
            Naf(segIDX,:)   = update_Naf_channels(ratesNaf(:,segIDX),Naf(segIDX,:),dt);
            Kf(segIDX,:)    = update_Kf_channels(ratesKf(:,segIDX),Kf(segIDX,:),dt);
            Ks(segIDX,:)    = update_Ks_channels(ratesKs(:,segIDX),Ks(segIDX,:),dt);
        end
        openNaf  = Naf(:,end).';
        openKf   = Kf(:,end).';
        openKs   = Ks(:,end).';
        
        iNaf = gNaf*openNaf.*((Vm + Vrest)-E_Na);
        iKf  = gKf*openKf.*((Vm + Vrest)-E_K );
        iKs  = gKs*openKs.*((Vm + Vrest)-E_K );
        Iion = iNaf + iKf + iKs;
        
        % Set rhs for Crank-Nicolson
        % RHS for first node segment.
        dist(1) = c(1)*(Vapp(2)-Vapp(1));
        rhs(1) = rhsM(1)*Vm(1) - c(1)*Vm(2)...
            - 2* dist(1) + 2* Iion(1);
        
        %RHS for segments 2:end-1.
        dist(2:numSegments-1) = c(2:end-1).*(Vapp(3:numSegments)-Vapp(2:numSegments-1))...
            - a(2:end-1).*(Vapp(2:numSegments-1)-Vapp(1:numSegments-2));
        rhs(2:numSegments-1) = -a(2:end-1).*Vm(1:end-2)...
            + rhsM(2:end-1).*Vm(2:end-1) - c(2:end-1).*Vm(3:end)...
            - 2*dist(2:end-1) + 2.0*Iion(2:end-1);
        
        % Axonal potential flux through segment i.
        dist(end) = -a(end).*(Vapp(numSegments) - Vapp(numSegments-1));
        rhs(end) = -a(end) .* Vm(end-1)...
            + rhsM(end) .* Vm(end) ...
            - 2 * dist(end) - 2.0*Iion(end);
        
        % Solve tridiagonal matrix
        Vm = tridag(a,b,c,rhs);
        
        % Record spike times
        spiked = find((Vm(nodeIDX) > Vthresh) & (Vprev(nodeIDX) < Vthresh));
        if any(spiked)
            numSpikes(spiked) = numSpikes(spiked) + 1;
            if any(numSpikes > maxSpikes)
                error('Segments %d exceeded maxSpikes.',find(numSpikes>maxSpikes,1));
            end
            for i = 1:length(spiked)
                spikeIDX = spiked(i);
                spikeOutput(monte,spikeIDX,numSpikes(spikeIDX)) = it;
            end
        end
        
        % Record voltage
        if recV
            if mod((it-1)*dt,Vsample) == 0
                vOutput(monte,round((it-1)*dt/Vsample+1),:) = Vm;
            end
        end
        
        if recECAP
            Irm = Vm./rm;
            Icm = cm.*(Vm-Vprev)/dt;
            Im = Iion + Irm + Icm;
            for xIDX = 1:length(eCAPxs)
                for zIDX = 1:length(eCAPzs)
                    eCAPout(xIDX,zIDX,monte,it) = Im*ecapRes(:,xIDX,zIDX);
                end
            end
        end
        
        if any(Vm > maxVolt)
            badIDX = find(Vm > maxVolt,1);
            error('Time is %0.0f, Vm[%d] is out of range: %f\n',it,badIDX,Vm(badIDX))
        end
    end
end

varargout{1} = spikeOutput;
varargout{2} = vOutput;
varargout{3} = eCAPout;
end