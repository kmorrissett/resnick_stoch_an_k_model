function [spikeOutput,vOutput] = Fibers_Run(Fibers,stimList,model,options,myFibs)
%Fibers_RUN: This function accepts a structure containing fiber
%population parameters (Fibers), a cell array containing seperate electrode
%current vectors for each fiber, and experiment model and options structures. It then runs
%all of the individual fibers in the population through stochAN_multi using
%the supplied Istim, model, and options.
%
% USAGE: [spikeOutput,vOutput] = Population_Run(Fibers,stimList,model,options);
%
% spikeOutput:  Cell array of arrays of spiketimes for each fiber segment.
% vOutput:      Cell array of arrays of voltages for each fiber segment for
%               each time step. Only generated when options.recV = 1.
% Fibers:       Structure containing arrays of geometric parameters for
%               individual fibers.
% Istim:        Array of stimulus currents for each timestep.
% model:        Structure containing model parameters.
% options:      Structure containing simulation optional arguments.
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

%% Set non-changing fiber parameters
options.XstimLoc = -(model.LtoD*Fibers.diamMean*1e-3/...
    model.normdtoD+model.nLen)*options.meanNodeAbove;

%% Unpack Fibers for parfor loop
if ~exist('myFibs','var'); myFibs = 1:length(Fibers.diameters); end
numFibers = length(myFibs);

diameterList = Fibers.diameters(myFibs);
dtoDList = Fibers.dtoD(myFibs);
numNodeList = Fibers.numNodes(myFibs);
zsList = Fibers.zs(myFibs);
intSegsList = Fibers.intSegs(myFibs);

%% Initialize output arrays
if options.recV; vOutput = cell(numFibers,1); end
spikeOutput = cell(numFibers,1);

model = orderfields(model);
options = orderfields(options);

% tic;
%% Run simulation for each fiber in loop
parfor fibIDX = 1:numFibers
    diameter = diameterList(fibIDX);  % axolemma diameter (um)
    dtoD = dtoDList(fibIDX);   % g-ratio for healthy reg.
    numNodes = numNodeList(fibIDX);   % nodes in fiber
    zDistance = zsList(fibIDX);        % distance b/w fiber & electrode (mm)
    intSegs = intSegsList(fibIDX);
    
    fiber = struct('diameter',diameter,'dtoD',dtoD,'numNodes',numNodes,...
        'zDistance',zDistance,'intSegs',intSegs);
    % fiber.cutoff = [];            % PLACEHOLDER: demyelination extent
    % fiber.demydtoD = [];          % PLACEHOLDER: demyelination severity
    
    %% Calculate segment properties from fiber and model info
    segments = fiber_initialization(fiber,model);
    
    %% Run model
    % Need consistent field order for MEX function.
    segments = orderfields(segments);
    
    Istim = stimList{fibIDX};
    if options.recV %#ok<PFBNS>
        [spikeOutput{fibIDX},vOutput{fibIDX}] = stochAN_multi_mex(Istim,segments,model,options);
    else
        spikeOutput{fibIDX} = stochAN_multi_mex(Istim,segments,model,options);
    end
end
% toc;
end

