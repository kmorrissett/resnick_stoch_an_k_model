function numKf = update_Kf_channels(ratesKf,numKf,dt)
% NUMKF: This function updates the number of channels in each state for the
% next timestep using a Jump Monte Carlo method.
%
% Usage: numKf = initialize_Kf_channels(ratesKf,numKf,dt)
%
%   numKf:      Array of # of channels in each state
%   ratesKf:    Voltage dependent transition rates 
%   dt:         Timestep (us)
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

zeta = [
    4*ratesKf(1);...
    3*ratesKf(1) + ratesKf(2);...
    2*ratesKf(1) + 2*ratesKf(2);...
    ratesKf(1) + 3*ratesKf(2);...
    4*ratesKf(2)];

Tl = 0;

p_trans_curr = [0,1,2,2,3,3,4,4,5];
p_trans_next = [0,2,1,3,2,4,3,5,4];

while   (Tl<=dt)
    
    lambda = numKf*zeta;
    
    tl = -log(rand) / lambda;
    Tl = Tl + tl;
    
    if (Tl>dt)
        break;
    end
    
    P = cumsum([0,...
        4*ratesKf(1)*numKf(1),...
        ratesKf(2)*numKf(2),...
        3*ratesKf(1)*numKf(2),...
        2*ratesKf(2)*numKf(3),...
        2*ratesKf(1)*numKf(3),...
        3*ratesKf(2)*numKf(4),...
        ratesKf(1)*numKf(4),...
        4*ratesKf(2)*numKf(5)...
        ]/lambda);
    
    ind = find(rand<P,1);
    
    numKf(p_trans_curr(ind)) = numKf(p_trans_curr(ind)) - 1;
    numKf(p_trans_next(ind)) = numKf(p_trans_next(ind)) + 1;
end

