function vOutput = deterAN_multi(Istim,segments,model,options)

% This function simulates a neural fiber stimulated by a disc electrode
% using deterministic approximations of Hodgkin and Huxley dynamics.
%
% USAGE: vOutput = deterAN_multi(Istim,segments,model,options)
%
% vOutput:  2D matrix of fiber voltages (# segments x # of samples)
% Istim:    array of stimulus currents
% segment:    structure containing segment properties
% model:    structure containing HH model parameters
% options:  structure containing experimental options
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

rng(options.seed); % Set the rng seed using clock time.

% Extract values from structures.
Vrest   = model.Vrest;
NafKins = model.Naf.Kins;
gNaf    = model.Naf.gNa;
E_Na    = model.Naf.E_Na;
KfKins  = model.Kf.Kins;
gKf     = model.Kf.gK;
E_K     = model.Kf.E_K;
KsKins  = model.Ks.Kins;
gKs     = model.Ks.gK;

maxVolt = options.maxVoltage;
maxSpikes = options.maxSpikes;
recV    = options.recV;
Vsample = options.Vsample;

nodeIDX = segments.nodeIDX;
ra = segments.ra;
rm = segments.rm;
cm = segments.cm;
x  = segments.x;

numNaf = segments.numNaf;
numKf = segments.numKf;
numKs = segments.numKs;

numNodes = length(nodeIDX);
numSegments = numNodes*(1+model.intSegs);

% Time step
dt = model.dt;
kMax = length(Istim); % Number of time steps.

% Calculate field resistivity between electrode and each segment
fieldResist = model.resMed/(4*pi*model.elecR)*asin(2*model.elecR./...
    (sqrt(segments.zDistance^2+(x+options.XstimLoc-model.elecR).^2)+...
    sqrt(segments.zDistance^2+(x+options.XstimLoc+model.elecR).^2)));
% ecapRes = resmed/(4*pi)/sqrt(ECAPheight^2+(ECAPaway-(x(1:end-1)+x(2:end))/2)^2);

%% Initialize Crank-Nicholson variables
% Set boundary conditions for Crank Nicolson
a = [0 ra(2:end)./ra(1:end-1)];          % sub diagonal of matrix A
b = [-(1.0 + ra(1)./rm(1)+2*ra(1).*cm(1)/dt),...
    -(1 + ra(2:end)./ra(1:end-1)+ra(2:end)./rm(2:end) +...
    2 *ra(2:end).*cm(2:end)/dt)];          % diagonal of matrix A
c = [ones(1,numSegments-1) 0];             % supradiagonal of matrix A
% A = spdiags([a' b' c'],-1:1,numSegments,numSegments);
dx1 = a;
dx2 = 1+dx1; dx2(1) = 0;
ux = [0 dx2(2:end) + ra(2:end)./rm(2:end) - 2 * ra(2:end) .* cm(2:end) /dt];
ux(1) = 0;

% Initialize output arrays.
vOutput = zeros((kMax-1)*dt/Vsample+1,numSegments+1,'single');
spikeOutput = nan(options.numMonte,numNodes,options.maxSpikes);

% ecap = nan(1,k_max);

% Variables initialization
Vm = zeros(1,numSegments+1);
rhs = zeros(1,numSegments);
dist = zeros(1,numSegments);

m = zeros(kMax,numSegments);
h = zeros(kMax,numSegments);
n = zeros(kMax,numSegments);
s = zeros(kMax,numSegments);

% Set gating parameters to resting values
m(1,:) = 0.0795;
h(1,:) = 0.7398;
n(1,:) = 0.0795;
s(1,:) = 0.9271;

for it = 2:kMax
    Vapp = Istim(it)*fieldResist; % Calculate applied voltages
    Vm(1) = 0;
    
    % update transition rates
    ratesNaf    = trans_rates_Naf(Vm(1:numSegments) + Vrest,NafKins);
    ratesKf     = trans_rates_K(Vm(1:numSegments) + Vrest,KfKins);
    ratesKs     = trans_rates_K(Vm(1:numSegments) + Vrest,KsKins);
    
    %differential equations for m,n and h
    m(it,:) = m(it-1,:)+ dt*(ratesNaf(1,:).*(1-m(it-1,:))-ratesNaf(2,:).*m(it-1,:));
    h(it,:) = h(it-1,:)+ dt*(ratesNaf(3,:).*(1-h(it-1,:))-ratesNaf(4,:).*h(it-1,:));
    
    n(it,:) = n(it-1,:)+ dt*(ratesKf(1,:).*(1-n(it-1,:))-ratesKf(2,:).*n(it-1,:));
    s(it,:) = s(it-1,:)+ dt*(ratesKs(1,:).*(1-s(it-1,:))-ratesKs(2,:).*s(it-1,:));
    
    openNaf = numNaf.*m(it,:).^3.*h(it,:);
    openKf  = numKf.*n(it,:).^4;
    openKs  = numKs.*s(it,:);
    
    iNaf = gNaf*openNaf.*((Vm(1:end-1)+ Vrest)-E_Na);
    iKf  = gKf*openKf.*((Vm(1:end-1)+ Vrest)-E_K );
    iKs  = gKs*openKs.*((Vm(1:end-1)+ Vrest)-E_K );
    Iion = iNaf + iKf + iKs;
    
    % Set rhs for Crank-Nicolson
    % Calculate RHS for first node segment.
    dist(1) = Vapp(2)-Vapp(1);
    rhs(1) = (1.0 + ra(1) / rm(1) - 2.0 * ra(1) * cm(1) / dt)...
        * Vm(1) - Vm(2) - 2* dist(1) + 2*ra(1) * Iion(1);
    
    %RHS for segments 2:end. xina is 0 for internodes.
    dist(2:numSegments) = dx1(2:end) .* Vapp(1:end-2) - dx2(2:end) .* Vapp(2:end-1)...
        + Vapp(3:end); % Axonal potential flux through segment i.
    rhs(2:numSegments) = -dx1(2:end) .* Vm(1:end-2) + ux(2:end) .* Vm(2:end-1)...
        - Vm(3:end) - 2 * dist(2:end) + 2.0 * ra(2:end) .* Iion(2:end);
    
    % Solve tridiagonal matrix
    Vm = tridag(a,b,c,rhs);
    
 % Record spike times
        spiked = (Vm(nodeIDX) > Vthresh) & (Vprev(nodeIDX) < Vthresh);
        if any(spiked)
            numSpikes(spiked) = numSpikes(spiked) + 1;
            if all(numSpikes < maxSpikes)
                spikeOutput(monte,spiked,numSpikes(spiked)) = it;
            end
        end
        
        % Record voltage
        if recV
            if mod((it-1)*dt,Vsample) == 0
                vOutput(monte,round((it-1)*dt/Vsample+1),:) = Vm;
            end
        end
        
        if any(Vm > maxVolt)
            badIDX = find(Vm(it,:) > maxVolt,1);
            error('Time is %0.0f, Vm[%02d] is out of range: %0.3f\n',it,badIDX,Vm(it,badIDX))
        end
end
end