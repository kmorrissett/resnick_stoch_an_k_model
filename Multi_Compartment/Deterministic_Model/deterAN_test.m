function vOutput = deterAN_test(I0)
%% Set model properties
model.dt = 1e-3;              % sim timestep in ms
model.Vrest =-84;             % resting membrane potential for Na and Kir equ. (mV)
% model.Vrest2 =-63.6;        % resting membrane potential for h and Klt equ. (mV)
model.Vmax = 1500;            % maximum voltage (mV)
model.Vthresh = 50;           % threshold voltage (mV)

model.elecR = 0.001;          % electrode radius (mm)
model.resMed = 25e3;            % resistivity of medium (ohm*mm)

% Fast sodium channel
Naf.E_Na = 40;           % reversal potenial for sodium in mV
Naf.gNa = 20e-12;      % single channel conductance for sodium in mS
Naf.density = 618e6;

Kins.a_m_A = 6.57;  Kins.a_m_B = -27.4;  Kins.a_m_C = 10.3; 
Kins.b_m_A = 0.304; Kins.b_m_B = -25.7;  Kins.b_m_C = 9.16;

Kins.a_h_A = 0.34;  Kins.a_h_B = -114;   Kins.a_h_C = 11.0;
Kins.b_h_A = 12.6;  Kins.b_h_B = -31.8;  Kins.b_h_C = 13.4;

Naf.Kins = cell2mat(struct2cell(Kins));
clear Kins;
model.Naf = Naf;
% % Slow sodium channel
% model.Nap.E_Na = model.Naf.E_Nap;           % reverse potenial for sodium in mV
% model.Nap.gNa = 20e-12;      % single channel conductance for sodium in mS
% model.Na.density = 618e6;
% 
% model.Nap.Kins.a_m_A = 0;    model.Nap.Kins.b_m_A = 0.304;
% model.Nap.Kins.a_m_B = 0;   model.Nap.Kins.b_m_B = -25.7;  
% model.Nap.Kins.a_m_C = 0;     model.Nap.Kins.b_m_C = 9.16;

% Fast potassium channel
Kf.E_K = -84;              % " potassium "
Kf.gK = 10e-12;           % single channel conductance for Kir in mS
Kf.density = 2.0371833e7;

Kins.a_A = 0.0462; Kins.a_B = -93.2;  Kins.a_C = 1.10;  
Kins.b_A = 0.0824; Kins.b_B = -76.0;  Kins.b_C = 10.5;
Kf.Kins = cell2mat(struct2cell(Kins));
clear Kins;
model.Kf = Kf;

% Slow potassium channel
Ks.E_K = -84;              % " potassium "
Ks.gK = 10e-12;           % single channel conductance for Kir in mS
Ks.density = 4.1160079e7;

Kins.a_A = 0.3;      Kins.a_B = -12.5;    Kins.a_C = 23.6;
Kins.b_A = 0.0036;   Kins.b_B = -80.1;    Kins.b_C = 21.8;
Ks.Kins = cell2mat(struct2cell(Kins));
clear Kins;
model.Ks = Ks;

% % Cyclic nucleotide gated cation channel.
% model.HCN.E_h = -43;              % " hyperpolarization-activated cation "
% model.HCN.gh = 13e-9;             % single channel conductance for HCN in mS
% model.HCN.N_h_max = 100;
% 
% % Slow rectifying potassium channel
% model.KLT.E_K = -88;              % " potassium "
% model.KLT.glt = 13e-9;            % single channel conductance for KLT in mS
% model.KLT.N_lt_max = 166;

%% Set fiber properties
diamMean = 1.477;
fiber.diameter = diamMean;        % axolemma diameter (um)
fiber.nLen = 0.001;          % length of internode (mm)
fiber.dtoD = 0.643;               % d/D
% fiber.cutoff = [];            % PLACEHOLDER: demyelination extent
% fiber.demydtoD = [];          % PLACEHOLDER: demyelination severity

fiber.constrict = 0.5;    % Constriction factor at node
fiber.LtoD = 92;                % internode length/diameter
fiber.numNodes = 36;
fiber.intSegs = 9;          % num internode segments per node
fiber.zDistance = 3;            % distance of fiber from electrode center (mm)

fiber.Node.rm = 10*831;         % node specific membrane resistance (Ohm-mm^2)
fiber.Node.cm = 0.5*0.041e-3;   % node specific membrane capacitance (F/mm^2)
fiber.Node.ra = 0.69*1063;      % node specific axonal resistance (Ohm-mm)

fiber.IntNode.rm = 6*209e6;     % internode specific membrane capacitance (Ohm-mm)
fiber.IntNode.cm = 1/11*1.6e-9; % internode specific membrane resistance
fiber.IntNode.ra = 0.69*1063;	% internode specific axonal resistance (Ohm-mm)  

%% Set options
options.seed = sum(100*clock);
options.numMonte = 1;
options.maxSpikes = 5;
options.maxVoltage = 1500;
options.Vsample = 0.001; % Voltage sampling period (us)
options.recV = 1;               % record voltage flag
options.meanNodeAbove = 10;
options.XstimLoc = -(fiber.LtoD*diamMean*1e-3/fiber.dtoD+fiber.nLen)*options.meanNodeAbove;
options.meanRecordNode = 32;
options.posRecord = options.meanRecordNode/36;
options.nodeInt = options.posRecord;
options.Vsample = 0.01;             % voltage sampling rate
% options.eCAPxLoc = 10*(0.231);    % PLACEHOLDER: ecap recording x-location
% options.eCAPzLoc = 6;             % PLACEHOLDER: ecap recording height

%% Generate stimulus
dt = model.dt;
simtime = 3.2;                 % simulation time in ms
t = 0:dt:simtime;
k_max = length(t);
pulseonset = 1;           % pulse onset time in ms
pw = 100;                   % pulse width in us
gap = 8;                  % interphase gap in us                 % peak current intensity
Istim = zeros(1,k_max);

% Calling the stimulating current functions
Istim(round(pulseonset/dt+1):round((pulseonset+pw*1e-3)/dt+1)) = I0;
Istim(round((pulseonset+(pw+gap)*1e-3)/dt+1):round((pulseonset+(2*pw+gap)*1e-3)/dt+1)) = -I0;

%% Run model
tic;
vOutput = stochAN_multi_deter(Istim,fiber,model,options);
toc;

figure
imagesc(squeeze(vOutput-84));
c = colorbar; c.Label.String = 'Voltage (mV)';
xlabel('Segment #');
ylabel('Timestep (\mus)');

figure
subplot(2,1,1)
for i = 1:30:360
    plot(0:dt*options.Vsample/dt:simtime,vOutput(:,i)-84); hold on;
end
subplot(2,1,2)
plot(0:dt:simtime,Istim,'k','LineWidth',2)
end