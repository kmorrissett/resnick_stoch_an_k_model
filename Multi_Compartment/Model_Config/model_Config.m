function model = model_Config
%MODELCONFIG This function contains the non-chaning model parameters for
%running stochAN_multi simulations.
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

model.dt = 1e-3;              % sim timestep in ms
model.Vrest =-84;             % resting membrane potential for Na and Kir equ. (mV)
% model.Vrest2 =-63.6;        % resting membrane potential for h and Klt equ. (mV)
model.Vmax = 1500;            % maximum voltage (mV)
model.Vthresh = 50;           % threshold voltage (mV)

model.elecR = 0.001;          % electrode radius (mm)
model.resMed = 25e3;            % resistivity of medium (ohm*mm)

model.intSegs = 9;              % num internode segments per node for 1.5 um fiber.
model.constrict = 0.5;          % Constriction factor at node
model.LtoD = 92;                % internode length/diameter
model.normdtoD = 0.643;
model.nLen = 0.001;             % length of internode (mm)

model.Node.rm = 10*831;         % node specific membrane resistance (Ohm-mm^2)
model.Node.cm = 0.5*0.041e-3;   % node specific membrane capacitance (F/mm^2)
model.Node.ra = 0.69*1063;      % node specific axonal resistance (Ohm-mm)

model.IntNode.rm = 6*209e6;     % internode specific membrane capacitance (Ohm-mm)
model.IntNode.cm = 1/11*1.6e-9; % internode specific membrane resistance
model.IntNode.ra = 0.69*1063;	% internode specific axonal resistance (Ohm-mm)

model.Terminal.areaCoef = 20;   % Surface area expansion coefficient for terminals.

% Fast sodium channel
Naf.E_Na = 40;           % reversal potenial for sodium in mV
Naf.gNa = 20e-12;      % single channel conductance for sodium in mS
Naf.density = 618e6;

Kins.a_m_A = 6.57;  Kins.a_m_B = -27.4;  Kins.a_m_C = 10.3; 
Kins.b_m_A = 0.304; Kins.b_m_B = -25.7;  Kins.b_m_C = 9.16;

Kins.a_h_A = 0.34;  Kins.a_h_B = -114;   Kins.a_h_C = 11.0;
Kins.b_h_A = 12.6;  Kins.b_h_B = -31.8;  Kins.b_h_C = 13.4;

Naf.Kins = cell2mat(struct2cell(Kins));
clear Kins;
model.Naf = Naf;
% % Slow sodium channel
% model.Nap.E_Na = model.Naf.E_Nap;           % reverse potenial for sodium in mV
% model.Nap.gNa = 20e-12;      % single channel conductance for sodium in mS
% model.Na.density = 618e6;
% 
% model.Nap.Kins.a_m_A = 0;    model.Nap.Kins.b_m_A = 0.304;
% model.Nap.Kins.a_m_B = 0;   model.Nap.Kins.b_m_B = -25.7;  
% model.Nap.Kins.a_m_C = 0;     model.Nap.Kins.b_m_C = 9.16;

% Fast potassium channel
Kf.E_K = -84;              % " potassium "
Kf.gK = 10e-12;           % single channel conductance for Kir in mS
Kf.density = 2.0371833e7;

Kins.a_A = 0.0462; Kins.a_B = -93.2;  Kins.a_C = 1.10;  
Kins.b_A = 0.0824; Kins.b_B = -76.0;  Kins.b_C = 10.5;
Kf.Kins = cell2mat(struct2cell(Kins));
clear Kins;
model.Kf = Kf;

% Slow potassium channel
Ks.E_K = -84;              % " potassium "
Ks.gK = 10e-12;           % single channel conductance for Kir in mS
Ks.density = 4.1160079e7;

Kins.a_A = 0.3;      Kins.a_B = -12.5;    Kins.a_C = 23.6;
Kins.b_A = 0.0036;   Kins.b_B = -80.1;    Kins.b_C = 21.8;
Ks.Kins = cell2mat(struct2cell(Kins));
clear Kins;
model.Ks = Ks;

% % Cyclic nucleotide gated cation channel.
% model.HCN.E_h = -43;              % " hyperpolarization-activated cation "
% model.HCN.gh = 13e-9;             % single channel conductance for HCN in mS
% model.HCN.N_h_max = 100;
% 
% % Slow rectifying potassium channel
% model.KLT.E_K = -88;              % " potassium "
% model.KLT.glt = 13e-9;            % single channel conductance for KLT in mS
% model.KLT.N_lt_max = 166
end

