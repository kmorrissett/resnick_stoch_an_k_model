function segments = fiber_initialization(fiber,model)
%FIBER_INITIALIZATION: This function recieves fiber and model structures as
%inputs and returns the structure segments which contains only the
%calculated quantities necessary for stochAN_multi simulation.
%
% Usage: segments = fiber_initialization(fiber,model)
%
%   segments:   Structure containing segments' electrical/spatial properties.
%   fiber:      Structure containing fiber's parameters
%   model:      Contains model parameters that don't vary fiber-to-fiber.
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

% Set up segment index arrays
intSegs = fiber.intSegs;
numSegments = (fiber.numNodes)*(1+intSegs);
segmentIDX = 1:numSegments;
nodeIDX = 1:intSegs+1:numSegments;
segments.nodeIDX = nodeIDX;
internodeIDX = segmentIDX(mod((segmentIDX-1),intSegs+1)~=0);
somaIDX=nodeIDX(6);

% Calculate segment geometrical properties
diameter = fiber.diameter*1e-3;
somadiameter=fiber.somadiameter*1e-3;
periphdiameter=fiber.periphdiameter*1e-3;
somalength=fiber.somalength*1e-3;
presomalength=fiber.presomalength*1e-3;
postsomalength=fiber.postsomalength*1e-3;
D = diameter/fiber.dtoD;      % Healthy region myelin diameter (mm)
h2 = D* model.LtoD/intSegs;   % length of internode segment (mm)
nodeSurf = pi()*model.constrict*model.nLen*diameter;   % Node surface area (mm^2)
nodeCross = pi*(diameter/2*model.constrict)^2;          % Node cross-section area (mm^2)
intCross = pi*(diameter/2)^2; % Internode cross-section area (mm^2)

somaSurf=pi*model.nLen*somadiameter;    %Soma surface area (mm^2)
somaCross=pi*(somadiameter/2)^2;        %Soma cross-section area (mm^2)


xset = zeros(1,numSegments);
xset(nodeIDX) = ones(1,fiber.numNodes)*model.nLen;
xset(internodeIDX) = ones(1,length(internodeIDX))*h2;
xset(somaIDX)=somalength;
xset(somaIDX-1)=presomalength;
xset(somaIDX+1)=postsomalength
segments.xApp = cumsum(xset);
segments.xEcap = segments.xApp;
segments.zDistance = fiber.zDistance;

rm =  zeros(1,numSegments);
cm =  zeros(1,numSegments);
ra =  zeros(1,numSegments-1);

% Calculate internode segment electrical properties
rm(internodeIDX) = model.IntNode.rm/h2;
cm(internodeIDX) = model.IntNode.cm*h2;
ra(:)            = model.IntNode.ra*h2/intCross;

% Calculate nodal segment electrical properties
rm(nodeIDX) = model.Node.rm/nodeSurf;
cm(nodeIDX) = model.Node.cm*nodeSurf;
ra(nodeIDX) = model.Node.ra*model.nLen/nodeCross;

rm(somaIDX-1) = model.Node.rm/nodeSurf;
cm(somaIDX-1) = model.Node.cm*nodeSurf;
ra(somaIDX-1) = model.Node.ra*model.nLen/nodeCross;

rm(somaIDX+1) = model.Node.rm/nodeSurf;
cm(somaIDX+1) = model.Node.cm*nodeSurf;
ra(somaIDX+1) = model.Node.ra*model.nLen/nodeCross;

rm(1) = model.Node.rm/(nodeSurf*model.Terminal.areaCoef);
cm(1) = model.Node.cm*nodeSurf*model.Terminal.areaCoef;
rm(somaIDX)=model.Node.rm/somaSurf
cm(somaIDX)=model.Node.cm*somaSurf
ra(somaIDX)=model.Node.ra*model.nLen/somaCross;

rm(end) = model.Node.rm/(nodeSurf*model.Terminal.areaCoef);
cm(end) = model.Node.cm*nodeSurf*model.Terminal.areaCoef;

segments.rm = rm;
segments.cm = cm;
segments.ra = ra;

numNaf = zeros(1,numSegments);
numNaf(nodeIDX) = round(nodeSurf*model.Naf.density);
numNaf(somaIDX-1) = round(nodeSurf*model.Naf.density*10);
numNaf(somaIDX+1) = round(nodeSurf*model.Naf.density*10);
numNaf(somaIDX) = round(somaSurf*model.Naf.density);
% numNaf(1) = round(nodeSurf*model.Terminal.areaCoef*model.Naf.density);
% numNaf(end) = round(nodeSurf*model.Terminal.areaCoef*model.Naf.density);

numKf =  zeros(1,numSegments);
numKf(nodeIDX) = round(nodeSurf*model.Kf.density);
numKf(somaIDX-1) = round(nodeSurf*model.Naf.density*10);
numKf(somaIDX+1) = round(nodeSurf*model.Naf.density*10);
numKf(somaIDX) = round(somaSurf*model.Kf.density);
% numKf(1) = round(nodeSurf*model.Terminal.areaCoef*model.Kf.density);
% numKf(end) = round(nodeSurf*model.Terminal.areaCoef*model.Kf.density);

numKs =  zeros(1,numSegments);
numKs(nodeIDX) = round(nodeSurf*model.Ks.density);
numKs(somaIDX-1) = round(nodeSurf*model.Naf.density*10);
numKs(somaIDX+1) = round(nodeSurf*model.Naf.density*10);
numKs(somaIDX) = round(somaSurf*model.Ks.density);
% numKs(1) = round(nodeSurf*model.Terminal.areaCoef*model.Ks.density);
% numKs(end) = round(nodeSurf*model.Terminal.areaCoef*model.Ks.density);

segments.numNaf = numNaf;
segments.numKf = numKf;
segments.numKs = numKs;
end