function fiberPop = initialize_Population_Demyelinated(numFibers,demySevParam,anamProps,model)
%INITIALIZE_POPULATION_DEMYELINATED: This function recieves and integer
%containing the number of fibers, a structure containing the statistical
%parameters for population fiber anatomy/geometry, and a matrix containing
%the demyelination severity parameters. It returns fiberPop, a structure
%containing arrays structural properties for all individual fibers. %Note:
%this differs from the normal function in that 'demyelinated' fibers are
%defined as in:
%
% Resnick, J.M., O�Brien, G., Rubinstein, J.T., 2018. Simulated auditory
%   nerve axon demyelination alters sensitivity and response timing to
%   extracellular stimulation. Hear. Res. 361, 121�137.
%   doi:10.1016/j.heares.2018.01.014
%
%
% Usage: fiberPop = initialize_Population_Demyelinated(numFibers,demySevParam,anamProps)
%
%   fiberPop:       Structure containing arrays of fiber individual fibers'
%                   properties.
%   numFibers:      Integer number of fibers to creates
%   demySevParam:   Matrix of statistical paramaters governing fiber 
%                   demyelination.
%   anamProps:      Anatomical property statistics for generating 
%                   populations.
%
% Jesse M. Resnick (resnick@uw.edu) � 2018
%%  Set Constant Anatomic Parameters
diamMean	= anamProps(1);	% use 2 mm Imennov & Rubinstein 2009. 1.477 mm Wan & Corfas 2017
diamStdev	= anamProps(2);   % use 0.5 mm Imennov & Rubinstein 2009. 0.22 mm Wan & Corfas 2017
normdtoDmu    = anamProps(3);     %dtoD of healthy neurons. 0.643 Wan & Corfas 2017
normdtoDsigma = anamProps(4);

diameters = logNormpdftoparams('fibnum',numFibers, 'avg', log(diamMean),...
    'std', diamStdev,'min',0.5,'drawType','uniform');

zs = ones(1,numFibers)*3; % Electrode distance from fiber. (mm)
numnodes = ceil(36./(diameters./(diamMean)));  % the number of active nodes in the nerve
intSegs  = ceil(model.intSegs.*diameters/diamMean);
maxNodes = max(numnodes);

%% Set myelination state for each fiber

normdtoD = normpdftoparams('fibnum',numFibers, 'avg',...
        normdtoDmu,'std',normdtoDsigma,'min',0.4,'max',1);
    
% Uses demySev mean and sigma to construct normpdf to pull random
% values from to assign the dtoD and cutoff for each fiber. Column 1
% contains mu and sigma values for dtoD and column 2 for cutoffs.
sevMu    = demySevParam(1,1);
sevSigma = demySevParam(1,2);

if sevSigma == 0
    dtoDSev = sevMu*ones(1,numFibers);
else
    dtoDSev = normpdftoparams('fibnum',numFibers, 'avg',...
        sevMu,'std',sevSigma,'min',-.2,'max',1);
end
demydtoD = normdtoD + (1-normdtoD).*dtoDSev;

extentMu    = demySevParam(2,1);
extentSigma = demySevParam(2,2);
if extentSigma == 0
    demySev = extentMu*ones(1,numFibers);
else
    demySev = normpdftoparams('fibnum',numFibers, 'avg',...
        extentMu,'std',extentSigma,'min',0,'max',1);
end
cutoff = round(numnodes.*demySev);
    
    
%% -----------------------------------------------------------------------
% SAVE EXPERIMENT CONFIGURATION INFORMATION to Struct
%-------------------------------------------------------------------------
% Save anatomical parameters used to generate pop
fiberPop.diamMean       = diamMean;
fiberPop.diamStdev      = diamStdev;
fiberPop.normdtoDmu     = normdtoDmu;
fiberPop.normdtoDsigma  = normdtoDsigma;
fiberPop.dtoDsevMu         = sevMu;
fiberPop.dtoDsevSigma      = sevSigma;
fiberPop.extentMu       = extentMu;
fiberPop.extentSigma    = extentSigma;
fiberPop.intSegs        = intSegs;

% Save generated fiber parameters
fiberPop.zs             = zs;
fiberPop.maxNodes       = maxNodes;
fiberPop.diameters		= diameters;
fiberPop.numNodes       = numnodes;
fiberPop.normdtoD       = normdtoD;
fiberPop.demydtoD       = demydtoD;
fiberPop.cutoff         = cutoff;
end