function [zs, diameters, dtoDs] = handpick_Population(anamProps)

numDs = length(anamProps.diameters);
numdtoDs = length(anamProps.dtoDs);
numZs = length(anamProps.zs);

diameters = repmat(anamProps.diameters,numZs*numdtoDs,1);
diameters = reshape(diameters,[numZs*numdtoDs*numDs,1]);

dtoDs = repmat(anamProps.dtoDs.',numZs*numDs,1);

zs      = repmat(anamProps.zs,numdtoDs,1);
zs      = reshape(zs,[numdtoDs*numZs,1]);
zs      = repmat(zs,numDs,1);
