function segments = fiber_initialization_demyelinated(fiber,model)
%FIBER_INITIALIZATION_DEMYELINATED: This function recieves fiber and model
%structures as inputs and returns the structure segments which contains
%only the calculated quantities necessary for stochAN_multi simulation.
%Note: this differs from the normal function in that 'demyelinated'
%segments are defined as in:
%
% Resnick, J.M., O�Brien, G., Rubinstein, J.T., 2018. Simulated auditory
%   nerve axon demyelination alters sensitivity and response timing to
%   extracellular stimulation. Hear. Res. 361, 121�137.
%   doi:10.1016/j.heares.2018.01.014
%
% Usage: segments = fiber_initialization_demyelinated(fiber,model)
%
%   segments:   Structure containing segments' electrical/spatial properties.
%   fiber:      Structure containing fiber's parameters
%   model:      Contains model parameters that don't vary fiber-to-fiber.
%
% Jesse M. Resnick (resnick@uw.edu) � 2018
% Set up segment index arrays
intSegs = model.intSegs;
numSegments = (fiber.numNodes)*(1+intSegs);
segmentIDX = 1:numSegments;
nodeIDX = 1:intSegs+1:numSegments;
segments.nodeIDX = nodeIDX;
internodeIDX = segmentIDX(mod((segmentIDX-1),intSegs+1)~=0);

% Calculate segment geometrical properties
diameter = fiber.diameter*1e-3;
D = diameter/fiber.normdtoD;      % Healthy region myelin diameter (mm)
demyD = diameter/fiber.demydtoD;  % Demyelinated region myelin diameter (mm)
h2 = D* model.LtoD/model.intSegs;   % length of internode segment (mm)
nodeSurf = pi()*model.constrict*model.nLen*diameter;   % Node surface area (mm^2)
nodeCross = pi*(diameter/2*model.constrict)^2;          % Node cross-section area (mm^2)
intCross = pi*(diameter/2)^2; % Internode cross-section area (mm^2)

xset = zeros(1,numSegments);
xset(nodeIDX) = ones(1,fiber.numNodes)*model.nLen;
xset(internodeIDX) = ones(1,length(internodeIDX))*h2;
segments.xApp = cumsum(xset);
segments.xEcap = segments.xApp;
segments.zDistance = fiber.zDistance;

rm =  zeros(1,numSegments);
cm =  zeros(1,numSegments);
ra =  zeros(1,numSegments-1);

% Calculate internodal segment resistances
rm(internodeIDX) = model.IntNode.rm/h2;
ra(:) = model.IntNode.ra*h2/intCross;

% Calculate nodal segment electrical properties
rm(nodeIDX) = model.Node.rm/nodeSurf;
cm(nodeIDX) = model.Node.cm*nodeSurf;
ra(nodeIDX) = model.Node.ra*model.nLen/nodeCross;

rm(1) = model.Node.rm/(nodeSurf*model.Terminal.areaCoef);
cm(1) = model.Node.cm*nodeSurf*model.Terminal.areaCoef;
rm(end) = model.Node.rm/(nodeSurf*model.Terminal.areaCoef);
cm(end) = model.Node.cm*nodeSurf*model.Terminal.areaCoef;

% Calculate demyelinated internode segment capacitance
intCutoff = fiber.cutoff*(model.intSegs+1);
demyInts = internodeIDX(internodeIDX < intCutoff);
invdemycmmy = log(demyD/diameter)/log(D/diameter)*(1/model.IntNode.cm-1/...
    (model.Node.cm*pi*diameter))+1/(model.Node.cm*pi*diameter);
cm(demyInts) = 1/invdemycmmy*h2;

% Calculate healthy internode segment capacitance
healthyInts = internodeIDX(internodeIDX >= intCutoff);
cm(healthyInts) = model.IntNode.cm*h2;

segments.rm = rm;
segments.cm = cm;
segments.ra = ra;

numNaf = zeros(1,numSegments);
numNaf(nodeIDX) = round(nodeSurf*model.Naf.density);
numKf =  zeros(1,numSegments);
numKf(nodeIDX) = round(nodeSurf*model.Kf.density);
numKs =  zeros(1,numSegments);
numKs(nodeIDX) = round(nodeSurf*model.Ks.density);

segments.numNaf = numNaf;
segments.numKf = numKf;
segments.numKs = numKs;
end

