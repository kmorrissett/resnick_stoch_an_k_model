function options = options_Config()
%OPTIONSCONFIG This function contains the optional parameters for running
%stochAN_multi simulations.
%
% Jesse M. Resnick (resnick@uw.edu) � 2018

%% Set options
options.seed = sum(100*clock);
options.numMonte = 10;
options.maxSpikes = 10;
options.maxVoltage = 1500;
options.recV = 0;               % record voltage flag
options.recECAP = 0;            % record ECAP flag
options.meanNodeAbove = 10;
options.meanRecordNode = 32;
options.posRecord = options.meanRecordNode/36;
options.nodeInt = options.posRecord;
options.Vsample = 0.01;             % voltage sampling rate
options.eCAPxLoc = 10*(0.231);    % ecap recording x-location
options.eCAPzLoc = 3;             % ecap recording height
end