function vararginCellArray = struct2varargin(structure)
%STRUCT2VARARGIN: Convert structure to cell array with format:
%   {fieldName1,value1,fieldName2,value2,...};

vararginCellArray(1,:) = fieldnames(structure);
vararginCellArray(2,:) = struct2cell(structure);
vararginCellArray = reshape(vararginCellArray,1,numel(vararginCellArray));

end

